﻿using Bdx.Common.StructureMap;
using StructureMap;

namespace Testing_GraphQL_Consumer.Services.Concrete
{
    public static class StructureMapBootStrapper
    {
        /// <summary>Configure</summary>
        public static IContainer Configure()
        {
            var container = StructureMapBootstrapper.Bootstrap("Bdx.*");
            container.Configure(r => r.AddRegistry(new NhsServicesRegistry()));
            return container;
        }
    }
}
