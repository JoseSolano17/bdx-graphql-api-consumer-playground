﻿using StructureMap;

namespace Testing_GraphQL_Consumer.Services
{
    public class NhsServicesRegistry : Registry
    {
        public NhsServicesRegistry()
        {
            For<IGraphQLApiConsumer>().Use<GraphQLApiConsumer>();

        }
    }
}
