﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Bdx.Common.GraphQl.Client;
using Bdx.Common.GraphQl.Client.Http;
using Bdx.Common.GraphQL.API.Consumer.Consumer;
using Newtonsoft.Json;
using Bdx.Common.GraphQL.API.Consumer.helpers;
using Bdx.Common.Utility;
using Bdx.Common.Utility.Enums;
using System.Threading.Tasks;
using System.Linq;

public partial class GraphQLApiMarket
{
	/// <summary>Cities</summary>
	[GraphQLField("cities")]
	public List<GraphQLApiCity> Cities { get; set; }

	/// <summary>Market Id</summary>
	[GraphQLField("id")]
	public int? Id { get; set; }

	/// <summary>Latitude</summary>
	[GraphQLField("lat")]
	public decimal? Lat { get; set; }

	/// <summary>Longitude</summary>
	[GraphQLField("lng")]
	public decimal? Lng { get; set; }

	/// <summary>Market Name</summary>
	[GraphQLField("name")]
	public string Name { get; set; }

	/// <summary>State Abbreviation</summary>
	[GraphQLField("stateAbbr")]
	public string StateAbbr { get; set; }

	/// <summary>State Name</summary>
	[GraphQLField("stateName")]
	public string StateName { get; set; }

	/// <summary>Total Communities</summary>
	[GraphQLField("totalComms")]
	public int? TotalComms { get; set; }

	/// <summary>Total Custom Builders</summary>
	[GraphQLField("totalCustomBuilders")]
	public int? TotalCustomBuilders { get; set; }

}

public partial class GraphQLApiCity
{
	/// <summary>County</summary>
	[GraphQLField("county")]
	public string County { get; set; }

	/// <summary>Market Id</summary>
	[GraphQLField("mktId")]
	public int? MktId { get; set; }

	/// <summary>Market Name</summary>
	[GraphQLField("mktName")]
	public string MktName { get; set; }

	/// <summary>City</summary>
	[GraphQLField("name")]
	public string Name { get; set; }

	/// <summary>State</summary>
	[GraphQLField("state")]
	public string State { get; set; }

}

public partial class GraphQLApiCommunity
{
	/// <summary>Community Description</summary>
	[GraphQLField("desc")]
	public string Desc { get; set; }

	/// <summary>Community Id</summary>
	[GraphQLField("id")]
	public int? Id { get; set; }

	/// <summary>Images</summary>
	[GraphQLField("images")]
	public List<GraphQLApiImage> Images { get; set; }

	/// <summary>Market Id</summary>
	[GraphQLField("marketId")]
	public int? MarketId { get; set; }

	/// <summary>Market Name</summary>
	[GraphQLField("marketName")]
	public string MarketName { get; set; }

	/// <summary>Community Name</summary>
	[GraphQLField("name")]
	public string Name { get; set; }

	/// <summary>Parent Community Id</summary>
	[GraphQLField("parentId")]
	public int? ParentId { get; set; }

	/// <summary>Parent Community Name</summary>
	[GraphQLField("parentName")]
	public string ParentName { get; set; }

	/// <summary>Community Type</summary>
	[GraphQLField("type")]
	public string Type { get; set; }

}

public partial class GraphQLApiImage
{
	/// <summary>Category</summary>
	[GraphQLField("category")]
	public string Category { get; set; }

	/// <summary>Click Through Url</summary>
	[GraphQLField("clickThruURL")]
	public string ClickThruURL { get; set; }

	/// <summary>Image Description</summary>
	[GraphQLField("description")]
	public string Description { get; set; }

	/// <summary>Image Id</summary>
	[GraphQLField("id")]
	public int? Id { get; set; }

	/// <summary>Image Name</summary>
	[GraphQLField("name")]
	public string Name { get; set; }

	/// <summary>Image Path</summary>
	[GraphQLField("path")]
	public string Path { get; set; }

	/// <summary>Image Sequence</summary>
	[GraphQLField("pos")]
	public int? Pos { get; set; }

	/// <summary>Primary Flag</summary>
	[GraphQLField("primaryFlag")]
	public string PrimaryFlag { get; set; }

	/// <summary>Image Title</summary>
	[GraphQLField("title")]
	public string Title { get; set; }

	/// <summary>Image Type Code</summary>
	[GraphQLField("type")]
	public string Type { get; set; }

}

public partial class GraphQLApiPartner
{
	/// <summary>Allow Registration</summary>
	[GraphQLField("allowRegistration")]
	public string AllowRegistration { get; set; }

	/// <summary>Brand Partner Id</summary>
	[GraphQLField("brandPartnerId")]
	public int? BrandPartnerId { get; set; }

	/// <summary>Brand Partner Name</summary>
	[GraphQLField("brandPartnerName")]
	public string BrandPartnerName { get; set; }

	/// <summary>Footer Show About Us</summary>
	[GraphQLField("footerShowAboutUs")]
	public string FooterShowAboutUs { get; set; }

	/// <summary>From Email</summary>
	[GraphQLField("fromEmail")]
	public string FromEmail { get; set; }

	/// <summary>From Email</summary>
	[GraphQLField("fromRegisterEmail")]
	public string FromRegisterEmail { get; set; }

	/// <summary>Partner Id</summary>
	[GraphQLField("id")]
	public int? Id { get; set; }

	/// <summary>Lead Posting Password</summary>
	[GraphQLField("leadPostingPassword")]
	public string LeadPostingPassword { get; set; }

	/// <summary>Mask Index</summary>
	[GraphQLField("maskIndex")]
	public int? MaskIndex { get; set; }

	/// <summary>Partner Name</summary>
	[GraphQLField("name")]
	public string Name { get; set; }

	/// <summary>Partner Logo</summary>
	[GraphQLField("partnerLogo")]
	public string PartnerLogo { get; set; }

	/// <summary>Partner Site Url</summary>
	[GraphQLField("siteUrl")]
	public string SiteUrl { get; set; }

	/// <summary>Uses Match Maker</summary>
	[GraphQLField("usesMatchMaker")]
	public string UsesMatchMaker { get; set; }

}

public partial class GraphQLApiState
{
	/// <summary>Latitude</summary>
	[GraphQLField("lat")]
	public decimal? Lat { get; set; }

	/// <summary>Longitude</summary>
	[GraphQLField("lng")]
	public decimal? Lng { get; set; }

	/// <summary>Markets</summary>
	[GraphQLField("markets")]
	public List<GraphQLApiMarket> Markets { get; set; }

	/// <summary>State Abbreviation</summary>
	[GraphQLField("stateAbbr")]
	public string StateAbbr { get; set; }

	/// <summary>State Name</summary>
	[GraphQLField("stateName")]
	public string StateName { get; set; }

}

public partial class GraphQLSearchCommunities
{
	/// <summary>Address</summary>
	[GraphQLField("addr")]
	public string Addr { get; set; }

	/// <summary>Maximum Baths Quantity</summary>
	[GraphQLField("baHi")]
	public int? BaHi { get; set; }

	/// <summary>Minimum Baths Quantity</summary>
	[GraphQLField("baLo")]
	public int? BaLo { get; set; }

	/// <summary>Maximum Spec Baths Quantity</summary>
	[GraphQLField("baSpecsHi")]
	public int? BaSpecsHi { get; set; }

	/// <summary>Minimun Spec Quantity Bathrooms</summary>
	[GraphQLField("baSpecsLo")]
	public int? BaSpecsLo { get; set; }

	/// <summary>Community Type (C: Coming soon; G: Gated; N: Normal; X: Closed)</summary>
	[GraphQLField("bcType")]
	public string BcType { get; set; }

	/// <summary>Brand object</summary>
	[GraphQLField("brand")]
	public GraphQLApiBrand Brand { get; set; }

	/// <summary>Brand Id</summary>
	[GraphQLField("brandId")]
	public int? BrandId { get; set; }

	/// <summary>Brand Name</summary>
	[GraphQLField("brandName")]
	public string BrandName { get; set; }

	/// <summary>Maximum Bedrooms Quantity</summary>
	[GraphQLField("brHi")]
	public int? BrHi { get; set; }

	/// <summary>Minimum Bedrooms Quantity</summary>
	[GraphQLField("brLo")]
	public int? BrLo { get; set; }

	/// <summary>Maximum Spec Bedrooms Quantity</summary>
	[GraphQLField("brSpecsHi")]
	public int? BrSpecsHi { get; set; }

	/// <summary>Minimum Bedrooms of Spec Quantity</summary>
	[GraphQLField("brSpecsLo")]
	public int? BrSpecsLo { get; set; }

	/// <summary>Builder Id</summary>
	[GraphQLField("builderId")]
	public int? BuilderId { get; set; }

	/// <summary>Builder Name</summary>
	[GraphQLField("builderName")]
	public string BuilderName { get; set; }

	/// <summary>City</summary>
	[GraphQLField("city")]
	public string City { get; set; }

	/// <summary>Community Latitude</summary>
	[GraphQLField("commLatitude")]
	public decimal? CommLatitude { get; set; }

	/// <summary>Community Longitude</summary>
	[GraphQLField("commLongitude")]
	public decimal? CommLongitude { get; set; }

	/// <summary>Num Homes</summary>
	[GraphQLField("commNumHomes")]
	public int? CommNumHomes { get; set; }

	/// <summary>Community Total Number Of Plans</summary>
	[GraphQLField("commNumPlans")]
	public int? CommNumPlans { get; set; }

	/// <summary>Community Status (N: Active; C: Coming Soon; G: Grand Opening; X: Close Out; CG: ComingSoonGrandOpening)</summary>
	[GraphQLField("commStatus")]
	public string CommStatus { get; set; }

	/// <summary>County</summary>
	[GraphQLField("county")]
	public string County { get; set; }

	/// <summary>Description Teaser</summary>
	[GraphQLField("descTeaser")]
	public string DescTeaser { get; set; }

	/// <summary>School Districts</summary>
	[GraphQLField("districts")]
	public List<GraphQLApiDisctrict> Districts { get; set; }

	/// <summary>Maximun Quantity Half Bathrooms</summary>
	[GraphQLField("halfBaHi")]
	public int? HalfBaHi { get; set; }

	/// <summary>Minimun Quantity Half Bathrooms</summary>
	[GraphQLField("halfBaLo")]
	public int? HalfBaLo { get; set; }

	/// <summary>Maximun Spec Quantity Half Bathrooms</summary>
	[GraphQLField("halfBaSpecsHi")]
	public int? HalfBaSpecsHi { get; set; }

	/// <summary>Minimun Quantity Half Bathrooms Specs</summary>
	[GraphQLField("halfBaSpecsLo")]
	public int? HalfBaSpecsLo { get; set; }

	/// <summary>Has Agent Promotion</summary>
	[GraphQLField("hasAgePromo")]
	public bool? HasAgePromo { get; set; }

	/// <summary>Has Age Rest</summary>
	[GraphQLField("hasAgeRest")]
	public bool? HasAgeRest { get; set; }

	/// <summary>Has Builder Review</summary>
	[GraphQLField("hasBuilderReview")]
	public bool? HasBuilderReview { get; set; }

	/// <summary>Has Community Center</summary>
	[GraphQLField("hasCommCenter")]
	public bool? HasCommCenter { get; set; }

	/// <summary>Has Consumer Promo</summary>
	[GraphQLField("hasConPromo")]
	public bool? HasConPromo { get; set; }

	/// <summary>Has Event </summary>
	[GraphQLField("hasEvent")]
	public bool? HasEvent { get; set; }

	/// <summary>Has Golf</summary>
	[GraphQLField("hasGolf")]
	public bool? HasGolf { get; set; }

	/// <summary>Has Hot Home</summary>
	[GraphQLField("hasHotHome")]
	public bool? HasHotHome { get; set; }

	/// <summary>Has Nature areas</summary>
	[GraphQLField("hasNature")]
	public bool? HasNature { get; set; }

	/// <summary>Has Park</summary>
	[GraphQLField("hasPark")]
	public bool? HasPark { get; set; }

	/// <summary>Has Pool</summary>
	[GraphQLField("hasPool")]
	public bool? HasPool { get; set; }

	/// <summary>Has Sport</summary>
	[GraphQLField("hasSport")]
	public bool? HasSport { get; set; }

	/// <summary>Has View</summary>
	[GraphQLField("hasView")]
	public bool? HasView { get; set; }

	/// <summary>Has View Only</summary>
	[GraphQLField("hasViewOnly")]
	public bool? HasViewOnly { get; set; }

	/// <summary>Has Water Front Only</summary>
	[GraphQLField("hasWaterFrontOnly")]
	public bool? HasWaterFrontOnly { get; set; }

	/// <summary>Id</summary>
	[GraphQLField("id")]
	public string Id { get; set; }

	/// <summary>Is Adult</summary>
	[GraphQLField("isAdult")]
	public bool? IsAdult { get; set; }

	/// <summary>Is Gated </summary>
	[GraphQLField("isGated")]
	public bool? IsGated { get; set; }

	/// <summary>Has Green</summary>
	[GraphQLField("isGreen")]
	public bool? IsGreen { get; set; }

	/// <summary>Is Master </summary>
	[GraphQLField("isMaster")]
	public bool? IsMaster { get; set; }

	/// <summary>Is Town Home</summary>
	[GraphQLField("isTownHome")]
	public bool? IsTownHome { get; set; }

	/// <summary>Is Urban</summary>
	[GraphQLField("isUrban")]
	public bool? IsUrban { get; set; }

	/// <summary>Listing Type</summary>
	[GraphQLField("listingType")]
	public string ListingType { get; set; }

	/// <summary>Market Id</summary>
	[GraphQLField("marketId")]
	public int? MarketId { get; set; }

	/// <summary>Market Name</summary>
	[GraphQLField("marketName")]
	public string MarketName { get; set; }

	/// <summary>Community Name</summary>
	[GraphQLField("name")]
	public string Name { get; set; }

	/// <summary>Quick Move In Count</summary>
	[GraphQLField("numQmi")]
	public int? NumQmi { get; set; }

	/// <summary>Office Latitude</summary>
	[GraphQLField("officeLatitude")]
	public decimal? OfficeLatitude { get; set; }

	/// <summary>Office Longitude</summary>
	[GraphQLField("officeLongitude")]
	public decimal? OfficeLongitude { get; set; }

	/// <summary>Parent Community Id</summary>
	[GraphQLField("parentCommId")]
	public int? ParentCommId { get; set; }

	/// <summary>Phone</summary>
	[GraphQLField("phone")]
	public string Phone { get; set; }

	/// <summary>Higher Price</summary>
	[GraphQLField("prHi")]
	public decimal? PrHi { get; set; }

	/// <summary>Minimum Price</summary>
	[GraphQLField("prLo")]
	public decimal? PrLo { get; set; }

	/// <summary>Project Type: (BYL: Boyl; CST: Custom; MFR: Manufactured)</summary>
	[GraphQLField("projectTypeCode")]
	public string ProjectTypeCode { get; set; }

	/// <summary>Schools</summary>
	[GraphQLField("schools")]
	public List<GraphQLApiSchool> Schools { get; set; }

	/// <summary>Maximum Square Feet Spec</summary>
	[GraphQLField("sfHiSpecs")]
	public int? SfHiSpecs { get; set; }

	/// <summary>Minimum Square Feet Spec</summary>
	[GraphQLField("sfLoSpecs")]
	public int? SfLoSpecs { get; set; }

	/// <summary>Maximum Square Feet</summary>
	[GraphQLField("sftHi")]
	public int? SftHi { get; set; }

	/// <summary>Minimum Square Feet</summary>
	[GraphQLField("sftLo")]
	public int? SftLo { get; set; }

	/// <summary>Sort Key</summary>
	[GraphQLField("sortKey")]
	public int? SortKey { get; set; }

	/// <summary>State</summary>
	[GraphQLField("state")]
	public string State { get; set; }

	/// <summary>Sub Video</summary>
	[GraphQLField("subVideo")]
	public string SubVideo { get; set; }

	/// <summary>Spotlight Thumbnail Image</summary>
	[GraphQLField("thumb1")]
	public string Thumb1 { get; set; }

	/// <summary>Spotlight Thumbnail Image</summary>
	[GraphQLField("thumb2")]
	public string Thumb2 { get; set; }

	/// <summary>Spotlight Thumbnail Image</summary>
	[GraphQLField("thumb3")]
	public string Thumb3 { get; set; }

	/// <summary>Zip Code</summary>
	[GraphQLField("zip")]
	public string Zip { get; set; }

}

public partial class GraphQLApiSchool
{
	/// <summary>School Level</summary>
	[GraphQLField("level")]
	public string Level { get; set; }

	/// <summary>National Center for Education Statistics ID</summary>
	[GraphQLField("ncesId")]
	public string NcesId { get; set; }

	/// <summary>School District Id</summary>
	[GraphQLField("schoolDistrictId")]
	public int? SchoolDistrictId { get; set; }

	/// <summary>School Id</summary>
	[GraphQLField("schoolId")]
	public int? SchoolId { get; set; }

	/// <summary>School Level Id</summary>
	[GraphQLField("schoolLevelId")]
	public int? SchoolLevelId { get; set; }

	/// <summary>School Name</summary>
	[GraphQLField("schoolName")]
	public string SchoolName { get; set; }

}

public partial class GraphQLApiBrand
{
	[GraphQLField("boylCount")]
	public int? BoylCount { get; set; }

	[GraphQLField("city")]
	public string City { get; set; }

	[GraphQLField("communityCount")]
	public int? CommunityCount { get; set; }

	[GraphQLField("customCount")]
	public int? CustomCount { get; set; }

	[GraphQLField("hasShowCase")]
	public int? HasShowCase { get; set; }

	[GraphQLField("id")]
	public int? Id { get; set; }

	[GraphQLField("isBoyl")]
	public int? IsBoyl { get; set; }

	[GraphQLField("isCustomBuilder")]
	public int? IsCustomBuilder { get; set; }

	[GraphQLField("isModularBuilder")]
	public int? IsModularBuilder { get; set; }

	[GraphQLField("logo")]
	public string Logo { get; set; }

	[GraphQLField("logoSmall")]
	public string LogoSmall { get; set; }

	[GraphQLField("marketId")]
	public int? MarketId { get; set; }

	[GraphQLField("modularCount")]
	public int? ModularCount { get; set; }

	[GraphQLField("name")]
	public string Name { get; set; }

	[GraphQLField("showCaseStatusId")]
	public int? ShowCaseStatusId { get; set; }

	[GraphQLField("siteUrl")]
	public string SiteUrl { get; set; }

	[GraphQLField("state")]
	public string State { get; set; }

	[GraphQLField("totalHome")]
	public int? TotalHome { get; set; }

}

public partial class GraphQLApiDisctrict
{
	/// <summary>School District Id</summary>
	[GraphQLField("districtId")]
	public int? DistrictId { get; set; }

	/// <summary>School District Name</summary>
	[GraphQLField("districtName")]
	public string DistrictName { get; set; }

	/// <summary>School District Url</summary>
	[GraphQLField("districtUrl")]
	public string DistrictUrl { get; set; }

	/// <summary>Fax</summary>
	[GraphQLField("fax")]
	public string Fax { get; set; }

	/// <summary>National Center for Education Statistics ID</summary>
	[GraphQLField("ncesId")]
	public string NcesId { get; set; }

	/// <summary>Phone</summary>
	[GraphQLField("phone")]
	public string Phone { get; set; }

}

public struct GraphQLGeoPolygonInputType
{
	/// <summary>Latitude</summary>
	[JsonProperty(PropertyName = "lat")]
	public float? Lat { get; set; }

	/// <summary>Longitude</summary>
	[JsonProperty(PropertyName = "lng")]
	public float? Lng { get; set; }

}

public struct GraphQLSortValueInputType
{
	/// <summary>Field name to Sort By</summary>
	[JsonProperty(PropertyName = "name")]
	public string Name { get; set; }

	/// <summary>Ascending or descending order. (ASC: default)</summary>
	[JsonProperty(PropertyName = "order"), JsonConverter(typeof(GraphQLEnumConverter))]
	public GraphQLSortOrder Order { get; set; }

}

[JsonConverter(typeof(GraphQLEnumConverter))]
public enum GraphQLSortOrder
{
	/// <summary>Ascending</summary>
	ASC,
	/// <summary>Descending</summary>
	DESC
}

[JsonConverter(typeof(GraphQLEnumConverter))]
public enum GraphQLSpecTypeInputType
{
	/// <summary>Active</summary>
	Active,
	/// <summary>Gallery Homes</summary>
	Gallery,
	/// <summary>Model Homes</summary>
	Model,
	/// <summary>Plan</summary>
	Plan,
	/// <summary>Spec</summary>
	Spec
}

public partial class GraphQLResultCommCounts
{
	/// <summary>Average Price</summary>
	[GraphQLField("averagePrice")]
	public decimal? AveragePrice { get; set; }

	/// <summary>Basic Listing Count</summary>
	[GraphQLField("blCount")]
	public int? BlCount { get; set; }

	/// <summary>Boyl Community Count</summary>
	[GraphQLField("boylCommCount")]
	public int? BoylCommCount { get; set; }

	/// <summary>Boyl Home Count</summary>
	[GraphQLField("boylHomeCount")]
	public int? BoylHomeCount { get; set; }

	/// <summary>Builder count</summary>
	[GraphQLField("builderCount")]
	public int? BuilderCount { get; set; }

	/// <summary>Communities All Homes Count</summary>
	[GraphQLField("commAllHomeCount")]
	public int? CommAllHomeCount { get; set; }

	/// <summary>Communities All Luxury Count</summary>
	[GraphQLField("commAllLuxuryCount")]
	public int? CommAllLuxuryCount { get; set; }

	/// <summary>Communities Count</summary>
	[GraphQLField("commCount")]
	public int? CommCount { get; set; }

	/// <summary>Facets</summary>
	[GraphQLField("facets")]
	public GraphQLApiFacets Facets { get; set; }

	/// <summary>Homes Count</summary>
	[GraphQLField("homeCount")]
	public int? HomeCount { get; set; }

	/// <summary>Hot Deals Count</summary>
	[GraphQLField("hotDealsCount")]
	public int? HotDealsCount { get; set; }

	/// <summary>Mfr Comm Count</summary>
	[GraphQLField("mfrCommCount")]
	public int? MfrCommCount { get; set; }

	/// <summary>Model Home Count</summary>
	[GraphQLField("modelHomeCount")]
	public int? ModelHomeCount { get; set; }

	/// <summary>Nby Community Count</summary>
	[GraphQLField("nbyCommCount")]
	public int? NbyCommCount { get; set; }

	/// <summary>Paid Community Count</summary>
	[GraphQLField("paidCommCount")]
	public int? PaidCommCount { get; set; }

	/// <summary>Paid Nby Community Count</summary>
	[GraphQLField("paidNbyCommCount")]
	public int? PaidNbyCommCount { get; set; }

	/// <summary>Quick Move In Count</summary>
	[GraphQLField("qmiCount")]
	public int? QmiCount { get; set; }

	/// <summary>Radius</summary>
	[GraphQLField("radius")]
	public int? Radius { get; set; }

	/// <summary>Square Feet Range</summary>
	[GraphQLField("sftRange")]
	public string SftRange { get; set; }

	/// <summary>Total Brands</summary>
	[GraphQLField("totalBrands")]
	public int? TotalBrands { get; set; }

	/// <summary>Total Plans</summary>
	[GraphQLField("totalPlans")]
	public int? TotalPlans { get; set; }

}

public partial class GraphQLApiFacets
{
	/// <summary>Is for Adult</summary>
	[GraphQLField("adult")]
	public int? Adult { get; set; }

	/// <summary>Bath Range</summary>
	[GraphQLField("baRange")]
	public string BaRange { get; set; }

	/// <summary>Boyl Community Count</summary>
	[GraphQLField("boyl")]
	public int? Boyl { get; set; }

	/// <summary>Brands</summary>
	[GraphQLField("brands")]
	public List<GraphQLApiFacetCountOption> Brands { get; set; }

	/// <summary>Bedroom Range</summary>
	[GraphQLField("brRange")]
	public string BrRange { get; set; }

	/// <summary>Builder Ids</summary>
	[GraphQLField("builders")]
	public List<int?> Builders { get; set; }

	/// <summary>Cities</summary>
	[GraphQLField("cities")]
	public List<GraphQLApiFacetCountOption> Cities { get; set; }

	/// <summary>Communities</summary>
	[GraphQLField("communities")]
	public List<GraphQLApiFacetOption> Communities { get; set; }

	/// <summary>County</summary>
	[GraphQLField("county")]
	public List<GraphQLApiFacetCountOption> County { get; set; }

	/// <summary>Custom</summary>
	[GraphQLField("custom")]
	public int? Custom { get; set; }

	/// <summary>Total Custom Builders</summary>
	[GraphQLField("customBuilders")]
	public int? CustomBuilders { get; set; }

	/// <summary>Gated</summary>
	[GraphQLField("gated")]
	public int? Gated { get; set; }

	/// <summary>Golf</summary>
	[GraphQLField("golf")]
	public int? Golf { get; set; }

	/// <summary>Green</summary>
	[GraphQLField("green")]
	public int? Green { get; set; }

	/// <summary>Mfr Comm Count</summary>
	[GraphQLField("manufactured")]
	public int? Manufactured { get; set; }

	/// <summary>Mf</summary>
	[GraphQLField("mf")]
	public int? Mf { get; set; }

	/// <summary>Has Nature areas</summary>
	[GraphQLField("nature")]
	public int? Nature { get; set; }

	/// <summary>Parks</summary>
	[GraphQLField("parks")]
	public int? Parks { get; set; }

	/// <summary>Pool</summary>
	[GraphQLField("pool")]
	public int? Pool { get; set; }

	/// <summary>Postal Codes</summary>
	[GraphQLField("postalCodes")]
	public List<GraphQLApiFacetCountOption> PostalCodes { get; set; }

	/// <summary>Price Range</summary>
	[GraphQLField("prRange")]
	public string PrRange { get; set; }

	/// <summary>School Districts</summary>
	[GraphQLField("schoolDistricts")]
	public List<GraphQLApiFacetOption> SchoolDistricts { get; set; }

	/// <summary>Square Feet</summary>
	[GraphQLField("sf")]
	public int? Sf { get; set; }

	/// <summary>Square Feet Range</summary>
	[GraphQLField("sftRange")]
	public string SftRange { get; set; }

	/// <summary>Sports</summary>
	[GraphQLField("sports")]
	public int? Sports { get; set; }

	/// <summary>Views</summary>
	[GraphQLField("views")]
	public int? Views { get; set; }

	/// <summary>Water front</summary>
	[GraphQLField("waterFront")]
	public int? WaterFront { get; set; }

}

public partial class GraphQLApiFacetCountOption
{
	/// <summary>Count</summary>
	[GraphQLField("count")]
	public int? Count { get; set; }

	/// <summary>Key</summary>
	[GraphQLField("key")]
	public int? Key { get; set; }

	/// <summary>Market Id</summary>
	[GraphQLField("marketId")]
	public string MarketId { get; set; }

	/// <summary>State</summary>
	[GraphQLField("state")]
	public string State { get; set; }

	/// <summary>Value</summary>
	[GraphQLField("value")]
	public string Value { get; set; }

}

public partial class GraphQLApiFacetOption
{
	/// <summary>Key</summary>
	[GraphQLField("key")]
	public int? Key { get; set; }

	/// <summary>State</summary>
	[GraphQLField("state")]
	public string State { get; set; }

	/// <summary>Value</summary>
	[GraphQLField("value")]
	public string Value { get; set; }

}

public partial class GraphQLSearchAllHomes
{
	/// <summary>Address</summary>
	[GraphQLField("addr")]
	public string Addr { get; set; }

	/// <summary>Address Plan</summary>
	[GraphQLField("addressPlan")]
	public string AddressPlan { get; set; }

	/// <summary>Amenities</summary>
	[GraphQLField("amenities")]
	public List<GraphQLApiAmenity> Amenities { get; set; }

	/// <summary>Maximum Baths Quantity</summary>
	[GraphQLField("baHi")]
	public int? BaHi { get; set; }

	/// <summary>Minimum Baths Quantity</summary>
	[GraphQLField("baLo")]
	public int? BaLo { get; set; }

	/// <summary>Community Type (C: Coming soon; G: Gated; N: Normal; X: Closed)</summary>
	[GraphQLField("bcType")]
	public string BcType { get; set; }

	/// <summary>Brand object</summary>
	[GraphQLField("brand")]
	public GraphQLApiBrand Brand { get; set; }

	/// <summary>Brand Id</summary>
	[GraphQLField("brandId")]
	public int? BrandId { get; set; }

	/// <summary>Brand Name</summary>
	[GraphQLField("brandName")]
	public string BrandName { get; set; }

	/// <summary>Maximum Bedrooms Quantity</summary>
	[GraphQLField("brHi")]
	public int? BrHi { get; set; }

	/// <summary>Minimum Bedrooms Quantity</summary>
	[GraphQLField("brLo")]
	public int? BrLo { get; set; }

	/// <summary>Builder Id</summary>
	[GraphQLField("builderId")]
	public int? BuilderId { get; set; }

	/// <summary>Builder Name</summary>
	[GraphQLField("builderName")]
	public string BuilderName { get; set; }

	/// <summary>City</summary>
	[GraphQLField("city")]
	public string City { get; set; }

	/// <summary>Community Id</summary>
	[GraphQLField("commId")]
	public int? CommId { get; set; }

	/// <summary>Community Status (N: Active; C: Coming Soon; G: Grand Opening; X: Close Out; CG: ComingSoonGrandOpening)</summary>
	[GraphQLField("commStatus")]
	public string CommStatus { get; set; }

	/// <summary>Community Name</summary>
	[GraphQLField("communityName")]
	public string CommunityName { get; set; }

	/// <summary>County</summary>
	[GraphQLField("county")]
	public string County { get; set; }

	/// <summary>Distance</summary>
	[GraphQLField("distance")]
	public decimal? Distance { get; set; }

	/// <summary>School Districts</summary>
	[GraphQLField("districts")]
	public List<GraphQLApiDisctrict> Districts { get; set; }

	/// <summary>Elevation path</summary>
	[GraphQLField("elevPath")]
	public string ElevPath { get; set; }

	/// <summary>Floor plan path</summary>
	[GraphQLField("floorPlanPath")]
	public string FloorPlanPath { get; set; }

	/// <summary>Maximum Garages Quantity</summary>
	[GraphQLField("garageHi")]
	public decimal? GarageHi { get; set; }

	/// <summary>Minimum Garages Quantity</summary>
	[GraphQLField("garageLo")]
	public decimal? GarageLo { get; set; }

	/// <summary>Half Bathrooms</summary>
	[GraphQLField("halfBa")]
	public int? HalfBa { get; set; }

	/// <summary>Has Agent Promotion</summary>
	[GraphQLField("hasAgePromo")]
	public bool? HasAgePromo { get; set; }

	/// <summary>Has Age Rest</summary>
	[GraphQLField("hasAgeRest")]
	public bool? HasAgeRest { get; set; }

	/// <summary>Has Builder Review</summary>
	[GraphQLField("hasBuilderReview")]
	public bool? HasBuilderReview { get; set; }

	/// <summary>Has Community Center</summary>
	[GraphQLField("hasCommCenter")]
	public bool? HasCommCenter { get; set; }

	/// <summary>Has Consumer Promo</summary>
	[GraphQLField("hasConPromo")]
	public bool? HasConPromo { get; set; }

	/// <summary>Has Event </summary>
	[GraphQLField("hasEvent")]
	public bool? HasEvent { get; set; }

	/// <summary>Has Golf</summary>
	[GraphQLField("hasGolf")]
	public bool? HasGolf { get; set; }

	/// <summary>Has Nature areas</summary>
	[GraphQLField("hasNature")]
	public bool? HasNature { get; set; }

	/// <summary>Has Park</summary>
	[GraphQLField("hasPark")]
	public bool? HasPark { get; set; }

	/// <summary>Has Pool</summary>
	[GraphQLField("hasPool")]
	public bool? HasPool { get; set; }

	/// <summary>Has Sport</summary>
	[GraphQLField("hasSport")]
	public bool? HasSport { get; set; }

	/// <summary>Has Video</summary>
	[GraphQLField("hasVideo")]
	public bool? HasVideo { get; set; }

	/// <summary>Has View</summary>
	[GraphQLField("hasView")]
	public bool? HasView { get; set; }

	/// <summary>Has View Only</summary>
	[GraphQLField("hasViewOnly")]
	public bool? HasViewOnly { get; set; }

	/// <summary>Has Water Front Only</summary>
	[GraphQLField("hasWaterFrontOnly")]
	public bool? HasWaterFrontOnly { get; set; }

	/// <summary>Home Id, Stores the Spec Id pr the plan Id according to the listing returned</summary>
	[GraphQLField("homeId")]
	public int? HomeId { get; set; }

	/// <summary>Home Status (R: Ready to Build; UC: Under Construction; A: Available Now; M: Model; S: Sold)</summary>
	[GraphQLField("homeStatus")]
	public string HomeStatus { get; set; }

	/// <summary>Home Types: SF - Single Family, MF - Multi Family</summary>
	[GraphQLField("homeType")]
	public string HomeType { get; set; }

	/// <summary>Id</summary>
	[GraphQLField("id")]
	public string Id { get; set; }

	/// <summary>Image Count</summary>
	[GraphQLField("imageCount")]
	public int? ImageCount { get; set; }

	/// <summary>Interior Image</summary>
	[GraphQLField("intImage")]
	public string IntImage { get; set; }

	/// <summary>Is Adult</summary>
	[GraphQLField("isAdult")]
	public bool? IsAdult { get; set; }

	/// <summary>Is Basic</summary>
	[GraphQLField("isBasic")]
	public bool? IsBasic { get; set; }

	/// <summary>Is Condo Type</summary>
	[GraphQLField("isCondoType")]
	public bool? IsCondoType { get; set; }

	/// <summary>Is Gated </summary>
	[GraphQLField("isGated")]
	public bool? IsGated { get; set; }

	/// <summary>Has Green</summary>
	[GraphQLField("isGreen")]
	public bool? IsGreen { get; set; }

	/// <summary>Is Hot Home</summary>
	[GraphQLField("isHotHome")]
	public bool? IsHotHome { get; set; }

	/// <summary>Used to know if the land is already excluded from the home price</summary>
	[GraphQLField("isLandExcluded")]
	public bool? IsLandExcluded { get; set; }

	/// <summary>Is Luxury</summary>
	[GraphQLField("isLuxury")]
	public bool? IsLuxury { get; set; }

	/// <summary>Is Master </summary>
	[GraphQLField("isMaster")]
	public bool? IsMaster { get; set; }

	/// <summary>Is Quick Move In</summary>
	[GraphQLField("isQmi")]
	public bool? IsQmi { get; set; }

	/// <summary>Is Sold</summary>
	[GraphQLField("isSold")]
	public bool? IsSold { get; set; }

	/// <summary>Is Spec</summary>
	[GraphQLField("isSpec")]
	public bool? IsSpec { get; set; }

	/// <summary>Is Town Home</summary>
	[GraphQLField("isTownHome")]
	public bool? IsTownHome { get; set; }

	/// <summary>Is Urban</summary>
	[GraphQLField("isUrban")]
	public bool? IsUrban { get; set; }

	/// <summary>Latitude</summary>
	[GraphQLField("latitude")]
	public decimal? Latitude { get; set; }

	/// <summary>Listing Type</summary>
	[GraphQLField("listingType")]
	public string ListingType { get; set; }

	/// <summary>Living Areas</summary>
	[GraphQLField("livAreas")]
	public int? LivAreas { get; set; }

	/// <summary>Logo Url Med</summary>
	[GraphQLField("logoUrlMed")]
	public string LogoUrlMed { get; set; }

	/// <summary>Logo Url Small</summary>
	[GraphQLField("logoUrlSmall")]
	public string LogoUrlSmall { get; set; }

	/// <summary>Longitude</summary>
	[GraphQLField("longitude")]
	public decimal? Longitude { get; set; }

	/// <summary>Market Id</summary>
	[GraphQLField("marketId")]
	public int? MarketId { get; set; }

	/// <summary>Market Name</summary>
	[GraphQLField("marketName")]
	public string MarketName { get; set; }

	/// <summary>Master Bed Location</summary>
	[GraphQLField("masterBrLoc")]
	public int? MasterBrLoc { get; set; }

	/// <summary>Move in date</summary>
	[GraphQLField("moveInDate")]
	public DateTime? MoveInDate { get; set; }

	/// <summary>Plan name</summary>
	[GraphQLField("name")]
	public string Name { get; set; }

	/// <summary>Parent Community Id</summary>
	[GraphQLField("parentCommId")]
	public int? ParentCommId { get; set; }

	/// <summary>Phone</summary>
	[GraphQLField("phone")]
	public string Phone { get; set; }

	/// <summary>Plan Id</summary>
	[GraphQLField("planId")]
	public int? PlanId { get; set; }

	/// <summary>Plan name</summary>
	[GraphQLField("planName")]
	public string PlanName { get; set; }

	/// <summary>Plan Viewer Path</summary>
	[GraphQLField("planViewerPath")]
	public string PlanViewerPath { get; set; }

	/// <summary>Higher Price</summary>
	[GraphQLField("prHi")]
	public decimal? PrHi { get; set; }

	/// <summary>Minimum Price</summary>
	[GraphQLField("prLo")]
	public decimal? PrLo { get; set; }

	/// <summary>Project Type: (BYL: Boyl; CST: Custom; MFR: Manufactured)</summary>
	[GraphQLField("projectTypeCode")]
	public string ProjectTypeCode { get; set; }

	/// <summary>Schools</summary>
	[GraphQLField("schools")]
	public List<GraphQLApiSchool> Schools { get; set; }

	/// <summary>Maximum Square Feet</summary>
	[GraphQLField("sftHi")]
	public int? SftHi { get; set; }

	/// <summary>Minimum Square Feet</summary>
	[GraphQLField("sftLo")]
	public int? SftLo { get; set; }

	/// <summary>Sold Date</summary>
	[GraphQLField("soldDate")]
	public DateTime? SoldDate { get; set; }

	/// <summary>Sold Price</summary>
	[GraphQLField("soldPrice")]
	public decimal? SoldPrice { get; set; }

	/// <summary>Sort Key</summary>
	[GraphQLField("sortKey")]
	public int? SortKey { get; set; }

	/// <summary>Spec Id</summary>
	[GraphQLField("specId")]
	public int? SpecId { get; set; }

	/// <summary>Spec Sale Status (C: Contract Pending; S: Sold; Null: Available)</summary>
	[GraphQLField("specSaleStatus")]
	public string SpecSaleStatus { get; set; }

	/// <summary>Spec Type</summary>
	[GraphQLField("specType")]
	public string SpecType { get; set; }

	/// <summary>State</summary>
	[GraphQLField("state")]
	public string State { get; set; }

	/// <summary>Stories</summary>
	[GraphQLField("stories")]
	public decimal? Stories { get; set; }

	/// <summary>Spotlight Thumbnail Image</summary>
	[GraphQLField("thumb")]
	public string Thumb { get; set; }

	/// <summary>Spotlight Thumbnail secundary Image</summary>
	[GraphQLField("thumb2")]
	public string Thumb2 { get; set; }

	/// <summary>Virtual Tour</summary>
	[GraphQLField("virtualTourPath")]
	public string VirtualTourPath { get; set; }

	/// <summary>Zip Code</summary>
	[GraphQLField("zip")]
	public string Zip { get; set; }

}

public partial class GraphQLApiAmenity
{
	/// <summary>Id</summary>
	[GraphQLField("id")]
	public int? Id { get; set; }

	/// <summary>Amenity Name</summary>
	[GraphQLField("name")]
	public string Name { get; set; }

}

public partial class GraphQLResultHomeCounts
{
	/// <summary>Average Price</summary>
	[GraphQLField("averagePrice")]
	public decimal? AveragePrice { get; set; }

	/// <summary>Basic Listing Count</summary>
	[GraphQLField("blCount")]
	public int? BlCount { get; set; }

	/// <summary>Boyl Community Count</summary>
	[GraphQLField("boylCommCount")]
	public int? BoylCommCount { get; set; }

	/// <summary>Boyl Home Count</summary>
	[GraphQLField("boylHomeCount")]
	public int? BoylHomeCount { get; set; }

	/// <summary>Builder count</summary>
	[GraphQLField("builderCount")]
	public int? BuilderCount { get; set; }

	/// <summary>Communities All Homes Count</summary>
	[GraphQLField("commAllHomeCount")]
	public int? CommAllHomeCount { get; set; }

	/// <summary>Communities All Luxury Count</summary>
	[GraphQLField("commAllLuxuryCount")]
	public int? CommAllLuxuryCount { get; set; }

	/// <summary>Communities Count</summary>
	[GraphQLField("commCount")]
	public int? CommCount { get; set; }

	/// <summary>Facets</summary>
	[GraphQLField("facets")]
	public GraphQLApiFacets Facets { get; set; }

	/// <summary>Homes Count</summary>
	[GraphQLField("homeCount")]
	public int? HomeCount { get; set; }

	/// <summary>Hot Deals Count</summary>
	[GraphQLField("hotDealsCount")]
	public int? HotDealsCount { get; set; }

	/// <summary>Model Home Count</summary>
	[GraphQLField("modelHomeCount")]
	public int? ModelHomeCount { get; set; }

	/// <summary>Nby Community Count</summary>
	[GraphQLField("nbyCommCount")]
	public int? NbyCommCount { get; set; }

	/// <summary>Paid Community Count</summary>
	[GraphQLField("paidCommCount")]
	public int? PaidCommCount { get; set; }

	/// <summary>Paid Nby Community Count</summary>
	[GraphQLField("paidNbyCommCount")]
	public int? PaidNbyCommCount { get; set; }

	/// <summary>Quick Move In Count</summary>
	[GraphQLField("qmiCount")]
	public int? QmiCount { get; set; }

	/// <summary>Radius</summary>
	[GraphQLField("radius")]
	public int? Radius { get; set; }

	/// <summary>Square Feet Range</summary>
	[GraphQLField("sftRange")]
	public string SftRange { get; set; }

	/// <summary>Sold Homes Count</summary>
	[GraphQLField("soldHomeCount")]
	public int? SoldHomeCount { get; set; }

	/// <summary>Total Brands</summary>
	[GraphQLField("totalBrands")]
	public int? TotalBrands { get; set; }

	/// <summary>Total Plans</summary>
	[GraphQLField("totalPlans")]
	public int? TotalPlans { get; set; }

}

public partial class GraphQLsearchAllImages
{
	/// <summary>Address</summary>
	[GraphQLField("addr")]
	public string Addr { get; set; }

	/// <summary>Address Plan</summary>
	[GraphQLField("addressPlan")]
	public string AddressPlan { get; set; }

	/// <summary>Amenities</summary>
	[GraphQLField("amenities")]
	public List<GraphQLApiAmenity> Amenities { get; set; }

	/// <summary>Maximum Baths Quantity</summary>
	[GraphQLField("baHi")]
	public int? BaHi { get; set; }

	/// <summary>Minimum Baths Quantity</summary>
	[GraphQLField("baLo")]
	public int? BaLo { get; set; }

	/// <summary>Community Type (C: Coming soon; G: Gated; N: Normal; X: Closed)</summary>
	[GraphQLField("bcType")]
	public string BcType { get; set; }

	/// <summary>Brand object</summary>
	[GraphQLField("brand")]
	public GraphQLApiBrand Brand { get; set; }

	/// <summary>Brand Id</summary>
	[GraphQLField("brandId")]
	public int? BrandId { get; set; }

	/// <summary>Brand Name</summary>
	[GraphQLField("brandName")]
	public string BrandName { get; set; }

	/// <summary>Maximum Bedrooms Quantity</summary>
	[GraphQLField("brHi")]
	public int? BrHi { get; set; }

	/// <summary>Minimum Bedrooms Quantity</summary>
	[GraphQLField("brLo")]
	public int? BrLo { get; set; }

	/// <summary>Builder Id</summary>
	[GraphQLField("builderId")]
	public int? BuilderId { get; set; }

	/// <summary>Builder Name</summary>
	[GraphQLField("builderName")]
	public string BuilderName { get; set; }

	/// <summary>City</summary>
	[GraphQLField("city")]
	public string City { get; set; }

	/// <summary>Community Id</summary>
	[GraphQLField("commId")]
	public int? CommId { get; set; }

	/// <summary>Community Status (N: Active; C: Coming Soon; G: Grand Opening; X: Close Out; CG: ComingSoonGrandOpening)</summary>
	[GraphQLField("commStatus")]
	public string CommStatus { get; set; }

	/// <summary>Community Name</summary>
	[GraphQLField("communityName")]
	public string CommunityName { get; set; }

	/// <summary>County</summary>
	[GraphQLField("county")]
	public string County { get; set; }

	/// <summary>Distance</summary>
	[GraphQLField("distance")]
	public decimal? Distance { get; set; }

	/// <summary>School Districts</summary>
	[GraphQLField("districts")]
	public List<GraphQLApiDisctrict> Districts { get; set; }

	/// <summary>Elevation path</summary>
	[GraphQLField("elevPath")]
	public string ElevPath { get; set; }

	/// <summary>Floor plan path</summary>
	[GraphQLField("floorPlanPath")]
	public string FloorPlanPath { get; set; }

	/// <summary>Maximum Garages Quantity</summary>
	[GraphQLField("garageHi")]
	public decimal? GarageHi { get; set; }

	/// <summary>Minimum Garages Quantity</summary>
	[GraphQLField("garageLo")]
	public decimal? GarageLo { get; set; }

	/// <summary>Half Bathrooms</summary>
	[GraphQLField("halfBa")]
	public int? HalfBa { get; set; }

	/// <summary>Has Agent Promotion</summary>
	[GraphQLField("hasAgePromo")]
	public bool? HasAgePromo { get; set; }

	/// <summary>Has Age Rest</summary>
	[GraphQLField("hasAgeRest")]
	public bool? HasAgeRest { get; set; }

	/// <summary>Has Builder Review</summary>
	[GraphQLField("hasBuilderReview")]
	public bool? HasBuilderReview { get; set; }

	/// <summary>Has Community Center</summary>
	[GraphQLField("hasCommCenter")]
	public bool? HasCommCenter { get; set; }

	/// <summary>Has Consumer Promo</summary>
	[GraphQLField("hasConPromo")]
	public bool? HasConPromo { get; set; }

	/// <summary>Has Event </summary>
	[GraphQLField("hasEvent")]
	public bool? HasEvent { get; set; }

	/// <summary>Has Golf</summary>
	[GraphQLField("hasGolf")]
	public bool? HasGolf { get; set; }

	/// <summary>Has Nature areas</summary>
	[GraphQLField("hasNature")]
	public bool? HasNature { get; set; }

	/// <summary>Has Park</summary>
	[GraphQLField("hasPark")]
	public bool? HasPark { get; set; }

	/// <summary>Has Pool</summary>
	[GraphQLField("hasPool")]
	public bool? HasPool { get; set; }

	/// <summary>Has Sport</summary>
	[GraphQLField("hasSport")]
	public bool? HasSport { get; set; }

	/// <summary>Has Video</summary>
	[GraphQLField("hasVideo")]
	public bool? HasVideo { get; set; }

	/// <summary>Has View</summary>
	[GraphQLField("hasView")]
	public bool? HasView { get; set; }

	/// <summary>Has View Only</summary>
	[GraphQLField("hasViewOnly")]
	public bool? HasViewOnly { get; set; }

	/// <summary>Has Water Front Only</summary>
	[GraphQLField("hasWaterFrontOnly")]
	public bool? HasWaterFrontOnly { get; set; }

	/// <summary>Home Id, Stores the Spec Id pr the plan Id according to the listing returned</summary>
	[GraphQLField("homeId")]
	public int? HomeId { get; set; }

	/// <summary>Home Status (R: Ready to Build; UC: Under Construction; A: Available Now; M: Model; S: Sold)</summary>
	[GraphQLField("homeStatus")]
	public string HomeStatus { get; set; }

	/// <summary>Home Types: SF - Single Family, MF - Multi Family</summary>
	[GraphQLField("homeType")]
	public string HomeType { get; set; }

	/// <summary>Id</summary>
	[GraphQLField("id")]
	public string Id { get; set; }

	/// <summary>Image Count</summary>
	[GraphQLField("imageCount")]
	public int? ImageCount { get; set; }

	/// <summary>Image information</summary>
	[GraphQLField("imageInfo")]
	public GraphQLApiImageSearchInfo ImageInfo { get; set; }

	/// <summary>Image Listing Id</summary>
	[GraphQLField("imageListingId")]
	public int? ImageListingId { get; set; }

	/// <summary>Image Name</summary>
	[GraphQLField("imageName")]
	public string ImageName { get; set; }

	/// <summary>Interior Image</summary>
	[GraphQLField("intImage")]
	public string IntImage { get; set; }

	/// <summary>Is Adult</summary>
	[GraphQLField("isAdult")]
	public bool? IsAdult { get; set; }

	/// <summary>Is Basic</summary>
	[GraphQLField("isBasic")]
	public bool? IsBasic { get; set; }

	/// <summary>Is Condo Type</summary>
	[GraphQLField("isCondoType")]
	public bool? IsCondoType { get; set; }

	/// <summary>Is Gated </summary>
	[GraphQLField("isGated")]
	public bool? IsGated { get; set; }

	/// <summary>Has Green</summary>
	[GraphQLField("isGreen")]
	public bool? IsGreen { get; set; }

	/// <summary>Is Hot Home</summary>
	[GraphQLField("isHotHome")]
	public bool? IsHotHome { get; set; }

	/// <summary>Used to know if the land is already excluded from the home price</summary>
	[GraphQLField("isLandExcluded")]
	public bool? IsLandExcluded { get; set; }

	/// <summary>Is Luxury</summary>
	[GraphQLField("isLuxury")]
	public bool? IsLuxury { get; set; }

	/// <summary>Is Master </summary>
	[GraphQLField("isMaster")]
	public bool? IsMaster { get; set; }

	/// <summary>Is Quick Move In</summary>
	[GraphQLField("isQmi")]
	public bool? IsQmi { get; set; }

	/// <summary>Is Sold</summary>
	[GraphQLField("isSold")]
	public bool? IsSold { get; set; }

	/// <summary>Is Spec</summary>
	[GraphQLField("isSpec")]
	public bool? IsSpec { get; set; }

	/// <summary>Is Town Home</summary>
	[GraphQLField("isTownHome")]
	public bool? IsTownHome { get; set; }

	/// <summary>Is Urban</summary>
	[GraphQLField("isUrban")]
	public bool? IsUrban { get; set; }

	/// <summary>Latitude</summary>
	[GraphQLField("latitude")]
	public decimal? Latitude { get; set; }

	/// <summary>Listing Type</summary>
	[GraphQLField("listingType")]
	public string ListingType { get; set; }

	/// <summary>Living Areas</summary>
	[GraphQLField("livAreas")]
	public int? LivAreas { get; set; }

	/// <summary>Logo Url Med</summary>
	[GraphQLField("logoUrlMed")]
	public string LogoUrlMed { get; set; }

	/// <summary>Logo Url Small</summary>
	[GraphQLField("logoUrlSmall")]
	public string LogoUrlSmall { get; set; }

	/// <summary>Longitude</summary>
	[GraphQLField("longitude")]
	public decimal? Longitude { get; set; }

	/// <summary>Market Id</summary>
	[GraphQLField("marketId")]
	public int? MarketId { get; set; }

	/// <summary>Market Name</summary>
	[GraphQLField("marketName")]
	public string MarketName { get; set; }

	/// <summary>Master Bed Location</summary>
	[GraphQLField("masterBrLoc")]
	public int? MasterBrLoc { get; set; }

	/// <summary>Move in date</summary>
	[GraphQLField("moveInDate")]
	public DateTime? MoveInDate { get; set; }

	/// <summary>Plan name</summary>
	[GraphQLField("name")]
	public string Name { get; set; }

	/// <summary>Parent Community Id</summary>
	[GraphQLField("parentCommId")]
	public int? ParentCommId { get; set; }

	/// <summary>Phone</summary>
	[GraphQLField("phone")]
	public string Phone { get; set; }

	/// <summary>Plan Id</summary>
	[GraphQLField("planId")]
	public int? PlanId { get; set; }

	/// <summary>Plan name</summary>
	[GraphQLField("planName")]
	public string PlanName { get; set; }

	/// <summary>Plan Viewer Path</summary>
	[GraphQLField("planViewerPath")]
	public string PlanViewerPath { get; set; }

	/// <summary>Higher Price</summary>
	[GraphQLField("prHi")]
	public decimal? PrHi { get; set; }

	/// <summary>Minimum Price</summary>
	[GraphQLField("prLo")]
	public decimal? PrLo { get; set; }

	/// <summary>Project Type: (BYL: Boyl; CST: Custom; MFR: Manufactured)</summary>
	[GraphQLField("projectTypeCode")]
	public string ProjectTypeCode { get; set; }

	/// <summary>Schools</summary>
	[GraphQLField("schools")]
	public List<GraphQLApiSchool> Schools { get; set; }

	/// <summary>Maximum Square Feet</summary>
	[GraphQLField("sftHi")]
	public int? SftHi { get; set; }

	/// <summary>Minimum Square Feet</summary>
	[GraphQLField("sftLo")]
	public int? SftLo { get; set; }

	/// <summary>Sold Date</summary>
	[GraphQLField("soldDate")]
	public DateTime? SoldDate { get; set; }

	/// <summary>Sold Price</summary>
	[GraphQLField("soldPrice")]
	public decimal? SoldPrice { get; set; }

	/// <summary>Sort Key</summary>
	[GraphQLField("sortKey")]
	public int? SortKey { get; set; }

	/// <summary>Spec Id</summary>
	[GraphQLField("specId")]
	public int? SpecId { get; set; }

	/// <summary>Spec Sale Status (C: Contract Pending; S: Sold; Null: Available)</summary>
	[GraphQLField("specSaleStatus")]
	public string SpecSaleStatus { get; set; }

	/// <summary>Spec Type</summary>
	[GraphQLField("specType")]
	public string SpecType { get; set; }

	/// <summary>State</summary>
	[GraphQLField("state")]
	public string State { get; set; }

	/// <summary>Stories</summary>
	[GraphQLField("stories")]
	public decimal? Stories { get; set; }

	/// <summary>Spotlight Thumbnail Image</summary>
	[GraphQLField("thumb")]
	public string Thumb { get; set; }

	/// <summary>Spotlight Thumbnail secundary Image</summary>
	[GraphQLField("thumb2")]
	public string Thumb2 { get; set; }

	/// <summary>Virtual Tour</summary>
	[GraphQLField("virtualTourPath")]
	public string VirtualTourPath { get; set; }

	/// <summary>Zip Code</summary>
	[GraphQLField("zip")]
	public string Zip { get; set; }

}

public partial class GraphQLApiImageSearchInfo
{
	/// <summary>Image Category</summary>
	[GraphQLField("category")]
	public string Category { get; set; }

	/// <summary>Image Color Type</summary>
	[GraphQLField("colorType")]
	public string ColorType { get; set; }

	/// <summary>Image Height</summary>
	[GraphQLField("height")]
	public int? Height { get; set; }

	/// <summary>Image Id</summary>
	[GraphQLField("id")]
	public int? Id { get; set; }

	/// <summary>Image Path</summary>
	[GraphQLField("path")]
	public string Path { get; set; }

	/// <summary>Image Sequence</summary>
	[GraphQLField("sequence")]
	public int? Sequence { get; set; }

	/// <summary>Image Type Code</summary>
	[GraphQLField("type")]
	public string Type { get; set; }

	/// <summary>Image Width</summary>
	[GraphQLField("width")]
	public int? Width { get; set; }

}

public partial class GraphQLLocations
{
	[GraphQLField("city")]
	public string City { get; set; }

	[GraphQLField("id")]
	public int? Id { get; set; }

	/// <summary>Latitude</summary>
	[GraphQLField("latitude")]
	public decimal? Latitude { get; set; }

	/// <summary>Longitude</summary>
	[GraphQLField("longitude")]
	public decimal? Longitude { get; set; }

	[GraphQLField("marketId")]
	public int? MarketId { get; set; }

	[GraphQLField("marketName")]
	public string MarketName { get; set; }

	[GraphQLField("name")]
	public string Name { get; set; }

	[GraphQLField("state")]
	public string State { get; set; }

	[GraphQLField("type")]
	public string Type { get; set; }

}

[JsonConverter(typeof(GraphQLEnumConverter))]
public enum GraphQLLocationTypeInputType
{
	/// <summary>Market</summary>
	Market,
	/// <summary>City</summary>
	City,
	/// <summary>County</summary>
	County,
	/// <summary>Zip</summary>
	Zip,
	/// <summary>Community</summary>
	Community,
	/// <summary>Developer</summary>
	Developer,
	/// <summary>Synthetic Geo</summary>
	SyntheticGeo,
	/// <summary>Brand</summary>
	Brand,
	/// <summary>Custom Location Community (CSL)</summary>
	Location
}

[JsonConverter(typeof(GraphQLEnumConverter))]
public enum GraphQLSortByInputType
{
	/// <summary>StartsWith</summary>
	StartsWith,
	/// <summary>Contains</summary>
	Contains,
	/// <summary>Default</summary>
	Default
}

public partial class GraphQLSearchAllPreferred
{
	/// <summary>Account Id</summary>
	[GraphQLField("accountId")]
	public int? AccountId { get; set; }

	/// <summary>Listing Id</summary>
	[GraphQLField("listingId")]
	public int? ListingId { get; set; }

	/// <summary>Listing Type</summary>
	[GraphQLField("listingType")]
	public GraphQLListingType ListingType { get; set; }

	/// <summary>Spec Id</summary>
	[GraphQLField("specId")]
	public int? SpecId { get; set; }

}

[JsonConverter(typeof(GraphQLEnumConverter))]
public enum GraphQLListingType
{
	/// <summary>Specification</summary>
	S,
	/// <summary>Plan</summary>
	P
}

[Dependency(DependencyScope.Transient)]
public sealed class GraphQLApiConsumer : IGraphQLApiConsumer
{
	private readonly IGraphQLClient _graphQlHttpClient;

	public GraphQLApiConsumer()
    {
        _graphQlHttpClient = new GraphQLHttpClient();
    }

    public GraphQLApiConsumer(IGraphQLClient graphQlHttpClient)
    {
        _graphQlHttpClient = graphQlHttpClient;
    }

    /// <summary>Gets or sets URL of the GraphQL API, when it is assign a new HttpClient id created./</summary>
    /// <value>The API URL.</value>
    public string ApiUrl {
        get => this._graphQlHttpClient.EndPoint.ToString();
        set => this._graphQlHttpClient.EndPoint = new Uri(value) ;
    }

    private void EnsureGraphQLUrl()
    {
        if(_graphQlHttpClient?.EndPoint == null)
            throw new Exception("The Api URL was not set, please add GraphQL source");
    }
	/// <param name="commId">Community Id</param>
	/// <param name="first">First</param>
	/// <param name="marketId">Market Id</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("communities")]
	public GraphQLQuery<GraphQLApiCommunity>Communities (string sessionToken, int? commId = null, int? first = null, int? marketId = null, Func<GraphQLApiCommunity, GraphQLApiCommunity> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return new GraphQLQuery<GraphQLApiCommunity>(this._graphQlHttpClient, true, currentContext, fields, sessionToken, commId, first, marketId);
	}

	/// <param name="commId">Community Id</param>
	/// <param name="first">First</param>
	/// <param name="marketId">Market Id</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("communities")]
	public Task<List<GraphQLApiCommunity>>CommunitiesAsync (string sessionToken, int? commId = null, int? first = null, int? marketId = null, Func<GraphQLApiCommunity, GraphQLApiCommunity> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return Task.Run(() => new GraphQLQuery<GraphQLApiCommunity>(this._graphQlHttpClient, true, currentContext, fields, sessionToken, commId, first, marketId).ToList());
	}

	/// <param name="id">Id</param>
	/// <param name="city">City</param>
	/// <param name="marketId">Market Id</param>
	/// <param name="marketName">Market Name</param>
	/// <param name="partnerId">Partner Id</param>
	/// <param name="searchTerm">Search Term</param>
	/// <param name="pageSize">Page Size</param>
	/// <param name="state">Search Term</param>
	/// <param name="types">Types</param>
	/// <param name="exactSearch">Exact Search by Search Term Parameter</param>
	/// <param name="sortBy">Sort By</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("locations")]
	public GraphQLQuery<GraphQLLocations>Locations (string sessionToken, int? partnerId, int? id = null, string city = null, int? marketId = null, string marketName = null, string searchTerm = null, int? pageSize = 20, string state = null, GraphQLLocationTypeInputType[] types = null, bool? exactSearch = null, GraphQLSortByInputType? sortBy = null, Func<GraphQLLocations, GraphQLLocations> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return new GraphQLQuery<GraphQLLocations>(this._graphQlHttpClient, true, currentContext, fields, sessionToken, partnerId, id, city, marketId, marketName, searchTerm, pageSize, state, types, exactSearch, sortBy);
	}

	/// <param name="partnerId">Partner Id</param>
	/// <param name="id">Id</param>
	/// <param name="city">City</param>
	/// <param name="marketId">Market Id</param>
	/// <param name="marketName">Market Name</param>
	/// <param name="searchTerm">Search Term</param>
	/// <param name="pageSize">Page Size</param>
	/// <param name="state">Search Term</param>
	/// <param name="types">Types</param>
	/// <param name="exactSearch">Exact Search by Search Term Parameter</param>
	/// <param name="sortBy">Sort By</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("locations")]
	public Task<List<GraphQLLocations>>LocationsAsync (string sessionToken, int? partnerId, int? id = null, string city = null, int? marketId = null, string marketName = null, string searchTerm = null, int? pageSize = 20, string state = null, GraphQLLocationTypeInputType[] types = null, bool? exactSearch = null, GraphQLSortByInputType? sortBy = null, Func<GraphQLLocations, GraphQLLocations> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return Task.Run(() => new GraphQLQuery<GraphQLLocations>(this._graphQlHttpClient, true, currentContext, fields, sessionToken, partnerId, id, city, marketId, marketName, searchTerm, pageSize, state, types, exactSearch, sortBy).ToList());
	}

	/// <param name="marketId">Market Id</param>
	/// <param name="partnerId">Partner Id</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("market")]
	public GraphQLQuery<GraphQLApiMarket>Market (string sessionToken, int? marketId = 0, int? partnerId = 0, Func<GraphQLApiMarket, GraphQLApiMarket> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return new GraphQLQuery<GraphQLApiMarket>(this._graphQlHttpClient, false, currentContext, fields, sessionToken, marketId, partnerId);
	}

	/// <param name="marketId">Market Id</param>
	/// <param name="partnerId">Partner Id</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("market")]
	public Task<List<GraphQLApiMarket>>MarketAsync (string sessionToken, int? marketId = 0, int? partnerId = 0, Func<GraphQLApiMarket, GraphQLApiMarket> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return Task.Run(() => new GraphQLQuery<GraphQLApiMarket>(this._graphQlHttpClient, false, currentContext, fields, sessionToken, marketId, partnerId).ToList());
	}

	/// <param name="brandId">Brand Id</param>
	/// <param name="partnerId">Partner Id</param>
	/// <param name="state">State</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("markets")]
	public GraphQLQuery<GraphQLApiMarket>Markets (string sessionToken, int? brandId = -1, int? partnerId = 0, string state = null, Func<GraphQLApiMarket, GraphQLApiMarket> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return new GraphQLQuery<GraphQLApiMarket>(this._graphQlHttpClient, true, currentContext, fields, sessionToken, brandId, partnerId, state);
	}

	/// <param name="brandId">Brand Id</param>
	/// <param name="partnerId">Partner Id</param>
	/// <param name="state">State</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("markets")]
	public Task<List<GraphQLApiMarket>>MarketsAsync (string sessionToken, int? brandId = -1, int? partnerId = 0, string state = null, Func<GraphQLApiMarket, GraphQLApiMarket> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return Task.Run(() => new GraphQLQuery<GraphQLApiMarket>(this._graphQlHttpClient, true, currentContext, fields, sessionToken, brandId, partnerId, state).ToList());
	}

	/// <param name="first">First</param>
	/// <param name="offset">Skip first N records</param>
	/// <param name="partnerId">Partner Id</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("partners")]
	public GraphQLQuery<GraphQLApiPartner>Partners (string sessionToken, int? first = null, int? offset = null, int? partnerId = null, Func<GraphQLApiPartner, GraphQLApiPartner> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return new GraphQLQuery<GraphQLApiPartner>(this._graphQlHttpClient, true, currentContext, fields, sessionToken, first, offset, partnerId);
	}

	/// <param name="first">First</param>
	/// <param name="offset">Skip first N records</param>
	/// <param name="partnerId">Partner Id</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("partners")]
	public Task<List<GraphQLApiPartner>>PartnersAsync (string sessionToken, int? first = null, int? offset = null, int? partnerId = null, Func<GraphQLApiPartner, GraphQLApiPartner> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return Task.Run(() => new GraphQLQuery<GraphQLApiPartner>(this._graphQlHttpClient, true, currentContext, fields, sessionToken, first, offset, partnerId).ToList());
	}

	/// <param name="alphaResults">Alpha Results</param>
	/// <param name="aPromo">Has Agent Promo</param>
	/// <param name="basicListingToTheEnd">Basic Listing to End</param>
	/// <param name="bath">Bath</param>
	/// <param name="bcType">Community Type (C: Coming soon; G: Gated; N: Normal; X: Closed)</param>
	/// <param name="bed">Bed</param>
	/// <param name="brandId">Brand Id</param>
	/// <param name="brandIds">Brands</param>
	/// <param name="builderId">Builder Id</param>
	/// <param name="builderIds">Builder Ids</param>
	/// <param name="cities">Cities</param>
	/// <param name="city">City</param>
	/// <param name="commId">Community Id</param>
	/// <param name="parentCommId">Community Id</param>
	/// <param name="commIds">Community Ids</param>
	/// <param name="commStatus">Community Status (N: Active; C: Coming Soon; G: Grand Opening; X: Close Out; CG: ComingSoonGrandOpening)</param>
	/// <param name="communityName">Community Name</param>
	/// <param name="counties">Counties</param>
	/// <param name="county">County</param>
	/// <param name="cPromo">Has Consumer Promo</param>
	/// <param name="custom">Custom</param>
	/// <param name="customBuilderLocations">Custom Builder Locations</param>
	/// <param name="customResults">Custom Results</param>
	/// <param name="dist">Distance</param>
	/// <param name="districtIds">District Ids</param>
	/// <param name="excludeBasiCommunities">Exclude Basic Communities</param>
	/// <param name="excludeRegularComms">Exclude Basic Communities</param>
	/// <param name="excludeCommunitiesByBuilderId">Exclude Communities by Builder Id</param>
	/// <param name="first">First</param>
	/// <param name="garage">Garage</param>
	/// <param name="geoPolygon">Array or coordinates to apply polygon/draw on map search</param>
	/// <param name="halfBath">Half Bath</param>
	/// <param name="homeStatus">Home Status (R: Ready to Build; UC: Under Construction; A: Available Now; M: Model; S: Sold)</param>
	/// <param name="includeMpcs">Include Master Plan Communities</param>
	/// <param name="includeBasicMpc">Include Basic Master Plan Community</param>
	/// <param name="listingIds">LivingIds</param>
	/// <param name="imageIds">LivingIds</param>
	/// <param name="livAreas">Living Areas</param>
	/// <param name="marketId">Market Id</param>
	/// <param name="marketIds">Market Ids</param>
	/// <param name="masterBrLoc">Master Bed Location</param>
	/// <param name="maxLat">Max Latitude</param>
	/// <param name="maxLng">Max Longitude</param>
	/// <param name="mfr">Manufactured</param>
	/// <param name="minLat">Min Latitude</param>
	/// <param name="minLng">Min Longitude</param>
	/// <param name="moveInDate">Move in date</param>
	/// <param name="noBoyl">No Boyl</param>
	/// <param name="numStory">Num Story</param>
	/// <param name="offset">Skip first N records</param>
	/// <param name="originLat">Origin Latitude</param>
	/// <param name="originLng">Origin Longitude</param>
	/// <param name="page">Page</param>
	/// <param name="pageSize">Page Size</param>
	/// <param name="partnerId">Partner Id</param>
	/// <param name="planId">Plan Id</param>
	/// <param name="planIds">Plan Ids</param>
	/// <param name="planName">Plan name</param>
	/// <param name="postalCode">Postal Code</param>
	/// <param name="postalCodes">Postal Codes</param>
	/// <param name="prHi">Higher Price</param>
	/// <param name="prLo">Minimum Price</param>
	/// <param name="projectTypeCode">Project Type: (BYL: Boyl; CST: Custom; MFR: Manufactured)</param>
	/// <param name="promo">Has Promo</param>
	/// <param name="qmi">Quick Move In</param>
	/// <param name="radius">Radius</param>
	/// <param name="radiusUnit">Raius Unit</param>
	/// <param name="sfhigh">Maximum Square Feet</param>
	/// <param name="sflow">Minimum Square Feet</param>
	/// <param name="soldDataToken">If provided and it's valid, it will return Sold Data info, Otherwise will filter out sold data records even if HomeStatus: S is specified</param>
	/// <param name="soldFromDate">Sold From Date</param>
	/// <param name="sort">List of fields to sort</param>
	/// <param name="sortFirstBy">Sort First By</param>
	/// <param name="specId">Spec Id</param>
	/// <param name="specIds">Spec Ids</param>
	/// <param name="specTypes">Spec Type</param>
	/// <param name="state">State</param>
	/// <param name="story">Story</param>
	/// <param name="adult">Is for Adult</param>
	/// <param name="ageRestricted">Age Restricted</param>
	/// <param name="gated">Gated</param>
	/// <param name="golf">Golf</param>
	/// <param name="green">Green</param>
	/// <param name="nature">Has Nature areas</param>
	/// <param name="parks">Parks</param>
	/// <param name="pool">Pool</param>
	/// <param name="sports">Sports</param>
	/// <param name="views">Views</param>
	/// <param name="waterfront">Water front</param>
	/// <param name="hasCommCenter">Has Community Center</param>
	/// <param name="hasEvent">Has Event </param>
	/// <param name="hasConPromo">Has Consumer Promo</param>
	/// <param name="hasAgePromo">Has Agent Promotion</param>
	/// <param name="hasHotDeals">Has Hot Home</param>
	/// <param name="hasLuxuryHomes">Has Hot Home</param>
	/// <param name="hasViewOnly">Has View Only</param>
	/// <param name="hasBuilderReview">Has Builder Review</param>
	/// <param name="isHotHome">Is Hot Home</param>
	/// <param name="isMaster">Is Master </param>
	/// <param name="isUrban">Is Urban</param>
	/// <param name="isCondoType">Is Condo Type</param>
	/// <param name="isMultiFamily">Whether the community has at least one multi family home, or when the home type is MF at home level</param>
	/// <param name="isSingleFamily">Whether the community has at least one single family home, or the home is SF at home level</param>
	/// <param name="isTownHome">Is Town Home</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("searchAllComms")]
	public GraphQLQuery<GraphQLSearchCommunities>SearchAllComms (string sessionToken, int? partnerId, bool? alphaResults = null, bool? aPromo = null, bool? basicListingToTheEnd = null, int? bath = null, string bcType = null, int? bed = null, int? brandId = null, int?[] brandIds = null, int? builderId = null, string builderIds = null, string cities = null, string city = null, int? commId = null, int? parentCommId = null, string commIds = null, string commStatus = null, string communityName = null, string counties = null, string county = null, bool? cPromo = null, bool? custom = null, bool? customBuilderLocations = null, bool? customResults = null, float? dist = null, string districtIds = null, bool? excludeBasiCommunities = null, bool? excludeRegularComms = null, bool? excludeCommunitiesByBuilderId = null, int? first = null, int? garage = null, GraphQLGeoPolygonInputType[] geoPolygon = null, int? halfBath = null, string homeStatus = null, bool? includeMpcs = null, bool? includeBasicMpc = null, int?[] listingIds = null, int?[] imageIds = null, int? livAreas = null, int? marketId = null, string marketIds = null, int? masterBrLoc = null, float? maxLat = null, float? maxLng = null, bool? mfr = null, float? minLat = null, float? minLng = null, DateTime? moveInDate = null, bool? noBoyl = null, float? numStory = null, int? offset = null, float? originLat = null, float? originLng = null, int? page = null, int? pageSize = null, int? planId = null, string planIds = null, string planName = null, string postalCode = null, string postalCodes = null, float? prHi = null, float? prLo = null, string projectTypeCode = null, bool? promo = null, bool? qmi = null, float? radius = null, string radiusUnit = null, int? sfhigh = null, int? sflow = null, string soldDataToken = null, DateTime? soldFromDate = null, GraphQLSortValueInputType[] sort = null, string sortFirstBy = null, int? specId = null, string specIds = null, GraphQLSpecTypeInputType[] specTypes = null, string state = null, float? story = null, bool? adult = null, bool? ageRestricted = null, bool? gated = null, bool? golf = null, bool? green = null, bool? nature = null, bool? parks = null, bool? pool = null, bool? sports = null, bool? views = null, bool? waterfront = null, bool? hasCommCenter = null, bool? hasEvent = null, bool? hasConPromo = null, bool? hasAgePromo = null, bool? hasHotDeals = null, bool? hasLuxuryHomes = null, bool? hasViewOnly = null, bool? hasBuilderReview = null, bool? isHotHome = null, bool? isMaster = null, bool? isUrban = null, bool? isCondoType = null, bool? isMultiFamily = null, bool? isSingleFamily = null, bool? isTownHome = null, Func<GraphQLSearchCommunities, GraphQLSearchCommunities> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return new GraphQLQuery<GraphQLSearchCommunities>(this._graphQlHttpClient, true, currentContext, fields, sessionToken, partnerId, alphaResults, aPromo, basicListingToTheEnd, bath, bcType, bed, brandId, brandIds, builderId, builderIds, cities, city, commId, parentCommId, commIds, commStatus, communityName, counties, county, cPromo, custom, customBuilderLocations, customResults, dist, districtIds, excludeBasiCommunities, excludeRegularComms, excludeCommunitiesByBuilderId, first, garage, geoPolygon, halfBath, homeStatus, includeMpcs, includeBasicMpc, listingIds, imageIds, livAreas, marketId, marketIds, masterBrLoc, maxLat, maxLng, mfr, minLat, minLng, moveInDate, noBoyl, numStory, offset, originLat, originLng, page, pageSize, planId, planIds, planName, postalCode, postalCodes, prHi, prLo, projectTypeCode, promo, qmi, radius, radiusUnit, sfhigh, sflow, soldDataToken, soldFromDate, sort, sortFirstBy, specId, specIds, specTypes, state, story, adult, ageRestricted, gated, golf, green, nature, parks, pool, sports, views, waterfront, hasCommCenter, hasEvent, hasConPromo, hasAgePromo, hasHotDeals, hasLuxuryHomes, hasViewOnly, hasBuilderReview, isHotHome, isMaster, isUrban, isCondoType, isMultiFamily, isSingleFamily, isTownHome);
	}

	/// <param name="partnerId">Partner Id</param>
	/// <param name="alphaResults">Alpha Results</param>
	/// <param name="aPromo">Has Agent Promo</param>
	/// <param name="basicListingToTheEnd">Basic Listing to End</param>
	/// <param name="bath">Bath</param>
	/// <param name="bcType">Community Type (C: Coming soon; G: Gated; N: Normal; X: Closed)</param>
	/// <param name="bed">Bed</param>
	/// <param name="brandId">Brand Id</param>
	/// <param name="brandIds">Brands</param>
	/// <param name="builderId">Builder Id</param>
	/// <param name="builderIds">Builder Ids</param>
	/// <param name="cities">Cities</param>
	/// <param name="city">City</param>
	/// <param name="commId">Community Id</param>
	/// <param name="parentCommId">Community Id</param>
	/// <param name="commIds">Community Ids</param>
	/// <param name="commStatus">Community Status (N: Active; C: Coming Soon; G: Grand Opening; X: Close Out; CG: ComingSoonGrandOpening)</param>
	/// <param name="communityName">Community Name</param>
	/// <param name="counties">Counties</param>
	/// <param name="county">County</param>
	/// <param name="cPromo">Has Consumer Promo</param>
	/// <param name="custom">Custom</param>
	/// <param name="customBuilderLocations">Custom Builder Locations</param>
	/// <param name="customResults">Custom Results</param>
	/// <param name="dist">Distance</param>
	/// <param name="districtIds">District Ids</param>
	/// <param name="excludeBasiCommunities">Exclude Basic Communities</param>
	/// <param name="excludeRegularComms">Exclude Basic Communities</param>
	/// <param name="excludeCommunitiesByBuilderId">Exclude Communities by Builder Id</param>
	/// <param name="first">First</param>
	/// <param name="garage">Garage</param>
	/// <param name="geoPolygon">Array or coordinates to apply polygon/draw on map search</param>
	/// <param name="halfBath">Half Bath</param>
	/// <param name="homeStatus">Home Status (R: Ready to Build; UC: Under Construction; A: Available Now; M: Model; S: Sold)</param>
	/// <param name="includeMpcs">Include Master Plan Communities</param>
	/// <param name="includeBasicMpc">Include Basic Master Plan Community</param>
	/// <param name="listingIds">LivingIds</param>
	/// <param name="imageIds">LivingIds</param>
	/// <param name="livAreas">Living Areas</param>
	/// <param name="marketId">Market Id</param>
	/// <param name="marketIds">Market Ids</param>
	/// <param name="masterBrLoc">Master Bed Location</param>
	/// <param name="maxLat">Max Latitude</param>
	/// <param name="maxLng">Max Longitude</param>
	/// <param name="mfr">Manufactured</param>
	/// <param name="minLat">Min Latitude</param>
	/// <param name="minLng">Min Longitude</param>
	/// <param name="moveInDate">Move in date</param>
	/// <param name="noBoyl">No Boyl</param>
	/// <param name="numStory">Num Story</param>
	/// <param name="offset">Skip first N records</param>
	/// <param name="originLat">Origin Latitude</param>
	/// <param name="originLng">Origin Longitude</param>
	/// <param name="page">Page</param>
	/// <param name="pageSize">Page Size</param>
	/// <param name="planId">Plan Id</param>
	/// <param name="planIds">Plan Ids</param>
	/// <param name="planName">Plan name</param>
	/// <param name="postalCode">Postal Code</param>
	/// <param name="postalCodes">Postal Codes</param>
	/// <param name="prHi">Higher Price</param>
	/// <param name="prLo">Minimum Price</param>
	/// <param name="projectTypeCode">Project Type: (BYL: Boyl; CST: Custom; MFR: Manufactured)</param>
	/// <param name="promo">Has Promo</param>
	/// <param name="qmi">Quick Move In</param>
	/// <param name="radius">Radius</param>
	/// <param name="radiusUnit">Raius Unit</param>
	/// <param name="sfhigh">Maximum Square Feet</param>
	/// <param name="sflow">Minimum Square Feet</param>
	/// <param name="soldDataToken">If provided and it's valid, it will return Sold Data info, Otherwise will filter out sold data records even if HomeStatus: S is specified</param>
	/// <param name="soldFromDate">Sold From Date</param>
	/// <param name="sort">List of fields to sort</param>
	/// <param name="sortFirstBy">Sort First By</param>
	/// <param name="specId">Spec Id</param>
	/// <param name="specIds">Spec Ids</param>
	/// <param name="specTypes">Spec Type</param>
	/// <param name="state">State</param>
	/// <param name="story">Story</param>
	/// <param name="adult">Is for Adult</param>
	/// <param name="ageRestricted">Age Restricted</param>
	/// <param name="gated">Gated</param>
	/// <param name="golf">Golf</param>
	/// <param name="green">Green</param>
	/// <param name="nature">Has Nature areas</param>
	/// <param name="parks">Parks</param>
	/// <param name="pool">Pool</param>
	/// <param name="sports">Sports</param>
	/// <param name="views">Views</param>
	/// <param name="waterfront">Water front</param>
	/// <param name="hasCommCenter">Has Community Center</param>
	/// <param name="hasEvent">Has Event </param>
	/// <param name="hasConPromo">Has Consumer Promo</param>
	/// <param name="hasAgePromo">Has Agent Promotion</param>
	/// <param name="hasHotDeals">Has Hot Home</param>
	/// <param name="hasLuxuryHomes">Has Hot Home</param>
	/// <param name="hasViewOnly">Has View Only</param>
	/// <param name="hasBuilderReview">Has Builder Review</param>
	/// <param name="isHotHome">Is Hot Home</param>
	/// <param name="isMaster">Is Master </param>
	/// <param name="isUrban">Is Urban</param>
	/// <param name="isCondoType">Is Condo Type</param>
	/// <param name="isMultiFamily">Whether the community has at least one multi family home, or when the home type is MF at home level</param>
	/// <param name="isSingleFamily">Whether the community has at least one single family home, or the home is SF at home level</param>
	/// <param name="isTownHome">Is Town Home</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("searchAllComms")]
	public Task<List<GraphQLSearchCommunities>>SearchAllCommsAsync (string sessionToken, int? partnerId, bool? alphaResults = null, bool? aPromo = null, bool? basicListingToTheEnd = null, int? bath = null, string bcType = null, int? bed = null, int? brandId = null, int?[] brandIds = null, int? builderId = null, string builderIds = null, string cities = null, string city = null, int? commId = null, int? parentCommId = null, string commIds = null, string commStatus = null, string communityName = null, string counties = null, string county = null, bool? cPromo = null, bool? custom = null, bool? customBuilderLocations = null, bool? customResults = null, float? dist = null, string districtIds = null, bool? excludeBasiCommunities = null, bool? excludeRegularComms = null, bool? excludeCommunitiesByBuilderId = null, int? first = null, int? garage = null, GraphQLGeoPolygonInputType[] geoPolygon = null, int? halfBath = null, string homeStatus = null, bool? includeMpcs = null, bool? includeBasicMpc = null, int?[] listingIds = null, int?[] imageIds = null, int? livAreas = null, int? marketId = null, string marketIds = null, int? masterBrLoc = null, float? maxLat = null, float? maxLng = null, bool? mfr = null, float? minLat = null, float? minLng = null, DateTime? moveInDate = null, bool? noBoyl = null, float? numStory = null, int? offset = null, float? originLat = null, float? originLng = null, int? page = null, int? pageSize = null, int? planId = null, string planIds = null, string planName = null, string postalCode = null, string postalCodes = null, float? prHi = null, float? prLo = null, string projectTypeCode = null, bool? promo = null, bool? qmi = null, float? radius = null, string radiusUnit = null, int? sfhigh = null, int? sflow = null, string soldDataToken = null, DateTime? soldFromDate = null, GraphQLSortValueInputType[] sort = null, string sortFirstBy = null, int? specId = null, string specIds = null, GraphQLSpecTypeInputType[] specTypes = null, string state = null, float? story = null, bool? adult = null, bool? ageRestricted = null, bool? gated = null, bool? golf = null, bool? green = null, bool? nature = null, bool? parks = null, bool? pool = null, bool? sports = null, bool? views = null, bool? waterfront = null, bool? hasCommCenter = null, bool? hasEvent = null, bool? hasConPromo = null, bool? hasAgePromo = null, bool? hasHotDeals = null, bool? hasLuxuryHomes = null, bool? hasViewOnly = null, bool? hasBuilderReview = null, bool? isHotHome = null, bool? isMaster = null, bool? isUrban = null, bool? isCondoType = null, bool? isMultiFamily = null, bool? isSingleFamily = null, bool? isTownHome = null, Func<GraphQLSearchCommunities, GraphQLSearchCommunities> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return Task.Run(() => new GraphQLQuery<GraphQLSearchCommunities>(this._graphQlHttpClient, true, currentContext, fields, sessionToken, partnerId, alphaResults, aPromo, basicListingToTheEnd, bath, bcType, bed, brandId, brandIds, builderId, builderIds, cities, city, commId, parentCommId, commIds, commStatus, communityName, counties, county, cPromo, custom, customBuilderLocations, customResults, dist, districtIds, excludeBasiCommunities, excludeRegularComms, excludeCommunitiesByBuilderId, first, garage, geoPolygon, halfBath, homeStatus, includeMpcs, includeBasicMpc, listingIds, imageIds, livAreas, marketId, marketIds, masterBrLoc, maxLat, maxLng, mfr, minLat, minLng, moveInDate, noBoyl, numStory, offset, originLat, originLng, page, pageSize, planId, planIds, planName, postalCode, postalCodes, prHi, prLo, projectTypeCode, promo, qmi, radius, radiusUnit, sfhigh, sflow, soldDataToken, soldFromDate, sort, sortFirstBy, specId, specIds, specTypes, state, story, adult, ageRestricted, gated, golf, green, nature, parks, pool, sports, views, waterfront, hasCommCenter, hasEvent, hasConPromo, hasAgePromo, hasHotDeals, hasLuxuryHomes, hasViewOnly, hasBuilderReview, isHotHome, isMaster, isUrban, isCondoType, isMultiFamily, isSingleFamily, isTownHome).ToList());
	}

	/// <param name="alphaResults">Alpha Results</param>
	/// <param name="aPromo">Has Agent Promo</param>
	/// <param name="basicListingToTheEnd">Basic Listing to End</param>
	/// <param name="bath">Bath</param>
	/// <param name="bcType">Community Type (C: Coming soon; G: Gated; N: Normal; X: Closed)</param>
	/// <param name="bed">Bed</param>
	/// <param name="brandId">Brand Id</param>
	/// <param name="brandIds">Brands</param>
	/// <param name="builderId">Builder Id</param>
	/// <param name="builderIds">Builder Ids</param>
	/// <param name="cities">Cities</param>
	/// <param name="city">City</param>
	/// <param name="commId">Community Id</param>
	/// <param name="parentCommId">Community Id</param>
	/// <param name="commIds">Community Ids</param>
	/// <param name="commStatus">Community Status (N: Active; C: Coming Soon; G: Grand Opening; X: Close Out; CG: ComingSoonGrandOpening)</param>
	/// <param name="communityName">Community Name</param>
	/// <param name="counties">Counties</param>
	/// <param name="county">County</param>
	/// <param name="cPromo">Has Consumer Promo</param>
	/// <param name="custom">Custom</param>
	/// <param name="customBuilderLocations">Custom Builder Locations</param>
	/// <param name="customResults">Custom Results</param>
	/// <param name="dist">Distance</param>
	/// <param name="districtIds">District Ids</param>
	/// <param name="excludeBasiCommunities">Exclude Basic Communities</param>
	/// <param name="excludeRegularComms">Exclude Basic Communities</param>
	/// <param name="excludeCommunitiesByBuilderId">Exclude Communities by Builder Id</param>
	/// <param name="first">First</param>
	/// <param name="garage">Garage</param>
	/// <param name="geoPolygon">Array or coordinates to apply polygon/draw on map search</param>
	/// <param name="halfBath">Half Bath</param>
	/// <param name="homeStatus">Home Status (R: Ready to Build; UC: Under Construction; A: Available Now; M: Model; S: Sold)</param>
	/// <param name="includeMpcs">Include Master Plan Communities</param>
	/// <param name="includeBasicMpc">Include Basic Master Plan Community</param>
	/// <param name="listingIds">LivingIds</param>
	/// <param name="imageIds">LivingIds</param>
	/// <param name="livAreas">Living Areas</param>
	/// <param name="marketId">Market Id</param>
	/// <param name="marketIds">Market Ids</param>
	/// <param name="masterBrLoc">Master Bed Location</param>
	/// <param name="maxLat">Max Latitude</param>
	/// <param name="maxLng">Max Longitude</param>
	/// <param name="mfr">Manufactured</param>
	/// <param name="minLat">Min Latitude</param>
	/// <param name="minLng">Min Longitude</param>
	/// <param name="moveInDate">Move in date</param>
	/// <param name="noBoyl">No Boyl</param>
	/// <param name="numStory">Num Story</param>
	/// <param name="offset">Skip first N records</param>
	/// <param name="originLat">Origin Latitude</param>
	/// <param name="originLng">Origin Longitude</param>
	/// <param name="page">Page</param>
	/// <param name="pageSize">Page Size</param>
	/// <param name="partnerId">Partner Id</param>
	/// <param name="planId">Plan Id</param>
	/// <param name="planIds">Plan Ids</param>
	/// <param name="planName">Plan name</param>
	/// <param name="postalCode">Postal Code</param>
	/// <param name="postalCodes">Postal Codes</param>
	/// <param name="prHi">Higher Price</param>
	/// <param name="prLo">Minimum Price</param>
	/// <param name="projectTypeCode">Project Type: (BYL: Boyl; CST: Custom; MFR: Manufactured)</param>
	/// <param name="promo">Has Promo</param>
	/// <param name="qmi">Quick Move In</param>
	/// <param name="radius">Radius</param>
	/// <param name="radiusUnit">Raius Unit</param>
	/// <param name="sfhigh">Maximum Square Feet</param>
	/// <param name="sflow">Minimum Square Feet</param>
	/// <param name="soldDataToken">If provided and it's valid, it will return Sold Data info, Otherwise will filter out sold data records even if HomeStatus: S is specified</param>
	/// <param name="soldFromDate">Sold From Date</param>
	/// <param name="sort">List of fields to sort</param>
	/// <param name="sortFirstBy">Sort First By</param>
	/// <param name="specId">Spec Id</param>
	/// <param name="specIds">Spec Ids</param>
	/// <param name="specTypes">Spec Type</param>
	/// <param name="state">State</param>
	/// <param name="story">Story</param>
	/// <param name="adult">Is for Adult</param>
	/// <param name="ageRestricted">Age Restricted</param>
	/// <param name="gated">Gated</param>
	/// <param name="golf">Golf</param>
	/// <param name="green">Green</param>
	/// <param name="nature">Has Nature areas</param>
	/// <param name="parks">Parks</param>
	/// <param name="pool">Pool</param>
	/// <param name="sports">Sports</param>
	/// <param name="views">Views</param>
	/// <param name="waterfront">Water front</param>
	/// <param name="hasCommCenter">Has Community Center</param>
	/// <param name="hasEvent">Has Event </param>
	/// <param name="hasConPromo">Has Consumer Promo</param>
	/// <param name="hasAgePromo">Has Agent Promotion</param>
	/// <param name="hasHotDeals">Has Hot Home</param>
	/// <param name="hasLuxuryHomes">Has Hot Home</param>
	/// <param name="hasViewOnly">Has View Only</param>
	/// <param name="hasBuilderReview">Has Builder Review</param>
	/// <param name="isHotHome">Is Hot Home</param>
	/// <param name="isMaster">Is Master </param>
	/// <param name="isUrban">Is Urban</param>
	/// <param name="isCondoType">Is Condo Type</param>
	/// <param name="isMultiFamily">Whether the community has at least one multi family home, or when the home type is MF at home level</param>
	/// <param name="isSingleFamily">Whether the community has at least one single family home, or the home is SF at home level</param>
	/// <param name="isTownHome">Is Town Home</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("searchAllCommsCounts")]
	public GraphQLQuery<GraphQLResultCommCounts>SearchAllCommsCounts (string sessionToken, int? partnerId, bool? alphaResults = null, bool? aPromo = null, bool? basicListingToTheEnd = null, int? bath = null, string bcType = null, int? bed = null, int? brandId = null, int?[] brandIds = null, int? builderId = null, string builderIds = null, string cities = null, string city = null, int? commId = null, int? parentCommId = null, string commIds = null, string commStatus = null, string communityName = null, string counties = null, string county = null, bool? cPromo = null, bool? custom = null, bool? customBuilderLocations = null, bool? customResults = null, float? dist = null, string districtIds = null, bool? excludeBasiCommunities = null, bool? excludeRegularComms = null, bool? excludeCommunitiesByBuilderId = null, int? first = null, int? garage = null, GraphQLGeoPolygonInputType[] geoPolygon = null, int? halfBath = null, string homeStatus = null, bool? includeMpcs = null, bool? includeBasicMpc = null, int?[] listingIds = null, int?[] imageIds = null, int? livAreas = null, int? marketId = null, string marketIds = null, int? masterBrLoc = null, float? maxLat = null, float? maxLng = null, bool? mfr = null, float? minLat = null, float? minLng = null, DateTime? moveInDate = null, bool? noBoyl = null, float? numStory = null, int? offset = null, float? originLat = null, float? originLng = null, int? page = null, int? pageSize = null, int? planId = null, string planIds = null, string planName = null, string postalCode = null, string postalCodes = null, float? prHi = null, float? prLo = null, string projectTypeCode = null, bool? promo = null, bool? qmi = null, float? radius = null, string radiusUnit = null, int? sfhigh = null, int? sflow = null, string soldDataToken = null, DateTime? soldFromDate = null, GraphQLSortValueInputType[] sort = null, string sortFirstBy = null, int? specId = null, string specIds = null, GraphQLSpecTypeInputType[] specTypes = null, string state = null, float? story = null, bool? adult = null, bool? ageRestricted = null, bool? gated = null, bool? golf = null, bool? green = null, bool? nature = null, bool? parks = null, bool? pool = null, bool? sports = null, bool? views = null, bool? waterfront = null, bool? hasCommCenter = null, bool? hasEvent = null, bool? hasConPromo = null, bool? hasAgePromo = null, bool? hasHotDeals = null, bool? hasLuxuryHomes = null, bool? hasViewOnly = null, bool? hasBuilderReview = null, bool? isHotHome = null, bool? isMaster = null, bool? isUrban = null, bool? isCondoType = null, bool? isMultiFamily = null, bool? isSingleFamily = null, bool? isTownHome = null, Func<GraphQLResultCommCounts, GraphQLResultCommCounts> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return new GraphQLQuery<GraphQLResultCommCounts>(this._graphQlHttpClient, false, currentContext, fields, sessionToken, partnerId, alphaResults, aPromo, basicListingToTheEnd, bath, bcType, bed, brandId, brandIds, builderId, builderIds, cities, city, commId, parentCommId, commIds, commStatus, communityName, counties, county, cPromo, custom, customBuilderLocations, customResults, dist, districtIds, excludeBasiCommunities, excludeRegularComms, excludeCommunitiesByBuilderId, first, garage, geoPolygon, halfBath, homeStatus, includeMpcs, includeBasicMpc, listingIds, imageIds, livAreas, marketId, marketIds, masterBrLoc, maxLat, maxLng, mfr, minLat, minLng, moveInDate, noBoyl, numStory, offset, originLat, originLng, page, pageSize, planId, planIds, planName, postalCode, postalCodes, prHi, prLo, projectTypeCode, promo, qmi, radius, radiusUnit, sfhigh, sflow, soldDataToken, soldFromDate, sort, sortFirstBy, specId, specIds, specTypes, state, story, adult, ageRestricted, gated, golf, green, nature, parks, pool, sports, views, waterfront, hasCommCenter, hasEvent, hasConPromo, hasAgePromo, hasHotDeals, hasLuxuryHomes, hasViewOnly, hasBuilderReview, isHotHome, isMaster, isUrban, isCondoType, isMultiFamily, isSingleFamily, isTownHome);
	}

	/// <param name="partnerId">Partner Id</param>
	/// <param name="alphaResults">Alpha Results</param>
	/// <param name="aPromo">Has Agent Promo</param>
	/// <param name="basicListingToTheEnd">Basic Listing to End</param>
	/// <param name="bath">Bath</param>
	/// <param name="bcType">Community Type (C: Coming soon; G: Gated; N: Normal; X: Closed)</param>
	/// <param name="bed">Bed</param>
	/// <param name="brandId">Brand Id</param>
	/// <param name="brandIds">Brands</param>
	/// <param name="builderId">Builder Id</param>
	/// <param name="builderIds">Builder Ids</param>
	/// <param name="cities">Cities</param>
	/// <param name="city">City</param>
	/// <param name="commId">Community Id</param>
	/// <param name="parentCommId">Community Id</param>
	/// <param name="commIds">Community Ids</param>
	/// <param name="commStatus">Community Status (N: Active; C: Coming Soon; G: Grand Opening; X: Close Out; CG: ComingSoonGrandOpening)</param>
	/// <param name="communityName">Community Name</param>
	/// <param name="counties">Counties</param>
	/// <param name="county">County</param>
	/// <param name="cPromo">Has Consumer Promo</param>
	/// <param name="custom">Custom</param>
	/// <param name="customBuilderLocations">Custom Builder Locations</param>
	/// <param name="customResults">Custom Results</param>
	/// <param name="dist">Distance</param>
	/// <param name="districtIds">District Ids</param>
	/// <param name="excludeBasiCommunities">Exclude Basic Communities</param>
	/// <param name="excludeRegularComms">Exclude Basic Communities</param>
	/// <param name="excludeCommunitiesByBuilderId">Exclude Communities by Builder Id</param>
	/// <param name="first">First</param>
	/// <param name="garage">Garage</param>
	/// <param name="geoPolygon">Array or coordinates to apply polygon/draw on map search</param>
	/// <param name="halfBath">Half Bath</param>
	/// <param name="homeStatus">Home Status (R: Ready to Build; UC: Under Construction; A: Available Now; M: Model; S: Sold)</param>
	/// <param name="includeMpcs">Include Master Plan Communities</param>
	/// <param name="includeBasicMpc">Include Basic Master Plan Community</param>
	/// <param name="listingIds">LivingIds</param>
	/// <param name="imageIds">LivingIds</param>
	/// <param name="livAreas">Living Areas</param>
	/// <param name="marketId">Market Id</param>
	/// <param name="marketIds">Market Ids</param>
	/// <param name="masterBrLoc">Master Bed Location</param>
	/// <param name="maxLat">Max Latitude</param>
	/// <param name="maxLng">Max Longitude</param>
	/// <param name="mfr">Manufactured</param>
	/// <param name="minLat">Min Latitude</param>
	/// <param name="minLng">Min Longitude</param>
	/// <param name="moveInDate">Move in date</param>
	/// <param name="noBoyl">No Boyl</param>
	/// <param name="numStory">Num Story</param>
	/// <param name="offset">Skip first N records</param>
	/// <param name="originLat">Origin Latitude</param>
	/// <param name="originLng">Origin Longitude</param>
	/// <param name="page">Page</param>
	/// <param name="pageSize">Page Size</param>
	/// <param name="planId">Plan Id</param>
	/// <param name="planIds">Plan Ids</param>
	/// <param name="planName">Plan name</param>
	/// <param name="postalCode">Postal Code</param>
	/// <param name="postalCodes">Postal Codes</param>
	/// <param name="prHi">Higher Price</param>
	/// <param name="prLo">Minimum Price</param>
	/// <param name="projectTypeCode">Project Type: (BYL: Boyl; CST: Custom; MFR: Manufactured)</param>
	/// <param name="promo">Has Promo</param>
	/// <param name="qmi">Quick Move In</param>
	/// <param name="radius">Radius</param>
	/// <param name="radiusUnit">Raius Unit</param>
	/// <param name="sfhigh">Maximum Square Feet</param>
	/// <param name="sflow">Minimum Square Feet</param>
	/// <param name="soldDataToken">If provided and it's valid, it will return Sold Data info, Otherwise will filter out sold data records even if HomeStatus: S is specified</param>
	/// <param name="soldFromDate">Sold From Date</param>
	/// <param name="sort">List of fields to sort</param>
	/// <param name="sortFirstBy">Sort First By</param>
	/// <param name="specId">Spec Id</param>
	/// <param name="specIds">Spec Ids</param>
	/// <param name="specTypes">Spec Type</param>
	/// <param name="state">State</param>
	/// <param name="story">Story</param>
	/// <param name="adult">Is for Adult</param>
	/// <param name="ageRestricted">Age Restricted</param>
	/// <param name="gated">Gated</param>
	/// <param name="golf">Golf</param>
	/// <param name="green">Green</param>
	/// <param name="nature">Has Nature areas</param>
	/// <param name="parks">Parks</param>
	/// <param name="pool">Pool</param>
	/// <param name="sports">Sports</param>
	/// <param name="views">Views</param>
	/// <param name="waterfront">Water front</param>
	/// <param name="hasCommCenter">Has Community Center</param>
	/// <param name="hasEvent">Has Event </param>
	/// <param name="hasConPromo">Has Consumer Promo</param>
	/// <param name="hasAgePromo">Has Agent Promotion</param>
	/// <param name="hasHotDeals">Has Hot Home</param>
	/// <param name="hasLuxuryHomes">Has Hot Home</param>
	/// <param name="hasViewOnly">Has View Only</param>
	/// <param name="hasBuilderReview">Has Builder Review</param>
	/// <param name="isHotHome">Is Hot Home</param>
	/// <param name="isMaster">Is Master </param>
	/// <param name="isUrban">Is Urban</param>
	/// <param name="isCondoType">Is Condo Type</param>
	/// <param name="isMultiFamily">Whether the community has at least one multi family home, or when the home type is MF at home level</param>
	/// <param name="isSingleFamily">Whether the community has at least one single family home, or the home is SF at home level</param>
	/// <param name="isTownHome">Is Town Home</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("searchAllCommsCounts")]
	public Task<List<GraphQLResultCommCounts>>SearchAllCommsCountsAsync (string sessionToken, int? partnerId, bool? alphaResults = null, bool? aPromo = null, bool? basicListingToTheEnd = null, int? bath = null, string bcType = null, int? bed = null, int? brandId = null, int?[] brandIds = null, int? builderId = null, string builderIds = null, string cities = null, string city = null, int? commId = null, int? parentCommId = null, string commIds = null, string commStatus = null, string communityName = null, string counties = null, string county = null, bool? cPromo = null, bool? custom = null, bool? customBuilderLocations = null, bool? customResults = null, float? dist = null, string districtIds = null, bool? excludeBasiCommunities = null, bool? excludeRegularComms = null, bool? excludeCommunitiesByBuilderId = null, int? first = null, int? garage = null, GraphQLGeoPolygonInputType[] geoPolygon = null, int? halfBath = null, string homeStatus = null, bool? includeMpcs = null, bool? includeBasicMpc = null, int?[] listingIds = null, int?[] imageIds = null, int? livAreas = null, int? marketId = null, string marketIds = null, int? masterBrLoc = null, float? maxLat = null, float? maxLng = null, bool? mfr = null, float? minLat = null, float? minLng = null, DateTime? moveInDate = null, bool? noBoyl = null, float? numStory = null, int? offset = null, float? originLat = null, float? originLng = null, int? page = null, int? pageSize = null, int? planId = null, string planIds = null, string planName = null, string postalCode = null, string postalCodes = null, float? prHi = null, float? prLo = null, string projectTypeCode = null, bool? promo = null, bool? qmi = null, float? radius = null, string radiusUnit = null, int? sfhigh = null, int? sflow = null, string soldDataToken = null, DateTime? soldFromDate = null, GraphQLSortValueInputType[] sort = null, string sortFirstBy = null, int? specId = null, string specIds = null, GraphQLSpecTypeInputType[] specTypes = null, string state = null, float? story = null, bool? adult = null, bool? ageRestricted = null, bool? gated = null, bool? golf = null, bool? green = null, bool? nature = null, bool? parks = null, bool? pool = null, bool? sports = null, bool? views = null, bool? waterfront = null, bool? hasCommCenter = null, bool? hasEvent = null, bool? hasConPromo = null, bool? hasAgePromo = null, bool? hasHotDeals = null, bool? hasLuxuryHomes = null, bool? hasViewOnly = null, bool? hasBuilderReview = null, bool? isHotHome = null, bool? isMaster = null, bool? isUrban = null, bool? isCondoType = null, bool? isMultiFamily = null, bool? isSingleFamily = null, bool? isTownHome = null, Func<GraphQLResultCommCounts, GraphQLResultCommCounts> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return Task.Run(() => new GraphQLQuery<GraphQLResultCommCounts>(this._graphQlHttpClient, false, currentContext, fields, sessionToken, partnerId, alphaResults, aPromo, basicListingToTheEnd, bath, bcType, bed, brandId, brandIds, builderId, builderIds, cities, city, commId, parentCommId, commIds, commStatus, communityName, counties, county, cPromo, custom, customBuilderLocations, customResults, dist, districtIds, excludeBasiCommunities, excludeRegularComms, excludeCommunitiesByBuilderId, first, garage, geoPolygon, halfBath, homeStatus, includeMpcs, includeBasicMpc, listingIds, imageIds, livAreas, marketId, marketIds, masterBrLoc, maxLat, maxLng, mfr, minLat, minLng, moveInDate, noBoyl, numStory, offset, originLat, originLng, page, pageSize, planId, planIds, planName, postalCode, postalCodes, prHi, prLo, projectTypeCode, promo, qmi, radius, radiusUnit, sfhigh, sflow, soldDataToken, soldFromDate, sort, sortFirstBy, specId, specIds, specTypes, state, story, adult, ageRestricted, gated, golf, green, nature, parks, pool, sports, views, waterfront, hasCommCenter, hasEvent, hasConPromo, hasAgePromo, hasHotDeals, hasLuxuryHomes, hasViewOnly, hasBuilderReview, isHotHome, isMaster, isUrban, isCondoType, isMultiFamily, isSingleFamily, isTownHome).ToList());
	}

	/// <param name="alphaResults">Alpha Results</param>
	/// <param name="aPromo">Has Agent Promo</param>
	/// <param name="basicListingToTheEnd">Basic Listing to End</param>
	/// <param name="bath">Bath</param>
	/// <param name="bcType">Community Type (C: Coming soon; G: Gated; N: Normal; X: Closed)</param>
	/// <param name="bed">Bed</param>
	/// <param name="brandId">Brand Id</param>
	/// <param name="brandIds">Brands</param>
	/// <param name="builderId">Builder Id</param>
	/// <param name="builderIds">Builder Ids</param>
	/// <param name="cities">Cities</param>
	/// <param name="city">City</param>
	/// <param name="commId">Community Id</param>
	/// <param name="parentCommId">Community Id</param>
	/// <param name="commIds">Community Ids</param>
	/// <param name="commStatus">Community Status (N: Active; C: Coming Soon; G: Grand Opening; X: Close Out; CG: ComingSoonGrandOpening)</param>
	/// <param name="communityName">Community Name</param>
	/// <param name="counties">Counties</param>
	/// <param name="county">County</param>
	/// <param name="cPromo">Has Consumer Promo</param>
	/// <param name="custom">Custom</param>
	/// <param name="customBuilderLocations">Custom Builder Locations</param>
	/// <param name="customResults">Custom Results</param>
	/// <param name="dist">Distance</param>
	/// <param name="districtIds">District Ids</param>
	/// <param name="excludeBasiCommunities">Exclude Basic Communities</param>
	/// <param name="excludeRegularComms">Exclude Basic Communities</param>
	/// <param name="excludeCommunitiesByBuilderId">Exclude Communities by Builder Id</param>
	/// <param name="first">First</param>
	/// <param name="garage">Garage</param>
	/// <param name="geoPolygon">Array or coordinates to apply polygon/draw on map search</param>
	/// <param name="halfBath">Half Bath</param>
	/// <param name="homeStatus">Home Status (R: Ready to Build; UC: Under Construction; A: Available Now; M: Model; S: Sold)</param>
	/// <param name="includeMpcs">Include Master Plan Communities</param>
	/// <param name="includeBasicMpc">Include Basic Master Plan Community</param>
	/// <param name="listingIds">LivingIds</param>
	/// <param name="imageIds">LivingIds</param>
	/// <param name="livAreas">Living Areas</param>
	/// <param name="marketId">Market Id</param>
	/// <param name="marketIds">Market Ids</param>
	/// <param name="masterBrLoc">Master Bed Location</param>
	/// <param name="maxLat">Max Latitude</param>
	/// <param name="maxLng">Max Longitude</param>
	/// <param name="mfr">Manufactured</param>
	/// <param name="minLat">Min Latitude</param>
	/// <param name="minLng">Min Longitude</param>
	/// <param name="moveInDate">Move in date</param>
	/// <param name="noBoyl">No Boyl</param>
	/// <param name="numStory">Num Story</param>
	/// <param name="offset">Skip first N records</param>
	/// <param name="originLat">Origin Latitude</param>
	/// <param name="originLng">Origin Longitude</param>
	/// <param name="page">Page</param>
	/// <param name="pageSize">Page Size</param>
	/// <param name="partnerId">Partner Id</param>
	/// <param name="planId">Plan Id</param>
	/// <param name="planIds">Plan Ids</param>
	/// <param name="planName">Plan name</param>
	/// <param name="postalCode">Postal Code</param>
	/// <param name="postalCodes">Postal Codes</param>
	/// <param name="prHi">Higher Price</param>
	/// <param name="prLo">Minimum Price</param>
	/// <param name="projectTypeCode">Project Type: (BYL: Boyl; CST: Custom; MFR: Manufactured)</param>
	/// <param name="promo">Has Promo</param>
	/// <param name="qmi">Quick Move In</param>
	/// <param name="radius">Radius</param>
	/// <param name="radiusUnit">Raius Unit</param>
	/// <param name="sfhigh">Maximum Square Feet</param>
	/// <param name="sflow">Minimum Square Feet</param>
	/// <param name="soldDataToken">If provided and it's valid, it will return Sold Data info, Otherwise will filter out sold data records even if HomeStatus: S is specified</param>
	/// <param name="soldFromDate">Sold From Date</param>
	/// <param name="sort">List of fields to sort</param>
	/// <param name="sortFirstBy">Sort First By</param>
	/// <param name="specId">Spec Id</param>
	/// <param name="specIds">Spec Ids</param>
	/// <param name="specTypes">Spec Type</param>
	/// <param name="state">State</param>
	/// <param name="story">Story</param>
	/// <param name="adult">Is for Adult</param>
	/// <param name="ageRestricted">Age Restricted</param>
	/// <param name="gated">Gated</param>
	/// <param name="golf">Golf</param>
	/// <param name="green">Green</param>
	/// <param name="nature">Has Nature areas</param>
	/// <param name="parks">Parks</param>
	/// <param name="pool">Pool</param>
	/// <param name="sports">Sports</param>
	/// <param name="views">Views</param>
	/// <param name="waterfront">Water front</param>
	/// <param name="hasCommCenter">Has Community Center</param>
	/// <param name="hasEvent">Has Event </param>
	/// <param name="hasConPromo">Has Consumer Promo</param>
	/// <param name="hasAgePromo">Has Agent Promotion</param>
	/// <param name="hasHotDeals">Has Hot Home</param>
	/// <param name="hasLuxuryHomes">Has Hot Home</param>
	/// <param name="hasViewOnly">Has View Only</param>
	/// <param name="hasBuilderReview">Has Builder Review</param>
	/// <param name="isHotHome">Is Hot Home</param>
	/// <param name="isMaster">Is Master </param>
	/// <param name="isUrban">Is Urban</param>
	/// <param name="isCondoType">Is Condo Type</param>
	/// <param name="isMultiFamily">Whether the community has at least one multi family home, or when the home type is MF at home level</param>
	/// <param name="isSingleFamily">Whether the community has at least one single family home, or the home is SF at home level</param>
	/// <param name="isTownHome">Is Town Home</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("searchAllHomes")]
	public GraphQLQuery<GraphQLSearchAllHomes>SearchAllHomes (string sessionToken, int? partnerId, bool? alphaResults = null, bool? aPromo = null, bool? basicListingToTheEnd = null, int? bath = null, string bcType = null, int? bed = null, int? brandId = null, int?[] brandIds = null, int? builderId = null, string builderIds = null, string cities = null, string city = null, int? commId = null, int? parentCommId = null, string commIds = null, string commStatus = null, string communityName = null, string counties = null, string county = null, bool? cPromo = null, bool? custom = null, bool? customBuilderLocations = null, bool? customResults = null, float? dist = null, string districtIds = null, bool? excludeBasiCommunities = null, bool? excludeRegularComms = null, bool? excludeCommunitiesByBuilderId = null, int? first = null, int? garage = null, GraphQLGeoPolygonInputType[] geoPolygon = null, int? halfBath = null, string homeStatus = null, bool? includeMpcs = null, bool? includeBasicMpc = null, int?[] listingIds = null, int?[] imageIds = null, int? livAreas = null, int? marketId = null, string marketIds = null, int? masterBrLoc = null, float? maxLat = null, float? maxLng = null, bool? mfr = null, float? minLat = null, float? minLng = null, DateTime? moveInDate = null, bool? noBoyl = null, float? numStory = null, int? offset = null, float? originLat = null, float? originLng = null, int? page = null, int? pageSize = null, int? planId = null, string planIds = null, string planName = null, string postalCode = null, string postalCodes = null, float? prHi = null, float? prLo = null, string projectTypeCode = null, bool? promo = null, bool? qmi = null, float? radius = null, string radiusUnit = null, int? sfhigh = null, int? sflow = null, string soldDataToken = null, DateTime? soldFromDate = null, GraphQLSortValueInputType[] sort = null, string sortFirstBy = null, int? specId = null, string specIds = null, GraphQLSpecTypeInputType[] specTypes = null, string state = null, float? story = null, bool? adult = null, bool? ageRestricted = null, bool? gated = null, bool? golf = null, bool? green = null, bool? nature = null, bool? parks = null, bool? pool = null, bool? sports = null, bool? views = null, bool? waterfront = null, bool? hasCommCenter = null, bool? hasEvent = null, bool? hasConPromo = null, bool? hasAgePromo = null, bool? hasHotDeals = null, bool? hasLuxuryHomes = null, bool? hasViewOnly = null, bool? hasBuilderReview = null, bool? isHotHome = null, bool? isMaster = null, bool? isUrban = null, bool? isCondoType = null, bool? isMultiFamily = null, bool? isSingleFamily = null, bool? isTownHome = null, Func<GraphQLSearchAllHomes, GraphQLSearchAllHomes> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return new GraphQLQuery<GraphQLSearchAllHomes>(this._graphQlHttpClient, true, currentContext, fields, sessionToken, partnerId, alphaResults, aPromo, basicListingToTheEnd, bath, bcType, bed, brandId, brandIds, builderId, builderIds, cities, city, commId, parentCommId, commIds, commStatus, communityName, counties, county, cPromo, custom, customBuilderLocations, customResults, dist, districtIds, excludeBasiCommunities, excludeRegularComms, excludeCommunitiesByBuilderId, first, garage, geoPolygon, halfBath, homeStatus, includeMpcs, includeBasicMpc, listingIds, imageIds, livAreas, marketId, marketIds, masterBrLoc, maxLat, maxLng, mfr, minLat, minLng, moveInDate, noBoyl, numStory, offset, originLat, originLng, page, pageSize, planId, planIds, planName, postalCode, postalCodes, prHi, prLo, projectTypeCode, promo, qmi, radius, radiusUnit, sfhigh, sflow, soldDataToken, soldFromDate, sort, sortFirstBy, specId, specIds, specTypes, state, story, adult, ageRestricted, gated, golf, green, nature, parks, pool, sports, views, waterfront, hasCommCenter, hasEvent, hasConPromo, hasAgePromo, hasHotDeals, hasLuxuryHomes, hasViewOnly, hasBuilderReview, isHotHome, isMaster, isUrban, isCondoType, isMultiFamily, isSingleFamily, isTownHome);
	}

	/// <param name="partnerId">Partner Id</param>
	/// <param name="alphaResults">Alpha Results</param>
	/// <param name="aPromo">Has Agent Promo</param>
	/// <param name="basicListingToTheEnd">Basic Listing to End</param>
	/// <param name="bath">Bath</param>
	/// <param name="bcType">Community Type (C: Coming soon; G: Gated; N: Normal; X: Closed)</param>
	/// <param name="bed">Bed</param>
	/// <param name="brandId">Brand Id</param>
	/// <param name="brandIds">Brands</param>
	/// <param name="builderId">Builder Id</param>
	/// <param name="builderIds">Builder Ids</param>
	/// <param name="cities">Cities</param>
	/// <param name="city">City</param>
	/// <param name="commId">Community Id</param>
	/// <param name="parentCommId">Community Id</param>
	/// <param name="commIds">Community Ids</param>
	/// <param name="commStatus">Community Status (N: Active; C: Coming Soon; G: Grand Opening; X: Close Out; CG: ComingSoonGrandOpening)</param>
	/// <param name="communityName">Community Name</param>
	/// <param name="counties">Counties</param>
	/// <param name="county">County</param>
	/// <param name="cPromo">Has Consumer Promo</param>
	/// <param name="custom">Custom</param>
	/// <param name="customBuilderLocations">Custom Builder Locations</param>
	/// <param name="customResults">Custom Results</param>
	/// <param name="dist">Distance</param>
	/// <param name="districtIds">District Ids</param>
	/// <param name="excludeBasiCommunities">Exclude Basic Communities</param>
	/// <param name="excludeRegularComms">Exclude Basic Communities</param>
	/// <param name="excludeCommunitiesByBuilderId">Exclude Communities by Builder Id</param>
	/// <param name="first">First</param>
	/// <param name="garage">Garage</param>
	/// <param name="geoPolygon">Array or coordinates to apply polygon/draw on map search</param>
	/// <param name="halfBath">Half Bath</param>
	/// <param name="homeStatus">Home Status (R: Ready to Build; UC: Under Construction; A: Available Now; M: Model; S: Sold)</param>
	/// <param name="includeMpcs">Include Master Plan Communities</param>
	/// <param name="includeBasicMpc">Include Basic Master Plan Community</param>
	/// <param name="listingIds">LivingIds</param>
	/// <param name="imageIds">LivingIds</param>
	/// <param name="livAreas">Living Areas</param>
	/// <param name="marketId">Market Id</param>
	/// <param name="marketIds">Market Ids</param>
	/// <param name="masterBrLoc">Master Bed Location</param>
	/// <param name="maxLat">Max Latitude</param>
	/// <param name="maxLng">Max Longitude</param>
	/// <param name="mfr">Manufactured</param>
	/// <param name="minLat">Min Latitude</param>
	/// <param name="minLng">Min Longitude</param>
	/// <param name="moveInDate">Move in date</param>
	/// <param name="noBoyl">No Boyl</param>
	/// <param name="numStory">Num Story</param>
	/// <param name="offset">Skip first N records</param>
	/// <param name="originLat">Origin Latitude</param>
	/// <param name="originLng">Origin Longitude</param>
	/// <param name="page">Page</param>
	/// <param name="pageSize">Page Size</param>
	/// <param name="planId">Plan Id</param>
	/// <param name="planIds">Plan Ids</param>
	/// <param name="planName">Plan name</param>
	/// <param name="postalCode">Postal Code</param>
	/// <param name="postalCodes">Postal Codes</param>
	/// <param name="prHi">Higher Price</param>
	/// <param name="prLo">Minimum Price</param>
	/// <param name="projectTypeCode">Project Type: (BYL: Boyl; CST: Custom; MFR: Manufactured)</param>
	/// <param name="promo">Has Promo</param>
	/// <param name="qmi">Quick Move In</param>
	/// <param name="radius">Radius</param>
	/// <param name="radiusUnit">Raius Unit</param>
	/// <param name="sfhigh">Maximum Square Feet</param>
	/// <param name="sflow">Minimum Square Feet</param>
	/// <param name="soldDataToken">If provided and it's valid, it will return Sold Data info, Otherwise will filter out sold data records even if HomeStatus: S is specified</param>
	/// <param name="soldFromDate">Sold From Date</param>
	/// <param name="sort">List of fields to sort</param>
	/// <param name="sortFirstBy">Sort First By</param>
	/// <param name="specId">Spec Id</param>
	/// <param name="specIds">Spec Ids</param>
	/// <param name="specTypes">Spec Type</param>
	/// <param name="state">State</param>
	/// <param name="story">Story</param>
	/// <param name="adult">Is for Adult</param>
	/// <param name="ageRestricted">Age Restricted</param>
	/// <param name="gated">Gated</param>
	/// <param name="golf">Golf</param>
	/// <param name="green">Green</param>
	/// <param name="nature">Has Nature areas</param>
	/// <param name="parks">Parks</param>
	/// <param name="pool">Pool</param>
	/// <param name="sports">Sports</param>
	/// <param name="views">Views</param>
	/// <param name="waterfront">Water front</param>
	/// <param name="hasCommCenter">Has Community Center</param>
	/// <param name="hasEvent">Has Event </param>
	/// <param name="hasConPromo">Has Consumer Promo</param>
	/// <param name="hasAgePromo">Has Agent Promotion</param>
	/// <param name="hasHotDeals">Has Hot Home</param>
	/// <param name="hasLuxuryHomes">Has Hot Home</param>
	/// <param name="hasViewOnly">Has View Only</param>
	/// <param name="hasBuilderReview">Has Builder Review</param>
	/// <param name="isHotHome">Is Hot Home</param>
	/// <param name="isMaster">Is Master </param>
	/// <param name="isUrban">Is Urban</param>
	/// <param name="isCondoType">Is Condo Type</param>
	/// <param name="isMultiFamily">Whether the community has at least one multi family home, or when the home type is MF at home level</param>
	/// <param name="isSingleFamily">Whether the community has at least one single family home, or the home is SF at home level</param>
	/// <param name="isTownHome">Is Town Home</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("searchAllHomes")]
	public Task<List<GraphQLSearchAllHomes>>SearchAllHomesAsync (string sessionToken, int? partnerId, bool? alphaResults = null, bool? aPromo = null, bool? basicListingToTheEnd = null, int? bath = null, string bcType = null, int? bed = null, int? brandId = null, int?[] brandIds = null, int? builderId = null, string builderIds = null, string cities = null, string city = null, int? commId = null, int? parentCommId = null, string commIds = null, string commStatus = null, string communityName = null, string counties = null, string county = null, bool? cPromo = null, bool? custom = null, bool? customBuilderLocations = null, bool? customResults = null, float? dist = null, string districtIds = null, bool? excludeBasiCommunities = null, bool? excludeRegularComms = null, bool? excludeCommunitiesByBuilderId = null, int? first = null, int? garage = null, GraphQLGeoPolygonInputType[] geoPolygon = null, int? halfBath = null, string homeStatus = null, bool? includeMpcs = null, bool? includeBasicMpc = null, int?[] listingIds = null, int?[] imageIds = null, int? livAreas = null, int? marketId = null, string marketIds = null, int? masterBrLoc = null, float? maxLat = null, float? maxLng = null, bool? mfr = null, float? minLat = null, float? minLng = null, DateTime? moveInDate = null, bool? noBoyl = null, float? numStory = null, int? offset = null, float? originLat = null, float? originLng = null, int? page = null, int? pageSize = null, int? planId = null, string planIds = null, string planName = null, string postalCode = null, string postalCodes = null, float? prHi = null, float? prLo = null, string projectTypeCode = null, bool? promo = null, bool? qmi = null, float? radius = null, string radiusUnit = null, int? sfhigh = null, int? sflow = null, string soldDataToken = null, DateTime? soldFromDate = null, GraphQLSortValueInputType[] sort = null, string sortFirstBy = null, int? specId = null, string specIds = null, GraphQLSpecTypeInputType[] specTypes = null, string state = null, float? story = null, bool? adult = null, bool? ageRestricted = null, bool? gated = null, bool? golf = null, bool? green = null, bool? nature = null, bool? parks = null, bool? pool = null, bool? sports = null, bool? views = null, bool? waterfront = null, bool? hasCommCenter = null, bool? hasEvent = null, bool? hasConPromo = null, bool? hasAgePromo = null, bool? hasHotDeals = null, bool? hasLuxuryHomes = null, bool? hasViewOnly = null, bool? hasBuilderReview = null, bool? isHotHome = null, bool? isMaster = null, bool? isUrban = null, bool? isCondoType = null, bool? isMultiFamily = null, bool? isSingleFamily = null, bool? isTownHome = null, Func<GraphQLSearchAllHomes, GraphQLSearchAllHomes> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return Task.Run(() => new GraphQLQuery<GraphQLSearchAllHomes>(this._graphQlHttpClient, true, currentContext, fields, sessionToken, partnerId, alphaResults, aPromo, basicListingToTheEnd, bath, bcType, bed, brandId, brandIds, builderId, builderIds, cities, city, commId, parentCommId, commIds, commStatus, communityName, counties, county, cPromo, custom, customBuilderLocations, customResults, dist, districtIds, excludeBasiCommunities, excludeRegularComms, excludeCommunitiesByBuilderId, first, garage, geoPolygon, halfBath, homeStatus, includeMpcs, includeBasicMpc, listingIds, imageIds, livAreas, marketId, marketIds, masterBrLoc, maxLat, maxLng, mfr, minLat, minLng, moveInDate, noBoyl, numStory, offset, originLat, originLng, page, pageSize, planId, planIds, planName, postalCode, postalCodes, prHi, prLo, projectTypeCode, promo, qmi, radius, radiusUnit, sfhigh, sflow, soldDataToken, soldFromDate, sort, sortFirstBy, specId, specIds, specTypes, state, story, adult, ageRestricted, gated, golf, green, nature, parks, pool, sports, views, waterfront, hasCommCenter, hasEvent, hasConPromo, hasAgePromo, hasHotDeals, hasLuxuryHomes, hasViewOnly, hasBuilderReview, isHotHome, isMaster, isUrban, isCondoType, isMultiFamily, isSingleFamily, isTownHome).ToList());
	}

	/// <param name="alphaResults">Alpha Results</param>
	/// <param name="aPromo">Has Agent Promo</param>
	/// <param name="basicListingToTheEnd">Basic Listing to End</param>
	/// <param name="bath">Bath</param>
	/// <param name="bcType">Community Type (C: Coming soon; G: Gated; N: Normal; X: Closed)</param>
	/// <param name="bed">Bed</param>
	/// <param name="brandId">Brand Id</param>
	/// <param name="brandIds">Brands</param>
	/// <param name="builderId">Builder Id</param>
	/// <param name="builderIds">Builder Ids</param>
	/// <param name="cities">Cities</param>
	/// <param name="city">City</param>
	/// <param name="commId">Community Id</param>
	/// <param name="parentCommId">Community Id</param>
	/// <param name="commIds">Community Ids</param>
	/// <param name="commStatus">Community Status (N: Active; C: Coming Soon; G: Grand Opening; X: Close Out; CG: ComingSoonGrandOpening)</param>
	/// <param name="communityName">Community Name</param>
	/// <param name="counties">Counties</param>
	/// <param name="county">County</param>
	/// <param name="cPromo">Has Consumer Promo</param>
	/// <param name="custom">Custom</param>
	/// <param name="customBuilderLocations">Custom Builder Locations</param>
	/// <param name="customResults">Custom Results</param>
	/// <param name="dist">Distance</param>
	/// <param name="districtIds">District Ids</param>
	/// <param name="excludeBasiCommunities">Exclude Basic Communities</param>
	/// <param name="excludeRegularComms">Exclude Basic Communities</param>
	/// <param name="excludeCommunitiesByBuilderId">Exclude Communities by Builder Id</param>
	/// <param name="first">First</param>
	/// <param name="garage">Garage</param>
	/// <param name="geoPolygon">Array or coordinates to apply polygon/draw on map search</param>
	/// <param name="halfBath">Half Bath</param>
	/// <param name="homeStatus">Home Status (R: Ready to Build; UC: Under Construction; A: Available Now; M: Model; S: Sold)</param>
	/// <param name="includeMpcs">Include Master Plan Communities</param>
	/// <param name="includeBasicMpc">Include Basic Master Plan Community</param>
	/// <param name="listingIds">LivingIds</param>
	/// <param name="imageIds">LivingIds</param>
	/// <param name="livAreas">Living Areas</param>
	/// <param name="marketId">Market Id</param>
	/// <param name="marketIds">Market Ids</param>
	/// <param name="masterBrLoc">Master Bed Location</param>
	/// <param name="maxLat">Max Latitude</param>
	/// <param name="maxLng">Max Longitude</param>
	/// <param name="mfr">Manufactured</param>
	/// <param name="minLat">Min Latitude</param>
	/// <param name="minLng">Min Longitude</param>
	/// <param name="moveInDate">Move in date</param>
	/// <param name="noBoyl">No Boyl</param>
	/// <param name="numStory">Num Story</param>
	/// <param name="offset">Skip first N records</param>
	/// <param name="originLat">Origin Latitude</param>
	/// <param name="originLng">Origin Longitude</param>
	/// <param name="page">Page</param>
	/// <param name="pageSize">Page Size</param>
	/// <param name="partnerId">Partner Id</param>
	/// <param name="planId">Plan Id</param>
	/// <param name="planIds">Plan Ids</param>
	/// <param name="planName">Plan name</param>
	/// <param name="postalCode">Postal Code</param>
	/// <param name="postalCodes">Postal Codes</param>
	/// <param name="prHi">Higher Price</param>
	/// <param name="prLo">Minimum Price</param>
	/// <param name="projectTypeCode">Project Type: (BYL: Boyl; CST: Custom; MFR: Manufactured)</param>
	/// <param name="promo">Has Promo</param>
	/// <param name="qmi">Quick Move In</param>
	/// <param name="radius">Radius</param>
	/// <param name="radiusUnit">Raius Unit</param>
	/// <param name="sfhigh">Maximum Square Feet</param>
	/// <param name="sflow">Minimum Square Feet</param>
	/// <param name="soldDataToken">If provided and it's valid, it will return Sold Data info, Otherwise will filter out sold data records even if HomeStatus: S is specified</param>
	/// <param name="soldFromDate">Sold From Date</param>
	/// <param name="sort">List of fields to sort</param>
	/// <param name="sortFirstBy">Sort First By</param>
	/// <param name="specId">Spec Id</param>
	/// <param name="specIds">Spec Ids</param>
	/// <param name="specTypes">Spec Type</param>
	/// <param name="state">State</param>
	/// <param name="story">Story</param>
	/// <param name="adult">Is for Adult</param>
	/// <param name="ageRestricted">Age Restricted</param>
	/// <param name="gated">Gated</param>
	/// <param name="golf">Golf</param>
	/// <param name="green">Green</param>
	/// <param name="nature">Has Nature areas</param>
	/// <param name="parks">Parks</param>
	/// <param name="pool">Pool</param>
	/// <param name="sports">Sports</param>
	/// <param name="views">Views</param>
	/// <param name="waterfront">Water front</param>
	/// <param name="hasCommCenter">Has Community Center</param>
	/// <param name="hasEvent">Has Event </param>
	/// <param name="hasConPromo">Has Consumer Promo</param>
	/// <param name="hasAgePromo">Has Agent Promotion</param>
	/// <param name="hasHotDeals">Has Hot Home</param>
	/// <param name="hasLuxuryHomes">Has Hot Home</param>
	/// <param name="hasViewOnly">Has View Only</param>
	/// <param name="hasBuilderReview">Has Builder Review</param>
	/// <param name="isHotHome">Is Hot Home</param>
	/// <param name="isMaster">Is Master </param>
	/// <param name="isUrban">Is Urban</param>
	/// <param name="isCondoType">Is Condo Type</param>
	/// <param name="isMultiFamily">Whether the community has at least one multi family home, or when the home type is MF at home level</param>
	/// <param name="isSingleFamily">Whether the community has at least one single family home, or the home is SF at home level</param>
	/// <param name="isTownHome">Is Town Home</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("searchAllHomesCounts")]
	public GraphQLQuery<GraphQLResultHomeCounts>SearchAllHomesCounts (string sessionToken, int? partnerId, bool? alphaResults = null, bool? aPromo = null, bool? basicListingToTheEnd = null, int? bath = null, string bcType = null, int? bed = null, int? brandId = null, int?[] brandIds = null, int? builderId = null, string builderIds = null, string cities = null, string city = null, int? commId = null, int? parentCommId = null, string commIds = null, string commStatus = null, string communityName = null, string counties = null, string county = null, bool? cPromo = null, bool? custom = null, bool? customBuilderLocations = null, bool? customResults = null, float? dist = null, string districtIds = null, bool? excludeBasiCommunities = null, bool? excludeRegularComms = null, bool? excludeCommunitiesByBuilderId = null, int? first = null, int? garage = null, GraphQLGeoPolygonInputType[] geoPolygon = null, int? halfBath = null, string homeStatus = null, bool? includeMpcs = null, bool? includeBasicMpc = null, int?[] listingIds = null, int?[] imageIds = null, int? livAreas = null, int? marketId = null, string marketIds = null, int? masterBrLoc = null, float? maxLat = null, float? maxLng = null, bool? mfr = null, float? minLat = null, float? minLng = null, DateTime? moveInDate = null, bool? noBoyl = null, float? numStory = null, int? offset = null, float? originLat = null, float? originLng = null, int? page = null, int? pageSize = null, int? planId = null, string planIds = null, string planName = null, string postalCode = null, string postalCodes = null, float? prHi = null, float? prLo = null, string projectTypeCode = null, bool? promo = null, bool? qmi = null, float? radius = null, string radiusUnit = null, int? sfhigh = null, int? sflow = null, string soldDataToken = null, DateTime? soldFromDate = null, GraphQLSortValueInputType[] sort = null, string sortFirstBy = null, int? specId = null, string specIds = null, GraphQLSpecTypeInputType[] specTypes = null, string state = null, float? story = null, bool? adult = null, bool? ageRestricted = null, bool? gated = null, bool? golf = null, bool? green = null, bool? nature = null, bool? parks = null, bool? pool = null, bool? sports = null, bool? views = null, bool? waterfront = null, bool? hasCommCenter = null, bool? hasEvent = null, bool? hasConPromo = null, bool? hasAgePromo = null, bool? hasHotDeals = null, bool? hasLuxuryHomes = null, bool? hasViewOnly = null, bool? hasBuilderReview = null, bool? isHotHome = null, bool? isMaster = null, bool? isUrban = null, bool? isCondoType = null, bool? isMultiFamily = null, bool? isSingleFamily = null, bool? isTownHome = null, Func<GraphQLResultHomeCounts, GraphQLResultHomeCounts> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return new GraphQLQuery<GraphQLResultHomeCounts>(this._graphQlHttpClient, false, currentContext, fields, sessionToken, partnerId, alphaResults, aPromo, basicListingToTheEnd, bath, bcType, bed, brandId, brandIds, builderId, builderIds, cities, city, commId, parentCommId, commIds, commStatus, communityName, counties, county, cPromo, custom, customBuilderLocations, customResults, dist, districtIds, excludeBasiCommunities, excludeRegularComms, excludeCommunitiesByBuilderId, first, garage, geoPolygon, halfBath, homeStatus, includeMpcs, includeBasicMpc, listingIds, imageIds, livAreas, marketId, marketIds, masterBrLoc, maxLat, maxLng, mfr, minLat, minLng, moveInDate, noBoyl, numStory, offset, originLat, originLng, page, pageSize, planId, planIds, planName, postalCode, postalCodes, prHi, prLo, projectTypeCode, promo, qmi, radius, radiusUnit, sfhigh, sflow, soldDataToken, soldFromDate, sort, sortFirstBy, specId, specIds, specTypes, state, story, adult, ageRestricted, gated, golf, green, nature, parks, pool, sports, views, waterfront, hasCommCenter, hasEvent, hasConPromo, hasAgePromo, hasHotDeals, hasLuxuryHomes, hasViewOnly, hasBuilderReview, isHotHome, isMaster, isUrban, isCondoType, isMultiFamily, isSingleFamily, isTownHome);
	}

	/// <param name="partnerId">Partner Id</param>
	/// <param name="alphaResults">Alpha Results</param>
	/// <param name="aPromo">Has Agent Promo</param>
	/// <param name="basicListingToTheEnd">Basic Listing to End</param>
	/// <param name="bath">Bath</param>
	/// <param name="bcType">Community Type (C: Coming soon; G: Gated; N: Normal; X: Closed)</param>
	/// <param name="bed">Bed</param>
	/// <param name="brandId">Brand Id</param>
	/// <param name="brandIds">Brands</param>
	/// <param name="builderId">Builder Id</param>
	/// <param name="builderIds">Builder Ids</param>
	/// <param name="cities">Cities</param>
	/// <param name="city">City</param>
	/// <param name="commId">Community Id</param>
	/// <param name="parentCommId">Community Id</param>
	/// <param name="commIds">Community Ids</param>
	/// <param name="commStatus">Community Status (N: Active; C: Coming Soon; G: Grand Opening; X: Close Out; CG: ComingSoonGrandOpening)</param>
	/// <param name="communityName">Community Name</param>
	/// <param name="counties">Counties</param>
	/// <param name="county">County</param>
	/// <param name="cPromo">Has Consumer Promo</param>
	/// <param name="custom">Custom</param>
	/// <param name="customBuilderLocations">Custom Builder Locations</param>
	/// <param name="customResults">Custom Results</param>
	/// <param name="dist">Distance</param>
	/// <param name="districtIds">District Ids</param>
	/// <param name="excludeBasiCommunities">Exclude Basic Communities</param>
	/// <param name="excludeRegularComms">Exclude Basic Communities</param>
	/// <param name="excludeCommunitiesByBuilderId">Exclude Communities by Builder Id</param>
	/// <param name="first">First</param>
	/// <param name="garage">Garage</param>
	/// <param name="geoPolygon">Array or coordinates to apply polygon/draw on map search</param>
	/// <param name="halfBath">Half Bath</param>
	/// <param name="homeStatus">Home Status (R: Ready to Build; UC: Under Construction; A: Available Now; M: Model; S: Sold)</param>
	/// <param name="includeMpcs">Include Master Plan Communities</param>
	/// <param name="includeBasicMpc">Include Basic Master Plan Community</param>
	/// <param name="listingIds">LivingIds</param>
	/// <param name="imageIds">LivingIds</param>
	/// <param name="livAreas">Living Areas</param>
	/// <param name="marketId">Market Id</param>
	/// <param name="marketIds">Market Ids</param>
	/// <param name="masterBrLoc">Master Bed Location</param>
	/// <param name="maxLat">Max Latitude</param>
	/// <param name="maxLng">Max Longitude</param>
	/// <param name="mfr">Manufactured</param>
	/// <param name="minLat">Min Latitude</param>
	/// <param name="minLng">Min Longitude</param>
	/// <param name="moveInDate">Move in date</param>
	/// <param name="noBoyl">No Boyl</param>
	/// <param name="numStory">Num Story</param>
	/// <param name="offset">Skip first N records</param>
	/// <param name="originLat">Origin Latitude</param>
	/// <param name="originLng">Origin Longitude</param>
	/// <param name="page">Page</param>
	/// <param name="pageSize">Page Size</param>
	/// <param name="planId">Plan Id</param>
	/// <param name="planIds">Plan Ids</param>
	/// <param name="planName">Plan name</param>
	/// <param name="postalCode">Postal Code</param>
	/// <param name="postalCodes">Postal Codes</param>
	/// <param name="prHi">Higher Price</param>
	/// <param name="prLo">Minimum Price</param>
	/// <param name="projectTypeCode">Project Type: (BYL: Boyl; CST: Custom; MFR: Manufactured)</param>
	/// <param name="promo">Has Promo</param>
	/// <param name="qmi">Quick Move In</param>
	/// <param name="radius">Radius</param>
	/// <param name="radiusUnit">Raius Unit</param>
	/// <param name="sfhigh">Maximum Square Feet</param>
	/// <param name="sflow">Minimum Square Feet</param>
	/// <param name="soldDataToken">If provided and it's valid, it will return Sold Data info, Otherwise will filter out sold data records even if HomeStatus: S is specified</param>
	/// <param name="soldFromDate">Sold From Date</param>
	/// <param name="sort">List of fields to sort</param>
	/// <param name="sortFirstBy">Sort First By</param>
	/// <param name="specId">Spec Id</param>
	/// <param name="specIds">Spec Ids</param>
	/// <param name="specTypes">Spec Type</param>
	/// <param name="state">State</param>
	/// <param name="story">Story</param>
	/// <param name="adult">Is for Adult</param>
	/// <param name="ageRestricted">Age Restricted</param>
	/// <param name="gated">Gated</param>
	/// <param name="golf">Golf</param>
	/// <param name="green">Green</param>
	/// <param name="nature">Has Nature areas</param>
	/// <param name="parks">Parks</param>
	/// <param name="pool">Pool</param>
	/// <param name="sports">Sports</param>
	/// <param name="views">Views</param>
	/// <param name="waterfront">Water front</param>
	/// <param name="hasCommCenter">Has Community Center</param>
	/// <param name="hasEvent">Has Event </param>
	/// <param name="hasConPromo">Has Consumer Promo</param>
	/// <param name="hasAgePromo">Has Agent Promotion</param>
	/// <param name="hasHotDeals">Has Hot Home</param>
	/// <param name="hasLuxuryHomes">Has Hot Home</param>
	/// <param name="hasViewOnly">Has View Only</param>
	/// <param name="hasBuilderReview">Has Builder Review</param>
	/// <param name="isHotHome">Is Hot Home</param>
	/// <param name="isMaster">Is Master </param>
	/// <param name="isUrban">Is Urban</param>
	/// <param name="isCondoType">Is Condo Type</param>
	/// <param name="isMultiFamily">Whether the community has at least one multi family home, or when the home type is MF at home level</param>
	/// <param name="isSingleFamily">Whether the community has at least one single family home, or the home is SF at home level</param>
	/// <param name="isTownHome">Is Town Home</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("searchAllHomesCounts")]
	public Task<List<GraphQLResultHomeCounts>>SearchAllHomesCountsAsync (string sessionToken, int? partnerId, bool? alphaResults = null, bool? aPromo = null, bool? basicListingToTheEnd = null, int? bath = null, string bcType = null, int? bed = null, int? brandId = null, int?[] brandIds = null, int? builderId = null, string builderIds = null, string cities = null, string city = null, int? commId = null, int? parentCommId = null, string commIds = null, string commStatus = null, string communityName = null, string counties = null, string county = null, bool? cPromo = null, bool? custom = null, bool? customBuilderLocations = null, bool? customResults = null, float? dist = null, string districtIds = null, bool? excludeBasiCommunities = null, bool? excludeRegularComms = null, bool? excludeCommunitiesByBuilderId = null, int? first = null, int? garage = null, GraphQLGeoPolygonInputType[] geoPolygon = null, int? halfBath = null, string homeStatus = null, bool? includeMpcs = null, bool? includeBasicMpc = null, int?[] listingIds = null, int?[] imageIds = null, int? livAreas = null, int? marketId = null, string marketIds = null, int? masterBrLoc = null, float? maxLat = null, float? maxLng = null, bool? mfr = null, float? minLat = null, float? minLng = null, DateTime? moveInDate = null, bool? noBoyl = null, float? numStory = null, int? offset = null, float? originLat = null, float? originLng = null, int? page = null, int? pageSize = null, int? planId = null, string planIds = null, string planName = null, string postalCode = null, string postalCodes = null, float? prHi = null, float? prLo = null, string projectTypeCode = null, bool? promo = null, bool? qmi = null, float? radius = null, string radiusUnit = null, int? sfhigh = null, int? sflow = null, string soldDataToken = null, DateTime? soldFromDate = null, GraphQLSortValueInputType[] sort = null, string sortFirstBy = null, int? specId = null, string specIds = null, GraphQLSpecTypeInputType[] specTypes = null, string state = null, float? story = null, bool? adult = null, bool? ageRestricted = null, bool? gated = null, bool? golf = null, bool? green = null, bool? nature = null, bool? parks = null, bool? pool = null, bool? sports = null, bool? views = null, bool? waterfront = null, bool? hasCommCenter = null, bool? hasEvent = null, bool? hasConPromo = null, bool? hasAgePromo = null, bool? hasHotDeals = null, bool? hasLuxuryHomes = null, bool? hasViewOnly = null, bool? hasBuilderReview = null, bool? isHotHome = null, bool? isMaster = null, bool? isUrban = null, bool? isCondoType = null, bool? isMultiFamily = null, bool? isSingleFamily = null, bool? isTownHome = null, Func<GraphQLResultHomeCounts, GraphQLResultHomeCounts> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return Task.Run(() => new GraphQLQuery<GraphQLResultHomeCounts>(this._graphQlHttpClient, false, currentContext, fields, sessionToken, partnerId, alphaResults, aPromo, basicListingToTheEnd, bath, bcType, bed, brandId, brandIds, builderId, builderIds, cities, city, commId, parentCommId, commIds, commStatus, communityName, counties, county, cPromo, custom, customBuilderLocations, customResults, dist, districtIds, excludeBasiCommunities, excludeRegularComms, excludeCommunitiesByBuilderId, first, garage, geoPolygon, halfBath, homeStatus, includeMpcs, includeBasicMpc, listingIds, imageIds, livAreas, marketId, marketIds, masterBrLoc, maxLat, maxLng, mfr, minLat, minLng, moveInDate, noBoyl, numStory, offset, originLat, originLng, page, pageSize, planId, planIds, planName, postalCode, postalCodes, prHi, prLo, projectTypeCode, promo, qmi, radius, radiusUnit, sfhigh, sflow, soldDataToken, soldFromDate, sort, sortFirstBy, specId, specIds, specTypes, state, story, adult, ageRestricted, gated, golf, green, nature, parks, pool, sports, views, waterfront, hasCommCenter, hasEvent, hasConPromo, hasAgePromo, hasHotDeals, hasLuxuryHomes, hasViewOnly, hasBuilderReview, isHotHome, isMaster, isUrban, isCondoType, isMultiFamily, isSingleFamily, isTownHome).ToList());
	}

	/// <param name="alphaResults">Alpha Results</param>
	/// <param name="aPromo">Has Agent Promo</param>
	/// <param name="basicListingToTheEnd">Basic Listing to End</param>
	/// <param name="bath">Bath</param>
	/// <param name="bcType">Community Type (C: Coming soon; G: Gated; N: Normal; X: Closed)</param>
	/// <param name="bed">Bed</param>
	/// <param name="brandId">Brand Id</param>
	/// <param name="brandIds">Brands</param>
	/// <param name="builderId">Builder Id</param>
	/// <param name="builderIds">Builder Ids</param>
	/// <param name="cities">Cities</param>
	/// <param name="city">City</param>
	/// <param name="commId">Community Id</param>
	/// <param name="parentCommId">Community Id</param>
	/// <param name="commIds">Community Ids</param>
	/// <param name="commStatus">Community Status (N: Active; C: Coming Soon; G: Grand Opening; X: Close Out; CG: ComingSoonGrandOpening)</param>
	/// <param name="communityName">Community Name</param>
	/// <param name="counties">Counties</param>
	/// <param name="county">County</param>
	/// <param name="cPromo">Has Consumer Promo</param>
	/// <param name="custom">Custom</param>
	/// <param name="customBuilderLocations">Custom Builder Locations</param>
	/// <param name="customResults">Custom Results</param>
	/// <param name="dist">Distance</param>
	/// <param name="districtIds">District Ids</param>
	/// <param name="excludeBasiCommunities">Exclude Basic Communities</param>
	/// <param name="excludeRegularComms">Exclude Basic Communities</param>
	/// <param name="excludeCommunitiesByBuilderId">Exclude Communities by Builder Id</param>
	/// <param name="first">First</param>
	/// <param name="garage">Garage</param>
	/// <param name="geoPolygon">Array or coordinates to apply polygon/draw on map search</param>
	/// <param name="halfBath">Half Bath</param>
	/// <param name="homeStatus">Home Status (R: Ready to Build; UC: Under Construction; A: Available Now; M: Model; S: Sold)</param>
	/// <param name="includeMpcs">Include Master Plan Communities</param>
	/// <param name="includeBasicMpc">Include Basic Master Plan Community</param>
	/// <param name="listingIds">LivingIds</param>
	/// <param name="imageIds">LivingIds</param>
	/// <param name="livAreas">Living Areas</param>
	/// <param name="marketId">Market Id</param>
	/// <param name="marketIds">Market Ids</param>
	/// <param name="masterBrLoc">Master Bed Location</param>
	/// <param name="maxLat">Max Latitude</param>
	/// <param name="maxLng">Max Longitude</param>
	/// <param name="mfr">Manufactured</param>
	/// <param name="minLat">Min Latitude</param>
	/// <param name="minLng">Min Longitude</param>
	/// <param name="moveInDate">Move in date</param>
	/// <param name="noBoyl">No Boyl</param>
	/// <param name="numStory">Num Story</param>
	/// <param name="offset">Skip first N records</param>
	/// <param name="originLat">Origin Latitude</param>
	/// <param name="originLng">Origin Longitude</param>
	/// <param name="page">Page</param>
	/// <param name="pageSize">Page Size</param>
	/// <param name="partnerId">Partner Id</param>
	/// <param name="planId">Plan Id</param>
	/// <param name="planIds">Plan Ids</param>
	/// <param name="planName">Plan name</param>
	/// <param name="postalCode">Postal Code</param>
	/// <param name="postalCodes">Postal Codes</param>
	/// <param name="prHi">Higher Price</param>
	/// <param name="prLo">Minimum Price</param>
	/// <param name="projectTypeCode">Project Type: (BYL: Boyl; CST: Custom; MFR: Manufactured)</param>
	/// <param name="promo">Has Promo</param>
	/// <param name="qmi">Quick Move In</param>
	/// <param name="radius">Radius</param>
	/// <param name="radiusUnit">Raius Unit</param>
	/// <param name="sfhigh">Maximum Square Feet</param>
	/// <param name="sflow">Minimum Square Feet</param>
	/// <param name="soldDataToken">If provided and it's valid, it will return Sold Data info, Otherwise will filter out sold data records even if HomeStatus: S is specified</param>
	/// <param name="soldFromDate">Sold From Date</param>
	/// <param name="sort">List of fields to sort</param>
	/// <param name="sortFirstBy">Sort First By</param>
	/// <param name="specId">Spec Id</param>
	/// <param name="specIds">Spec Ids</param>
	/// <param name="specTypes">Spec Type</param>
	/// <param name="state">State</param>
	/// <param name="story">Story</param>
	/// <param name="adult">Is for Adult</param>
	/// <param name="ageRestricted">Age Restricted</param>
	/// <param name="gated">Gated</param>
	/// <param name="golf">Golf</param>
	/// <param name="green">Green</param>
	/// <param name="nature">Has Nature areas</param>
	/// <param name="parks">Parks</param>
	/// <param name="pool">Pool</param>
	/// <param name="sports">Sports</param>
	/// <param name="views">Views</param>
	/// <param name="waterfront">Water front</param>
	/// <param name="hasCommCenter">Has Community Center</param>
	/// <param name="hasEvent">Has Event </param>
	/// <param name="hasConPromo">Has Consumer Promo</param>
	/// <param name="hasAgePromo">Has Agent Promotion</param>
	/// <param name="hasHotDeals">Has Hot Home</param>
	/// <param name="hasLuxuryHomes">Has Hot Home</param>
	/// <param name="hasViewOnly">Has View Only</param>
	/// <param name="hasBuilderReview">Has Builder Review</param>
	/// <param name="isHotHome">Is Hot Home</param>
	/// <param name="isMaster">Is Master </param>
	/// <param name="isUrban">Is Urban</param>
	/// <param name="isCondoType">Is Condo Type</param>
	/// <param name="isMultiFamily">Whether the community has at least one multi family home, or when the home type is MF at home level</param>
	/// <param name="isSingleFamily">Whether the community has at least one single family home, or the home is SF at home level</param>
	/// <param name="isTownHome">Is Town Home</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("searchAllHomesFacets")]
	public GraphQLQuery<GraphQLApiFacets>SearchAllHomesFacets (string sessionToken, int? partnerId, bool? alphaResults = null, bool? aPromo = null, bool? basicListingToTheEnd = null, int? bath = null, string bcType = null, int? bed = null, int? brandId = null, int?[] brandIds = null, int? builderId = null, string builderIds = null, string cities = null, string city = null, int? commId = null, int? parentCommId = null, string commIds = null, string commStatus = null, string communityName = null, string counties = null, string county = null, bool? cPromo = null, bool? custom = null, bool? customBuilderLocations = null, bool? customResults = null, float? dist = null, string districtIds = null, bool? excludeBasiCommunities = null, bool? excludeRegularComms = null, bool? excludeCommunitiesByBuilderId = null, int? first = null, int? garage = null, GraphQLGeoPolygonInputType[] geoPolygon = null, int? halfBath = null, string homeStatus = null, bool? includeMpcs = null, bool? includeBasicMpc = null, int?[] listingIds = null, int?[] imageIds = null, int? livAreas = null, int? marketId = null, string marketIds = null, int? masterBrLoc = null, float? maxLat = null, float? maxLng = null, bool? mfr = null, float? minLat = null, float? minLng = null, DateTime? moveInDate = null, bool? noBoyl = null, float? numStory = null, int? offset = null, float? originLat = null, float? originLng = null, int? page = null, int? pageSize = null, int? planId = null, string planIds = null, string planName = null, string postalCode = null, string postalCodes = null, float? prHi = null, float? prLo = null, string projectTypeCode = null, bool? promo = null, bool? qmi = null, float? radius = null, string radiusUnit = null, int? sfhigh = null, int? sflow = null, string soldDataToken = null, DateTime? soldFromDate = null, GraphQLSortValueInputType[] sort = null, string sortFirstBy = null, int? specId = null, string specIds = null, GraphQLSpecTypeInputType[] specTypes = null, string state = null, float? story = null, bool? adult = null, bool? ageRestricted = null, bool? gated = null, bool? golf = null, bool? green = null, bool? nature = null, bool? parks = null, bool? pool = null, bool? sports = null, bool? views = null, bool? waterfront = null, bool? hasCommCenter = null, bool? hasEvent = null, bool? hasConPromo = null, bool? hasAgePromo = null, bool? hasHotDeals = null, bool? hasLuxuryHomes = null, bool? hasViewOnly = null, bool? hasBuilderReview = null, bool? isHotHome = null, bool? isMaster = null, bool? isUrban = null, bool? isCondoType = null, bool? isMultiFamily = null, bool? isSingleFamily = null, bool? isTownHome = null, Func<GraphQLApiFacets, GraphQLApiFacets> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return new GraphQLQuery<GraphQLApiFacets>(this._graphQlHttpClient, false, currentContext, fields, sessionToken, partnerId, alphaResults, aPromo, basicListingToTheEnd, bath, bcType, bed, brandId, brandIds, builderId, builderIds, cities, city, commId, parentCommId, commIds, commStatus, communityName, counties, county, cPromo, custom, customBuilderLocations, customResults, dist, districtIds, excludeBasiCommunities, excludeRegularComms, excludeCommunitiesByBuilderId, first, garage, geoPolygon, halfBath, homeStatus, includeMpcs, includeBasicMpc, listingIds, imageIds, livAreas, marketId, marketIds, masterBrLoc, maxLat, maxLng, mfr, minLat, minLng, moveInDate, noBoyl, numStory, offset, originLat, originLng, page, pageSize, planId, planIds, planName, postalCode, postalCodes, prHi, prLo, projectTypeCode, promo, qmi, radius, radiusUnit, sfhigh, sflow, soldDataToken, soldFromDate, sort, sortFirstBy, specId, specIds, specTypes, state, story, adult, ageRestricted, gated, golf, green, nature, parks, pool, sports, views, waterfront, hasCommCenter, hasEvent, hasConPromo, hasAgePromo, hasHotDeals, hasLuxuryHomes, hasViewOnly, hasBuilderReview, isHotHome, isMaster, isUrban, isCondoType, isMultiFamily, isSingleFamily, isTownHome);
	}

	/// <param name="partnerId">Partner Id</param>
	/// <param name="alphaResults">Alpha Results</param>
	/// <param name="aPromo">Has Agent Promo</param>
	/// <param name="basicListingToTheEnd">Basic Listing to End</param>
	/// <param name="bath">Bath</param>
	/// <param name="bcType">Community Type (C: Coming soon; G: Gated; N: Normal; X: Closed)</param>
	/// <param name="bed">Bed</param>
	/// <param name="brandId">Brand Id</param>
	/// <param name="brandIds">Brands</param>
	/// <param name="builderId">Builder Id</param>
	/// <param name="builderIds">Builder Ids</param>
	/// <param name="cities">Cities</param>
	/// <param name="city">City</param>
	/// <param name="commId">Community Id</param>
	/// <param name="parentCommId">Community Id</param>
	/// <param name="commIds">Community Ids</param>
	/// <param name="commStatus">Community Status (N: Active; C: Coming Soon; G: Grand Opening; X: Close Out; CG: ComingSoonGrandOpening)</param>
	/// <param name="communityName">Community Name</param>
	/// <param name="counties">Counties</param>
	/// <param name="county">County</param>
	/// <param name="cPromo">Has Consumer Promo</param>
	/// <param name="custom">Custom</param>
	/// <param name="customBuilderLocations">Custom Builder Locations</param>
	/// <param name="customResults">Custom Results</param>
	/// <param name="dist">Distance</param>
	/// <param name="districtIds">District Ids</param>
	/// <param name="excludeBasiCommunities">Exclude Basic Communities</param>
	/// <param name="excludeRegularComms">Exclude Basic Communities</param>
	/// <param name="excludeCommunitiesByBuilderId">Exclude Communities by Builder Id</param>
	/// <param name="first">First</param>
	/// <param name="garage">Garage</param>
	/// <param name="geoPolygon">Array or coordinates to apply polygon/draw on map search</param>
	/// <param name="halfBath">Half Bath</param>
	/// <param name="homeStatus">Home Status (R: Ready to Build; UC: Under Construction; A: Available Now; M: Model; S: Sold)</param>
	/// <param name="includeMpcs">Include Master Plan Communities</param>
	/// <param name="includeBasicMpc">Include Basic Master Plan Community</param>
	/// <param name="listingIds">LivingIds</param>
	/// <param name="imageIds">LivingIds</param>
	/// <param name="livAreas">Living Areas</param>
	/// <param name="marketId">Market Id</param>
	/// <param name="marketIds">Market Ids</param>
	/// <param name="masterBrLoc">Master Bed Location</param>
	/// <param name="maxLat">Max Latitude</param>
	/// <param name="maxLng">Max Longitude</param>
	/// <param name="mfr">Manufactured</param>
	/// <param name="minLat">Min Latitude</param>
	/// <param name="minLng">Min Longitude</param>
	/// <param name="moveInDate">Move in date</param>
	/// <param name="noBoyl">No Boyl</param>
	/// <param name="numStory">Num Story</param>
	/// <param name="offset">Skip first N records</param>
	/// <param name="originLat">Origin Latitude</param>
	/// <param name="originLng">Origin Longitude</param>
	/// <param name="page">Page</param>
	/// <param name="pageSize">Page Size</param>
	/// <param name="planId">Plan Id</param>
	/// <param name="planIds">Plan Ids</param>
	/// <param name="planName">Plan name</param>
	/// <param name="postalCode">Postal Code</param>
	/// <param name="postalCodes">Postal Codes</param>
	/// <param name="prHi">Higher Price</param>
	/// <param name="prLo">Minimum Price</param>
	/// <param name="projectTypeCode">Project Type: (BYL: Boyl; CST: Custom; MFR: Manufactured)</param>
	/// <param name="promo">Has Promo</param>
	/// <param name="qmi">Quick Move In</param>
	/// <param name="radius">Radius</param>
	/// <param name="radiusUnit">Raius Unit</param>
	/// <param name="sfhigh">Maximum Square Feet</param>
	/// <param name="sflow">Minimum Square Feet</param>
	/// <param name="soldDataToken">If provided and it's valid, it will return Sold Data info, Otherwise will filter out sold data records even if HomeStatus: S is specified</param>
	/// <param name="soldFromDate">Sold From Date</param>
	/// <param name="sort">List of fields to sort</param>
	/// <param name="sortFirstBy">Sort First By</param>
	/// <param name="specId">Spec Id</param>
	/// <param name="specIds">Spec Ids</param>
	/// <param name="specTypes">Spec Type</param>
	/// <param name="state">State</param>
	/// <param name="story">Story</param>
	/// <param name="adult">Is for Adult</param>
	/// <param name="ageRestricted">Age Restricted</param>
	/// <param name="gated">Gated</param>
	/// <param name="golf">Golf</param>
	/// <param name="green">Green</param>
	/// <param name="nature">Has Nature areas</param>
	/// <param name="parks">Parks</param>
	/// <param name="pool">Pool</param>
	/// <param name="sports">Sports</param>
	/// <param name="views">Views</param>
	/// <param name="waterfront">Water front</param>
	/// <param name="hasCommCenter">Has Community Center</param>
	/// <param name="hasEvent">Has Event </param>
	/// <param name="hasConPromo">Has Consumer Promo</param>
	/// <param name="hasAgePromo">Has Agent Promotion</param>
	/// <param name="hasHotDeals">Has Hot Home</param>
	/// <param name="hasLuxuryHomes">Has Hot Home</param>
	/// <param name="hasViewOnly">Has View Only</param>
	/// <param name="hasBuilderReview">Has Builder Review</param>
	/// <param name="isHotHome">Is Hot Home</param>
	/// <param name="isMaster">Is Master </param>
	/// <param name="isUrban">Is Urban</param>
	/// <param name="isCondoType">Is Condo Type</param>
	/// <param name="isMultiFamily">Whether the community has at least one multi family home, or when the home type is MF at home level</param>
	/// <param name="isSingleFamily">Whether the community has at least one single family home, or the home is SF at home level</param>
	/// <param name="isTownHome">Is Town Home</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("searchAllHomesFacets")]
	public Task<List<GraphQLApiFacets>>SearchAllHomesFacetsAsync (string sessionToken, int? partnerId, bool? alphaResults = null, bool? aPromo = null, bool? basicListingToTheEnd = null, int? bath = null, string bcType = null, int? bed = null, int? brandId = null, int?[] brandIds = null, int? builderId = null, string builderIds = null, string cities = null, string city = null, int? commId = null, int? parentCommId = null, string commIds = null, string commStatus = null, string communityName = null, string counties = null, string county = null, bool? cPromo = null, bool? custom = null, bool? customBuilderLocations = null, bool? customResults = null, float? dist = null, string districtIds = null, bool? excludeBasiCommunities = null, bool? excludeRegularComms = null, bool? excludeCommunitiesByBuilderId = null, int? first = null, int? garage = null, GraphQLGeoPolygonInputType[] geoPolygon = null, int? halfBath = null, string homeStatus = null, bool? includeMpcs = null, bool? includeBasicMpc = null, int?[] listingIds = null, int?[] imageIds = null, int? livAreas = null, int? marketId = null, string marketIds = null, int? masterBrLoc = null, float? maxLat = null, float? maxLng = null, bool? mfr = null, float? minLat = null, float? minLng = null, DateTime? moveInDate = null, bool? noBoyl = null, float? numStory = null, int? offset = null, float? originLat = null, float? originLng = null, int? page = null, int? pageSize = null, int? planId = null, string planIds = null, string planName = null, string postalCode = null, string postalCodes = null, float? prHi = null, float? prLo = null, string projectTypeCode = null, bool? promo = null, bool? qmi = null, float? radius = null, string radiusUnit = null, int? sfhigh = null, int? sflow = null, string soldDataToken = null, DateTime? soldFromDate = null, GraphQLSortValueInputType[] sort = null, string sortFirstBy = null, int? specId = null, string specIds = null, GraphQLSpecTypeInputType[] specTypes = null, string state = null, float? story = null, bool? adult = null, bool? ageRestricted = null, bool? gated = null, bool? golf = null, bool? green = null, bool? nature = null, bool? parks = null, bool? pool = null, bool? sports = null, bool? views = null, bool? waterfront = null, bool? hasCommCenter = null, bool? hasEvent = null, bool? hasConPromo = null, bool? hasAgePromo = null, bool? hasHotDeals = null, bool? hasLuxuryHomes = null, bool? hasViewOnly = null, bool? hasBuilderReview = null, bool? isHotHome = null, bool? isMaster = null, bool? isUrban = null, bool? isCondoType = null, bool? isMultiFamily = null, bool? isSingleFamily = null, bool? isTownHome = null, Func<GraphQLApiFacets, GraphQLApiFacets> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return Task.Run(() => new GraphQLQuery<GraphQLApiFacets>(this._graphQlHttpClient, false, currentContext, fields, sessionToken, partnerId, alphaResults, aPromo, basicListingToTheEnd, bath, bcType, bed, brandId, brandIds, builderId, builderIds, cities, city, commId, parentCommId, commIds, commStatus, communityName, counties, county, cPromo, custom, customBuilderLocations, customResults, dist, districtIds, excludeBasiCommunities, excludeRegularComms, excludeCommunitiesByBuilderId, first, garage, geoPolygon, halfBath, homeStatus, includeMpcs, includeBasicMpc, listingIds, imageIds, livAreas, marketId, marketIds, masterBrLoc, maxLat, maxLng, mfr, minLat, minLng, moveInDate, noBoyl, numStory, offset, originLat, originLng, page, pageSize, planId, planIds, planName, postalCode, postalCodes, prHi, prLo, projectTypeCode, promo, qmi, radius, radiusUnit, sfhigh, sflow, soldDataToken, soldFromDate, sort, sortFirstBy, specId, specIds, specTypes, state, story, adult, ageRestricted, gated, golf, green, nature, parks, pool, sports, views, waterfront, hasCommCenter, hasEvent, hasConPromo, hasAgePromo, hasHotDeals, hasLuxuryHomes, hasViewOnly, hasBuilderReview, isHotHome, isMaster, isUrban, isCondoType, isMultiFamily, isSingleFamily, isTownHome).ToList());
	}

	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("searchAllPreferred")]
	public GraphQLQuery<GraphQLSearchAllPreferred>SearchAllPreferred (string sessionToken, int? accountId = null, int? listingId = null, GraphQLListingType? listingType = null, int? soldSpecId = null, Func<GraphQLSearchAllPreferred, GraphQLSearchAllPreferred> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return new GraphQLQuery<GraphQLSearchAllPreferred>(this._graphQlHttpClient, true, currentContext, fields, sessionToken, accountId, listingId, listingType, soldSpecId);
	}

	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("searchAllPreferred")]
	public Task<List<GraphQLSearchAllPreferred>>SearchAllPreferredAsync (string sessionToken, int? accountId = null, int? listingId = null, GraphQLListingType? listingType = null, int? soldSpecId = null, Func<GraphQLSearchAllPreferred, GraphQLSearchAllPreferred> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return Task.Run(() => new GraphQLQuery<GraphQLSearchAllPreferred>(this._graphQlHttpClient, true, currentContext, fields, sessionToken, accountId, listingId, listingType, soldSpecId).ToList());
	}

	/// <param name="imgCat">Image Category</param>
	/// <param name="imgColorType">Image Color Type</param>
	/// <param name="imgId">Image Id</param>
	/// <param name="imgIds">Image Ids</param>
	/// <param name="imgType">Image Type Code</param>
	/// <param name="minImgHeight">Image Height</param>
	/// <param name="minImgWidth">Image Width</param>
	/// <param name="alphaResults">Alpha Results</param>
	/// <param name="aPromo">Has Agent Promo</param>
	/// <param name="basicListingToTheEnd">Basic Listing to End</param>
	/// <param name="bath">Bath</param>
	/// <param name="bcType">Community Type (C: Coming soon; G: Gated; N: Normal; X: Closed)</param>
	/// <param name="bed">Bed</param>
	/// <param name="brandId">Brand Id</param>
	/// <param name="brandIds">Brands</param>
	/// <param name="builderId">Builder Id</param>
	/// <param name="builderIds">Builder Ids</param>
	/// <param name="cities">Cities</param>
	/// <param name="city">City</param>
	/// <param name="commId">Community Id</param>
	/// <param name="parentCommId">Community Id</param>
	/// <param name="commIds">Community Ids</param>
	/// <param name="commStatus">Community Status (N: Active; C: Coming Soon; G: Grand Opening; X: Close Out; CG: ComingSoonGrandOpening)</param>
	/// <param name="communityName">Community Name</param>
	/// <param name="counties">Counties</param>
	/// <param name="county">County</param>
	/// <param name="cPromo">Has Consumer Promo</param>
	/// <param name="custom">Custom</param>
	/// <param name="customBuilderLocations">Custom Builder Locations</param>
	/// <param name="customResults">Custom Results</param>
	/// <param name="dist">Distance</param>
	/// <param name="districtIds">District Ids</param>
	/// <param name="excludeBasiCommunities">Exclude Basic Communities</param>
	/// <param name="excludeRegularComms">Exclude Basic Communities</param>
	/// <param name="excludeCommunitiesByBuilderId">Exclude Communities by Builder Id</param>
	/// <param name="first">First</param>
	/// <param name="garage">Garage</param>
	/// <param name="geoPolygon">Array or coordinates to apply polygon/draw on map search</param>
	/// <param name="halfBath">Half Bath</param>
	/// <param name="homeStatus">Home Status (R: Ready to Build; UC: Under Construction; A: Available Now; M: Model; S: Sold)</param>
	/// <param name="includeMpcs">Include Master Plan Communities</param>
	/// <param name="includeBasicMpc">Include Basic Master Plan Community</param>
	/// <param name="listingIds">LivingIds</param>
	/// <param name="imageIds">LivingIds</param>
	/// <param name="livAreas">Living Areas</param>
	/// <param name="marketId">Market Id</param>
	/// <param name="marketIds">Market Ids</param>
	/// <param name="masterBrLoc">Master Bed Location</param>
	/// <param name="maxLat">Max Latitude</param>
	/// <param name="maxLng">Max Longitude</param>
	/// <param name="mfr">Manufactured</param>
	/// <param name="minLat">Min Latitude</param>
	/// <param name="minLng">Min Longitude</param>
	/// <param name="moveInDate">Move in date</param>
	/// <param name="noBoyl">No Boyl</param>
	/// <param name="numStory">Num Story</param>
	/// <param name="offset">Skip first N records</param>
	/// <param name="originLat">Origin Latitude</param>
	/// <param name="originLng">Origin Longitude</param>
	/// <param name="page">Page</param>
	/// <param name="pageSize">Page Size</param>
	/// <param name="partnerId">Partner Id</param>
	/// <param name="planId">Plan Id</param>
	/// <param name="planIds">Plan Ids</param>
	/// <param name="planName">Plan name</param>
	/// <param name="postalCode">Postal Code</param>
	/// <param name="postalCodes">Postal Codes</param>
	/// <param name="prHi">Higher Price</param>
	/// <param name="prLo">Minimum Price</param>
	/// <param name="projectTypeCode">Project Type: (BYL: Boyl; CST: Custom; MFR: Manufactured)</param>
	/// <param name="promo">Has Promo</param>
	/// <param name="qmi">Quick Move In</param>
	/// <param name="radius">Radius</param>
	/// <param name="radiusUnit">Raius Unit</param>
	/// <param name="sfhigh">Maximum Square Feet</param>
	/// <param name="sflow">Minimum Square Feet</param>
	/// <param name="soldDataToken">If provided and it's valid, it will return Sold Data info, Otherwise will filter out sold data records even if HomeStatus: S is specified</param>
	/// <param name="soldFromDate">Sold From Date</param>
	/// <param name="sort">List of fields to sort</param>
	/// <param name="sortFirstBy">Sort First By</param>
	/// <param name="specId">Spec Id</param>
	/// <param name="specIds">Spec Ids</param>
	/// <param name="specTypes">Spec Type</param>
	/// <param name="state">State</param>
	/// <param name="story">Story</param>
	/// <param name="adult">Is for Adult</param>
	/// <param name="ageRestricted">Age Restricted</param>
	/// <param name="gated">Gated</param>
	/// <param name="golf">Golf</param>
	/// <param name="green">Green</param>
	/// <param name="nature">Has Nature areas</param>
	/// <param name="parks">Parks</param>
	/// <param name="pool">Pool</param>
	/// <param name="sports">Sports</param>
	/// <param name="views">Views</param>
	/// <param name="waterfront">Water front</param>
	/// <param name="hasCommCenter">Has Community Center</param>
	/// <param name="hasEvent">Has Event </param>
	/// <param name="hasConPromo">Has Consumer Promo</param>
	/// <param name="hasAgePromo">Has Agent Promotion</param>
	/// <param name="hasHotDeals">Has Hot Home</param>
	/// <param name="hasLuxuryHomes">Has Hot Home</param>
	/// <param name="hasViewOnly">Has View Only</param>
	/// <param name="hasBuilderReview">Has Builder Review</param>
	/// <param name="isHotHome">Is Hot Home</param>
	/// <param name="isMaster">Is Master </param>
	/// <param name="isUrban">Is Urban</param>
	/// <param name="isCondoType">Is Condo Type</param>
	/// <param name="isMultiFamily">Whether the community has at least one multi family home, or when the home type is MF at home level</param>
	/// <param name="isSingleFamily">Whether the community has at least one single family home, or the home is SF at home level</param>
	/// <param name="isTownHome">Is Town Home</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("searchImages")]
	public GraphQLQuery<GraphQLsearchAllImages>SearchImages (string sessionToken, int? partnerId, string imgCat = null, string imgColorType = null, int? imgId = null, string imgIds = null, string imgType = null, int? minImgHeight = null, int? minImgWidth = null, bool? alphaResults = null, bool? aPromo = null, bool? basicListingToTheEnd = null, int? bath = null, string bcType = null, int? bed = null, int? brandId = null, int?[] brandIds = null, int? builderId = null, string builderIds = null, string cities = null, string city = null, int? commId = null, int? parentCommId = null, string commIds = null, string commStatus = null, string communityName = null, string counties = null, string county = null, bool? cPromo = null, bool? custom = null, bool? customBuilderLocations = null, bool? customResults = null, float? dist = null, string districtIds = null, bool? excludeBasiCommunities = null, bool? excludeRegularComms = null, bool? excludeCommunitiesByBuilderId = null, int? first = null, int? garage = null, GraphQLGeoPolygonInputType[] geoPolygon = null, int? halfBath = null, string homeStatus = null, bool? includeMpcs = null, bool? includeBasicMpc = null, int?[] listingIds = null, int?[] imageIds = null, int? livAreas = null, int? marketId = null, string marketIds = null, int? masterBrLoc = null, float? maxLat = null, float? maxLng = null, bool? mfr = null, float? minLat = null, float? minLng = null, DateTime? moveInDate = null, bool? noBoyl = null, float? numStory = null, int? offset = null, float? originLat = null, float? originLng = null, int? page = null, int? pageSize = null, int? planId = null, string planIds = null, string planName = null, string postalCode = null, string postalCodes = null, float? prHi = null, float? prLo = null, string projectTypeCode = null, bool? promo = null, bool? qmi = null, float? radius = null, string radiusUnit = null, int? sfhigh = null, int? sflow = null, string soldDataToken = null, DateTime? soldFromDate = null, GraphQLSortValueInputType[] sort = null, string sortFirstBy = null, int? specId = null, string specIds = null, GraphQLSpecTypeInputType[] specTypes = null, string state = null, float? story = null, bool? adult = null, bool? ageRestricted = null, bool? gated = null, bool? golf = null, bool? green = null, bool? nature = null, bool? parks = null, bool? pool = null, bool? sports = null, bool? views = null, bool? waterfront = null, bool? hasCommCenter = null, bool? hasEvent = null, bool? hasConPromo = null, bool? hasAgePromo = null, bool? hasHotDeals = null, bool? hasLuxuryHomes = null, bool? hasViewOnly = null, bool? hasBuilderReview = null, bool? isHotHome = null, bool? isMaster = null, bool? isUrban = null, bool? isCondoType = null, bool? isMultiFamily = null, bool? isSingleFamily = null, bool? isTownHome = null, Func<GraphQLsearchAllImages, GraphQLsearchAllImages> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return new GraphQLQuery<GraphQLsearchAllImages>(this._graphQlHttpClient, true, currentContext, fields, sessionToken, partnerId, imgCat, imgColorType, imgId, imgIds, imgType, minImgHeight, minImgWidth, alphaResults, aPromo, basicListingToTheEnd, bath, bcType, bed, brandId, brandIds, builderId, builderIds, cities, city, commId, parentCommId, commIds, commStatus, communityName, counties, county, cPromo, custom, customBuilderLocations, customResults, dist, districtIds, excludeBasiCommunities, excludeRegularComms, excludeCommunitiesByBuilderId, first, garage, geoPolygon, halfBath, homeStatus, includeMpcs, includeBasicMpc, listingIds, imageIds, livAreas, marketId, marketIds, masterBrLoc, maxLat, maxLng, mfr, minLat, minLng, moveInDate, noBoyl, numStory, offset, originLat, originLng, page, pageSize, planId, planIds, planName, postalCode, postalCodes, prHi, prLo, projectTypeCode, promo, qmi, radius, radiusUnit, sfhigh, sflow, soldDataToken, soldFromDate, sort, sortFirstBy, specId, specIds, specTypes, state, story, adult, ageRestricted, gated, golf, green, nature, parks, pool, sports, views, waterfront, hasCommCenter, hasEvent, hasConPromo, hasAgePromo, hasHotDeals, hasLuxuryHomes, hasViewOnly, hasBuilderReview, isHotHome, isMaster, isUrban, isCondoType, isMultiFamily, isSingleFamily, isTownHome);
	}

	/// <param name="partnerId">Partner Id</param>
	/// <param name="imgCat">Image Category</param>
	/// <param name="imgColorType">Image Color Type</param>
	/// <param name="imgId">Image Id</param>
	/// <param name="imgIds">Image Ids</param>
	/// <param name="imgType">Image Type Code</param>
	/// <param name="minImgHeight">Image Height</param>
	/// <param name="minImgWidth">Image Width</param>
	/// <param name="alphaResults">Alpha Results</param>
	/// <param name="aPromo">Has Agent Promo</param>
	/// <param name="basicListingToTheEnd">Basic Listing to End</param>
	/// <param name="bath">Bath</param>
	/// <param name="bcType">Community Type (C: Coming soon; G: Gated; N: Normal; X: Closed)</param>
	/// <param name="bed">Bed</param>
	/// <param name="brandId">Brand Id</param>
	/// <param name="brandIds">Brands</param>
	/// <param name="builderId">Builder Id</param>
	/// <param name="builderIds">Builder Ids</param>
	/// <param name="cities">Cities</param>
	/// <param name="city">City</param>
	/// <param name="commId">Community Id</param>
	/// <param name="parentCommId">Community Id</param>
	/// <param name="commIds">Community Ids</param>
	/// <param name="commStatus">Community Status (N: Active; C: Coming Soon; G: Grand Opening; X: Close Out; CG: ComingSoonGrandOpening)</param>
	/// <param name="communityName">Community Name</param>
	/// <param name="counties">Counties</param>
	/// <param name="county">County</param>
	/// <param name="cPromo">Has Consumer Promo</param>
	/// <param name="custom">Custom</param>
	/// <param name="customBuilderLocations">Custom Builder Locations</param>
	/// <param name="customResults">Custom Results</param>
	/// <param name="dist">Distance</param>
	/// <param name="districtIds">District Ids</param>
	/// <param name="excludeBasiCommunities">Exclude Basic Communities</param>
	/// <param name="excludeRegularComms">Exclude Basic Communities</param>
	/// <param name="excludeCommunitiesByBuilderId">Exclude Communities by Builder Id</param>
	/// <param name="first">First</param>
	/// <param name="garage">Garage</param>
	/// <param name="geoPolygon">Array or coordinates to apply polygon/draw on map search</param>
	/// <param name="halfBath">Half Bath</param>
	/// <param name="homeStatus">Home Status (R: Ready to Build; UC: Under Construction; A: Available Now; M: Model; S: Sold)</param>
	/// <param name="includeMpcs">Include Master Plan Communities</param>
	/// <param name="includeBasicMpc">Include Basic Master Plan Community</param>
	/// <param name="listingIds">LivingIds</param>
	/// <param name="imageIds">LivingIds</param>
	/// <param name="livAreas">Living Areas</param>
	/// <param name="marketId">Market Id</param>
	/// <param name="marketIds">Market Ids</param>
	/// <param name="masterBrLoc">Master Bed Location</param>
	/// <param name="maxLat">Max Latitude</param>
	/// <param name="maxLng">Max Longitude</param>
	/// <param name="mfr">Manufactured</param>
	/// <param name="minLat">Min Latitude</param>
	/// <param name="minLng">Min Longitude</param>
	/// <param name="moveInDate">Move in date</param>
	/// <param name="noBoyl">No Boyl</param>
	/// <param name="numStory">Num Story</param>
	/// <param name="offset">Skip first N records</param>
	/// <param name="originLat">Origin Latitude</param>
	/// <param name="originLng">Origin Longitude</param>
	/// <param name="page">Page</param>
	/// <param name="pageSize">Page Size</param>
	/// <param name="planId">Plan Id</param>
	/// <param name="planIds">Plan Ids</param>
	/// <param name="planName">Plan name</param>
	/// <param name="postalCode">Postal Code</param>
	/// <param name="postalCodes">Postal Codes</param>
	/// <param name="prHi">Higher Price</param>
	/// <param name="prLo">Minimum Price</param>
	/// <param name="projectTypeCode">Project Type: (BYL: Boyl; CST: Custom; MFR: Manufactured)</param>
	/// <param name="promo">Has Promo</param>
	/// <param name="qmi">Quick Move In</param>
	/// <param name="radius">Radius</param>
	/// <param name="radiusUnit">Raius Unit</param>
	/// <param name="sfhigh">Maximum Square Feet</param>
	/// <param name="sflow">Minimum Square Feet</param>
	/// <param name="soldDataToken">If provided and it's valid, it will return Sold Data info, Otherwise will filter out sold data records even if HomeStatus: S is specified</param>
	/// <param name="soldFromDate">Sold From Date</param>
	/// <param name="sort">List of fields to sort</param>
	/// <param name="sortFirstBy">Sort First By</param>
	/// <param name="specId">Spec Id</param>
	/// <param name="specIds">Spec Ids</param>
	/// <param name="specTypes">Spec Type</param>
	/// <param name="state">State</param>
	/// <param name="story">Story</param>
	/// <param name="adult">Is for Adult</param>
	/// <param name="ageRestricted">Age Restricted</param>
	/// <param name="gated">Gated</param>
	/// <param name="golf">Golf</param>
	/// <param name="green">Green</param>
	/// <param name="nature">Has Nature areas</param>
	/// <param name="parks">Parks</param>
	/// <param name="pool">Pool</param>
	/// <param name="sports">Sports</param>
	/// <param name="views">Views</param>
	/// <param name="waterfront">Water front</param>
	/// <param name="hasCommCenter">Has Community Center</param>
	/// <param name="hasEvent">Has Event </param>
	/// <param name="hasConPromo">Has Consumer Promo</param>
	/// <param name="hasAgePromo">Has Agent Promotion</param>
	/// <param name="hasHotDeals">Has Hot Home</param>
	/// <param name="hasLuxuryHomes">Has Hot Home</param>
	/// <param name="hasViewOnly">Has View Only</param>
	/// <param name="hasBuilderReview">Has Builder Review</param>
	/// <param name="isHotHome">Is Hot Home</param>
	/// <param name="isMaster">Is Master </param>
	/// <param name="isUrban">Is Urban</param>
	/// <param name="isCondoType">Is Condo Type</param>
	/// <param name="isMultiFamily">Whether the community has at least one multi family home, or when the home type is MF at home level</param>
	/// <param name="isSingleFamily">Whether the community has at least one single family home, or the home is SF at home level</param>
	/// <param name="isTownHome">Is Town Home</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("searchImages")]
	public Task<List<GraphQLsearchAllImages>>SearchImagesAsync (string sessionToken, int? partnerId, string imgCat = null, string imgColorType = null, int? imgId = null, string imgIds = null, string imgType = null, int? minImgHeight = null, int? minImgWidth = null, bool? alphaResults = null, bool? aPromo = null, bool? basicListingToTheEnd = null, int? bath = null, string bcType = null, int? bed = null, int? brandId = null, int?[] brandIds = null, int? builderId = null, string builderIds = null, string cities = null, string city = null, int? commId = null, int? parentCommId = null, string commIds = null, string commStatus = null, string communityName = null, string counties = null, string county = null, bool? cPromo = null, bool? custom = null, bool? customBuilderLocations = null, bool? customResults = null, float? dist = null, string districtIds = null, bool? excludeBasiCommunities = null, bool? excludeRegularComms = null, bool? excludeCommunitiesByBuilderId = null, int? first = null, int? garage = null, GraphQLGeoPolygonInputType[] geoPolygon = null, int? halfBath = null, string homeStatus = null, bool? includeMpcs = null, bool? includeBasicMpc = null, int?[] listingIds = null, int?[] imageIds = null, int? livAreas = null, int? marketId = null, string marketIds = null, int? masterBrLoc = null, float? maxLat = null, float? maxLng = null, bool? mfr = null, float? minLat = null, float? minLng = null, DateTime? moveInDate = null, bool? noBoyl = null, float? numStory = null, int? offset = null, float? originLat = null, float? originLng = null, int? page = null, int? pageSize = null, int? planId = null, string planIds = null, string planName = null, string postalCode = null, string postalCodes = null, float? prHi = null, float? prLo = null, string projectTypeCode = null, bool? promo = null, bool? qmi = null, float? radius = null, string radiusUnit = null, int? sfhigh = null, int? sflow = null, string soldDataToken = null, DateTime? soldFromDate = null, GraphQLSortValueInputType[] sort = null, string sortFirstBy = null, int? specId = null, string specIds = null, GraphQLSpecTypeInputType[] specTypes = null, string state = null, float? story = null, bool? adult = null, bool? ageRestricted = null, bool? gated = null, bool? golf = null, bool? green = null, bool? nature = null, bool? parks = null, bool? pool = null, bool? sports = null, bool? views = null, bool? waterfront = null, bool? hasCommCenter = null, bool? hasEvent = null, bool? hasConPromo = null, bool? hasAgePromo = null, bool? hasHotDeals = null, bool? hasLuxuryHomes = null, bool? hasViewOnly = null, bool? hasBuilderReview = null, bool? isHotHome = null, bool? isMaster = null, bool? isUrban = null, bool? isCondoType = null, bool? isMultiFamily = null, bool? isSingleFamily = null, bool? isTownHome = null, Func<GraphQLsearchAllImages, GraphQLsearchAllImages> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return Task.Run(() => new GraphQLQuery<GraphQLsearchAllImages>(this._graphQlHttpClient, true, currentContext, fields, sessionToken, partnerId, imgCat, imgColorType, imgId, imgIds, imgType, minImgHeight, minImgWidth, alphaResults, aPromo, basicListingToTheEnd, bath, bcType, bed, brandId, brandIds, builderId, builderIds, cities, city, commId, parentCommId, commIds, commStatus, communityName, counties, county, cPromo, custom, customBuilderLocations, customResults, dist, districtIds, excludeBasiCommunities, excludeRegularComms, excludeCommunitiesByBuilderId, first, garage, geoPolygon, halfBath, homeStatus, includeMpcs, includeBasicMpc, listingIds, imageIds, livAreas, marketId, marketIds, masterBrLoc, maxLat, maxLng, mfr, minLat, minLng, moveInDate, noBoyl, numStory, offset, originLat, originLng, page, pageSize, planId, planIds, planName, postalCode, postalCodes, prHi, prLo, projectTypeCode, promo, qmi, radius, radiusUnit, sfhigh, sflow, soldDataToken, soldFromDate, sort, sortFirstBy, specId, specIds, specTypes, state, story, adult, ageRestricted, gated, golf, green, nature, parks, pool, sports, views, waterfront, hasCommCenter, hasEvent, hasConPromo, hasAgePromo, hasHotDeals, hasLuxuryHomes, hasViewOnly, hasBuilderReview, isHotHome, isMaster, isUrban, isCondoType, isMultiFamily, isSingleFamily, isTownHome).ToList());
	}

	/// <param name="imgCat">Image Category</param>
	/// <param name="imgColorType">Image Color Type</param>
	/// <param name="imgId">Image Id</param>
	/// <param name="imgIds">Image Ids</param>
	/// <param name="imgType">Image Type Code</param>
	/// <param name="minImgHeight">Image Height</param>
	/// <param name="minImgWidth">Image Width</param>
	/// <param name="alphaResults">Alpha Results</param>
	/// <param name="aPromo">Has Agent Promo</param>
	/// <param name="basicListingToTheEnd">Basic Listing to End</param>
	/// <param name="bath">Bath</param>
	/// <param name="bcType">Community Type (C: Coming soon; G: Gated; N: Normal; X: Closed)</param>
	/// <param name="bed">Bed</param>
	/// <param name="brandId">Brand Id</param>
	/// <param name="brandIds">Brands</param>
	/// <param name="builderId">Builder Id</param>
	/// <param name="builderIds">Builder Ids</param>
	/// <param name="cities">Cities</param>
	/// <param name="city">City</param>
	/// <param name="commId">Community Id</param>
	/// <param name="parentCommId">Community Id</param>
	/// <param name="commIds">Community Ids</param>
	/// <param name="commStatus">Community Status (N: Active; C: Coming Soon; G: Grand Opening; X: Close Out; CG: ComingSoonGrandOpening)</param>
	/// <param name="communityName">Community Name</param>
	/// <param name="counties">Counties</param>
	/// <param name="county">County</param>
	/// <param name="cPromo">Has Consumer Promo</param>
	/// <param name="custom">Custom</param>
	/// <param name="customBuilderLocations">Custom Builder Locations</param>
	/// <param name="customResults">Custom Results</param>
	/// <param name="dist">Distance</param>
	/// <param name="districtIds">District Ids</param>
	/// <param name="excludeBasiCommunities">Exclude Basic Communities</param>
	/// <param name="excludeRegularComms">Exclude Basic Communities</param>
	/// <param name="excludeCommunitiesByBuilderId">Exclude Communities by Builder Id</param>
	/// <param name="first">First</param>
	/// <param name="garage">Garage</param>
	/// <param name="geoPolygon">Array or coordinates to apply polygon/draw on map search</param>
	/// <param name="halfBath">Half Bath</param>
	/// <param name="homeStatus">Home Status (R: Ready to Build; UC: Under Construction; A: Available Now; M: Model; S: Sold)</param>
	/// <param name="includeMpcs">Include Master Plan Communities</param>
	/// <param name="includeBasicMpc">Include Basic Master Plan Community</param>
	/// <param name="listingIds">LivingIds</param>
	/// <param name="imageIds">LivingIds</param>
	/// <param name="livAreas">Living Areas</param>
	/// <param name="marketId">Market Id</param>
	/// <param name="marketIds">Market Ids</param>
	/// <param name="masterBrLoc">Master Bed Location</param>
	/// <param name="maxLat">Max Latitude</param>
	/// <param name="maxLng">Max Longitude</param>
	/// <param name="mfr">Manufactured</param>
	/// <param name="minLat">Min Latitude</param>
	/// <param name="minLng">Min Longitude</param>
	/// <param name="moveInDate">Move in date</param>
	/// <param name="noBoyl">No Boyl</param>
	/// <param name="numStory">Num Story</param>
	/// <param name="offset">Skip first N records</param>
	/// <param name="originLat">Origin Latitude</param>
	/// <param name="originLng">Origin Longitude</param>
	/// <param name="page">Page</param>
	/// <param name="pageSize">Page Size</param>
	/// <param name="partnerId">Partner Id</param>
	/// <param name="planId">Plan Id</param>
	/// <param name="planIds">Plan Ids</param>
	/// <param name="planName">Plan name</param>
	/// <param name="postalCode">Postal Code</param>
	/// <param name="postalCodes">Postal Codes</param>
	/// <param name="prHi">Higher Price</param>
	/// <param name="prLo">Minimum Price</param>
	/// <param name="projectTypeCode">Project Type: (BYL: Boyl; CST: Custom; MFR: Manufactured)</param>
	/// <param name="promo">Has Promo</param>
	/// <param name="qmi">Quick Move In</param>
	/// <param name="radius">Radius</param>
	/// <param name="radiusUnit">Raius Unit</param>
	/// <param name="sfhigh">Maximum Square Feet</param>
	/// <param name="sflow">Minimum Square Feet</param>
	/// <param name="soldDataToken">If provided and it's valid, it will return Sold Data info, Otherwise will filter out sold data records even if HomeStatus: S is specified</param>
	/// <param name="soldFromDate">Sold From Date</param>
	/// <param name="sort">List of fields to sort</param>
	/// <param name="sortFirstBy">Sort First By</param>
	/// <param name="specId">Spec Id</param>
	/// <param name="specIds">Spec Ids</param>
	/// <param name="specTypes">Spec Type</param>
	/// <param name="state">State</param>
	/// <param name="story">Story</param>
	/// <param name="adult">Is for Adult</param>
	/// <param name="ageRestricted">Age Restricted</param>
	/// <param name="gated">Gated</param>
	/// <param name="golf">Golf</param>
	/// <param name="green">Green</param>
	/// <param name="nature">Has Nature areas</param>
	/// <param name="parks">Parks</param>
	/// <param name="pool">Pool</param>
	/// <param name="sports">Sports</param>
	/// <param name="views">Views</param>
	/// <param name="waterfront">Water front</param>
	/// <param name="hasCommCenter">Has Community Center</param>
	/// <param name="hasEvent">Has Event </param>
	/// <param name="hasConPromo">Has Consumer Promo</param>
	/// <param name="hasAgePromo">Has Agent Promotion</param>
	/// <param name="hasHotDeals">Has Hot Home</param>
	/// <param name="hasLuxuryHomes">Has Hot Home</param>
	/// <param name="hasViewOnly">Has View Only</param>
	/// <param name="hasBuilderReview">Has Builder Review</param>
	/// <param name="isHotHome">Is Hot Home</param>
	/// <param name="isMaster">Is Master </param>
	/// <param name="isUrban">Is Urban</param>
	/// <param name="isCondoType">Is Condo Type</param>
	/// <param name="isMultiFamily">Whether the community has at least one multi family home, or when the home type is MF at home level</param>
	/// <param name="isSingleFamily">Whether the community has at least one single family home, or the home is SF at home level</param>
	/// <param name="isTownHome">Is Town Home</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("searchImagesCounts")]
	public GraphQLQuery<GraphQLResultHomeCounts>SearchImagesCounts (string sessionToken, int? partnerId, string imgCat = null, string imgColorType = null, int? imgId = null, string imgIds = null, string imgType = null, int? minImgHeight = null, int? minImgWidth = null, bool? alphaResults = null, bool? aPromo = null, bool? basicListingToTheEnd = null, int? bath = null, string bcType = null, int? bed = null, int? brandId = null, int?[] brandIds = null, int? builderId = null, string builderIds = null, string cities = null, string city = null, int? commId = null, int? parentCommId = null, string commIds = null, string commStatus = null, string communityName = null, string counties = null, string county = null, bool? cPromo = null, bool? custom = null, bool? customBuilderLocations = null, bool? customResults = null, float? dist = null, string districtIds = null, bool? excludeBasiCommunities = null, bool? excludeRegularComms = null, bool? excludeCommunitiesByBuilderId = null, int? first = null, int? garage = null, GraphQLGeoPolygonInputType[] geoPolygon = null, int? halfBath = null, string homeStatus = null, bool? includeMpcs = null, bool? includeBasicMpc = null, int?[] listingIds = null, int?[] imageIds = null, int? livAreas = null, int? marketId = null, string marketIds = null, int? masterBrLoc = null, float? maxLat = null, float? maxLng = null, bool? mfr = null, float? minLat = null, float? minLng = null, DateTime? moveInDate = null, bool? noBoyl = null, float? numStory = null, int? offset = null, float? originLat = null, float? originLng = null, int? page = null, int? pageSize = null, int? planId = null, string planIds = null, string planName = null, string postalCode = null, string postalCodes = null, float? prHi = null, float? prLo = null, string projectTypeCode = null, bool? promo = null, bool? qmi = null, float? radius = null, string radiusUnit = null, int? sfhigh = null, int? sflow = null, string soldDataToken = null, DateTime? soldFromDate = null, GraphQLSortValueInputType[] sort = null, string sortFirstBy = null, int? specId = null, string specIds = null, GraphQLSpecTypeInputType[] specTypes = null, string state = null, float? story = null, bool? adult = null, bool? ageRestricted = null, bool? gated = null, bool? golf = null, bool? green = null, bool? nature = null, bool? parks = null, bool? pool = null, bool? sports = null, bool? views = null, bool? waterfront = null, bool? hasCommCenter = null, bool? hasEvent = null, bool? hasConPromo = null, bool? hasAgePromo = null, bool? hasHotDeals = null, bool? hasLuxuryHomes = null, bool? hasViewOnly = null, bool? hasBuilderReview = null, bool? isHotHome = null, bool? isMaster = null, bool? isUrban = null, bool? isCondoType = null, bool? isMultiFamily = null, bool? isSingleFamily = null, bool? isTownHome = null, Func<GraphQLResultHomeCounts, GraphQLResultHomeCounts> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return new GraphQLQuery<GraphQLResultHomeCounts>(this._graphQlHttpClient, false, currentContext, fields, sessionToken, partnerId, imgCat, imgColorType, imgId, imgIds, imgType, minImgHeight, minImgWidth, alphaResults, aPromo, basicListingToTheEnd, bath, bcType, bed, brandId, brandIds, builderId, builderIds, cities, city, commId, parentCommId, commIds, commStatus, communityName, counties, county, cPromo, custom, customBuilderLocations, customResults, dist, districtIds, excludeBasiCommunities, excludeRegularComms, excludeCommunitiesByBuilderId, first, garage, geoPolygon, halfBath, homeStatus, includeMpcs, includeBasicMpc, listingIds, imageIds, livAreas, marketId, marketIds, masterBrLoc, maxLat, maxLng, mfr, minLat, minLng, moveInDate, noBoyl, numStory, offset, originLat, originLng, page, pageSize, planId, planIds, planName, postalCode, postalCodes, prHi, prLo, projectTypeCode, promo, qmi, radius, radiusUnit, sfhigh, sflow, soldDataToken, soldFromDate, sort, sortFirstBy, specId, specIds, specTypes, state, story, adult, ageRestricted, gated, golf, green, nature, parks, pool, sports, views, waterfront, hasCommCenter, hasEvent, hasConPromo, hasAgePromo, hasHotDeals, hasLuxuryHomes, hasViewOnly, hasBuilderReview, isHotHome, isMaster, isUrban, isCondoType, isMultiFamily, isSingleFamily, isTownHome);
	}

	/// <param name="partnerId">Partner Id</param>
	/// <param name="imgCat">Image Category</param>
	/// <param name="imgColorType">Image Color Type</param>
	/// <param name="imgId">Image Id</param>
	/// <param name="imgIds">Image Ids</param>
	/// <param name="imgType">Image Type Code</param>
	/// <param name="minImgHeight">Image Height</param>
	/// <param name="minImgWidth">Image Width</param>
	/// <param name="alphaResults">Alpha Results</param>
	/// <param name="aPromo">Has Agent Promo</param>
	/// <param name="basicListingToTheEnd">Basic Listing to End</param>
	/// <param name="bath">Bath</param>
	/// <param name="bcType">Community Type (C: Coming soon; G: Gated; N: Normal; X: Closed)</param>
	/// <param name="bed">Bed</param>
	/// <param name="brandId">Brand Id</param>
	/// <param name="brandIds">Brands</param>
	/// <param name="builderId">Builder Id</param>
	/// <param name="builderIds">Builder Ids</param>
	/// <param name="cities">Cities</param>
	/// <param name="city">City</param>
	/// <param name="commId">Community Id</param>
	/// <param name="parentCommId">Community Id</param>
	/// <param name="commIds">Community Ids</param>
	/// <param name="commStatus">Community Status (N: Active; C: Coming Soon; G: Grand Opening; X: Close Out; CG: ComingSoonGrandOpening)</param>
	/// <param name="communityName">Community Name</param>
	/// <param name="counties">Counties</param>
	/// <param name="county">County</param>
	/// <param name="cPromo">Has Consumer Promo</param>
	/// <param name="custom">Custom</param>
	/// <param name="customBuilderLocations">Custom Builder Locations</param>
	/// <param name="customResults">Custom Results</param>
	/// <param name="dist">Distance</param>
	/// <param name="districtIds">District Ids</param>
	/// <param name="excludeBasiCommunities">Exclude Basic Communities</param>
	/// <param name="excludeRegularComms">Exclude Basic Communities</param>
	/// <param name="excludeCommunitiesByBuilderId">Exclude Communities by Builder Id</param>
	/// <param name="first">First</param>
	/// <param name="garage">Garage</param>
	/// <param name="geoPolygon">Array or coordinates to apply polygon/draw on map search</param>
	/// <param name="halfBath">Half Bath</param>
	/// <param name="homeStatus">Home Status (R: Ready to Build; UC: Under Construction; A: Available Now; M: Model; S: Sold)</param>
	/// <param name="includeMpcs">Include Master Plan Communities</param>
	/// <param name="includeBasicMpc">Include Basic Master Plan Community</param>
	/// <param name="listingIds">LivingIds</param>
	/// <param name="imageIds">LivingIds</param>
	/// <param name="livAreas">Living Areas</param>
	/// <param name="marketId">Market Id</param>
	/// <param name="marketIds">Market Ids</param>
	/// <param name="masterBrLoc">Master Bed Location</param>
	/// <param name="maxLat">Max Latitude</param>
	/// <param name="maxLng">Max Longitude</param>
	/// <param name="mfr">Manufactured</param>
	/// <param name="minLat">Min Latitude</param>
	/// <param name="minLng">Min Longitude</param>
	/// <param name="moveInDate">Move in date</param>
	/// <param name="noBoyl">No Boyl</param>
	/// <param name="numStory">Num Story</param>
	/// <param name="offset">Skip first N records</param>
	/// <param name="originLat">Origin Latitude</param>
	/// <param name="originLng">Origin Longitude</param>
	/// <param name="page">Page</param>
	/// <param name="pageSize">Page Size</param>
	/// <param name="planId">Plan Id</param>
	/// <param name="planIds">Plan Ids</param>
	/// <param name="planName">Plan name</param>
	/// <param name="postalCode">Postal Code</param>
	/// <param name="postalCodes">Postal Codes</param>
	/// <param name="prHi">Higher Price</param>
	/// <param name="prLo">Minimum Price</param>
	/// <param name="projectTypeCode">Project Type: (BYL: Boyl; CST: Custom; MFR: Manufactured)</param>
	/// <param name="promo">Has Promo</param>
	/// <param name="qmi">Quick Move In</param>
	/// <param name="radius">Radius</param>
	/// <param name="radiusUnit">Raius Unit</param>
	/// <param name="sfhigh">Maximum Square Feet</param>
	/// <param name="sflow">Minimum Square Feet</param>
	/// <param name="soldDataToken">If provided and it's valid, it will return Sold Data info, Otherwise will filter out sold data records even if HomeStatus: S is specified</param>
	/// <param name="soldFromDate">Sold From Date</param>
	/// <param name="sort">List of fields to sort</param>
	/// <param name="sortFirstBy">Sort First By</param>
	/// <param name="specId">Spec Id</param>
	/// <param name="specIds">Spec Ids</param>
	/// <param name="specTypes">Spec Type</param>
	/// <param name="state">State</param>
	/// <param name="story">Story</param>
	/// <param name="adult">Is for Adult</param>
	/// <param name="ageRestricted">Age Restricted</param>
	/// <param name="gated">Gated</param>
	/// <param name="golf">Golf</param>
	/// <param name="green">Green</param>
	/// <param name="nature">Has Nature areas</param>
	/// <param name="parks">Parks</param>
	/// <param name="pool">Pool</param>
	/// <param name="sports">Sports</param>
	/// <param name="views">Views</param>
	/// <param name="waterfront">Water front</param>
	/// <param name="hasCommCenter">Has Community Center</param>
	/// <param name="hasEvent">Has Event </param>
	/// <param name="hasConPromo">Has Consumer Promo</param>
	/// <param name="hasAgePromo">Has Agent Promotion</param>
	/// <param name="hasHotDeals">Has Hot Home</param>
	/// <param name="hasLuxuryHomes">Has Hot Home</param>
	/// <param name="hasViewOnly">Has View Only</param>
	/// <param name="hasBuilderReview">Has Builder Review</param>
	/// <param name="isHotHome">Is Hot Home</param>
	/// <param name="isMaster">Is Master </param>
	/// <param name="isUrban">Is Urban</param>
	/// <param name="isCondoType">Is Condo Type</param>
	/// <param name="isMultiFamily">Whether the community has at least one multi family home, or when the home type is MF at home level</param>
	/// <param name="isSingleFamily">Whether the community has at least one single family home, or the home is SF at home level</param>
	/// <param name="isTownHome">Is Town Home</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("searchImagesCounts")]
	public Task<List<GraphQLResultHomeCounts>>SearchImagesCountsAsync (string sessionToken, int? partnerId, string imgCat = null, string imgColorType = null, int? imgId = null, string imgIds = null, string imgType = null, int? minImgHeight = null, int? minImgWidth = null, bool? alphaResults = null, bool? aPromo = null, bool? basicListingToTheEnd = null, int? bath = null, string bcType = null, int? bed = null, int? brandId = null, int?[] brandIds = null, int? builderId = null, string builderIds = null, string cities = null, string city = null, int? commId = null, int? parentCommId = null, string commIds = null, string commStatus = null, string communityName = null, string counties = null, string county = null, bool? cPromo = null, bool? custom = null, bool? customBuilderLocations = null, bool? customResults = null, float? dist = null, string districtIds = null, bool? excludeBasiCommunities = null, bool? excludeRegularComms = null, bool? excludeCommunitiesByBuilderId = null, int? first = null, int? garage = null, GraphQLGeoPolygonInputType[] geoPolygon = null, int? halfBath = null, string homeStatus = null, bool? includeMpcs = null, bool? includeBasicMpc = null, int?[] listingIds = null, int?[] imageIds = null, int? livAreas = null, int? marketId = null, string marketIds = null, int? masterBrLoc = null, float? maxLat = null, float? maxLng = null, bool? mfr = null, float? minLat = null, float? minLng = null, DateTime? moveInDate = null, bool? noBoyl = null, float? numStory = null, int? offset = null, float? originLat = null, float? originLng = null, int? page = null, int? pageSize = null, int? planId = null, string planIds = null, string planName = null, string postalCode = null, string postalCodes = null, float? prHi = null, float? prLo = null, string projectTypeCode = null, bool? promo = null, bool? qmi = null, float? radius = null, string radiusUnit = null, int? sfhigh = null, int? sflow = null, string soldDataToken = null, DateTime? soldFromDate = null, GraphQLSortValueInputType[] sort = null, string sortFirstBy = null, int? specId = null, string specIds = null, GraphQLSpecTypeInputType[] specTypes = null, string state = null, float? story = null, bool? adult = null, bool? ageRestricted = null, bool? gated = null, bool? golf = null, bool? green = null, bool? nature = null, bool? parks = null, bool? pool = null, bool? sports = null, bool? views = null, bool? waterfront = null, bool? hasCommCenter = null, bool? hasEvent = null, bool? hasConPromo = null, bool? hasAgePromo = null, bool? hasHotDeals = null, bool? hasLuxuryHomes = null, bool? hasViewOnly = null, bool? hasBuilderReview = null, bool? isHotHome = null, bool? isMaster = null, bool? isUrban = null, bool? isCondoType = null, bool? isMultiFamily = null, bool? isSingleFamily = null, bool? isTownHome = null, Func<GraphQLResultHomeCounts, GraphQLResultHomeCounts> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return Task.Run(() => new GraphQLQuery<GraphQLResultHomeCounts>(this._graphQlHttpClient, false, currentContext, fields, sessionToken, partnerId, imgCat, imgColorType, imgId, imgIds, imgType, minImgHeight, minImgWidth, alphaResults, aPromo, basicListingToTheEnd, bath, bcType, bed, brandId, brandIds, builderId, builderIds, cities, city, commId, parentCommId, commIds, commStatus, communityName, counties, county, cPromo, custom, customBuilderLocations, customResults, dist, districtIds, excludeBasiCommunities, excludeRegularComms, excludeCommunitiesByBuilderId, first, garage, geoPolygon, halfBath, homeStatus, includeMpcs, includeBasicMpc, listingIds, imageIds, livAreas, marketId, marketIds, masterBrLoc, maxLat, maxLng, mfr, minLat, minLng, moveInDate, noBoyl, numStory, offset, originLat, originLng, page, pageSize, planId, planIds, planName, postalCode, postalCodes, prHi, prLo, projectTypeCode, promo, qmi, radius, radiusUnit, sfhigh, sflow, soldDataToken, soldFromDate, sort, sortFirstBy, specId, specIds, specTypes, state, story, adult, ageRestricted, gated, golf, green, nature, parks, pool, sports, views, waterfront, hasCommCenter, hasEvent, hasConPromo, hasAgePromo, hasHotDeals, hasLuxuryHomes, hasViewOnly, hasBuilderReview, isHotHome, isMaster, isUrban, isCondoType, isMultiFamily, isSingleFamily, isTownHome).ToList());
	}

	/// <param name="stateAbbr">State Abbreviation</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("states")]
	public GraphQLQuery<GraphQLApiState>States (string sessionToken, string stateAbbr = null, Func<GraphQLApiState, GraphQLApiState> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return new GraphQLQuery<GraphQLApiState>(this._graphQlHttpClient, true, currentContext, fields, sessionToken, stateAbbr);
	}

	/// <param name="stateAbbr">State Abbreviation</param>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("states")]
	public Task<List<GraphQLApiState>>StatesAsync (string sessionToken, string stateAbbr = null, Func<GraphQLApiState, GraphQLApiState> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return Task.Run(() => new GraphQLQuery<GraphQLApiState>(this._graphQlHttpClient, true, currentContext, fields, sessionToken, stateAbbr).ToList());
	}

}
