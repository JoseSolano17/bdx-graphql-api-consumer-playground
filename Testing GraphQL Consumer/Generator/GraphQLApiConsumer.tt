﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Bdx.Common.GraphQl.Client;
using Bdx.Common.GraphQl.Client.Http;
using Bdx.Common.GraphQL.API.Consumer.Consumer;
using Newtonsoft.Json;
using Bdx.Common.GraphQL.API.Consumer.helpers;
using Bdx.Common.Utility;
using Bdx.Common.Utility.Enums;
using System.Threading.Tasks;
using System.Linq;
<#@ template debug="true" hostspecific="true" language="C#" #>
<#@ assembly name="System.Core" #>
<#@ import namespace="System.Linq" #>
<#@ import namespace="System.Text" #>
<#@ import namespace="System.Collections.Generic" #>
<#@ import namespace="System.IO" #>
<#@ import namespace="System" #>
<#@ import namespace="System.Net" #>
<#@ output extension=".cs" #>
<#@ include file="Introspection.tt" #>
<#
	string introspectionResult = HttpPostGraphQLQuery("http://sprint-api.newhomesource.com/api/v3/graphQL", "query IntrospectionQuery { __schema { queryType { name } mutationType { name } subscriptionType { name } types { ...FullType } directives { name description locations args { ...InputValue } } } } fragment FullType on __Type { kind name description fields(includeDeprecated: true) { name description args { ...InputValue } type { ...TypeRef } isDeprecated deprecationReason } inputFields { ...InputValue } interfaces { ...TypeRef } enumValues(includeDeprecated: true) { name description isDeprecated deprecationReason } possibleTypes { ...TypeRef }} fragment InputValue on __InputValue { name description type { ...TypeRef } defaultValue} fragment TypeRef on __Type { kind name ofType { kind name ofType { kind name ofType { kind name ofType { kind name ofType { kind name ofType { kind name ofType { kind name } } } } } } }}");

	var introspection = Introspection.FromJson(introspectionResult);

	foreach(var type in introspection.Data.Schema.Types.Where(t => t.Kind != Kind.Scalar && !t.Name.StartsWith("__") && t.Name != introspection.Data.Schema.QueryType.Name&& t.Name != introspection.Data.Schema.MutationType.Name))
	{
		if(type.Kind == Kind.Object)
		{
#>
public partial class GraphQL<#= type.Name #>
{
<# 
			foreach(var field in type.Fields)
			{
				if(!string.IsNullOrEmpty(field.Description))
				{
#>
	/// <summary><#= field.Description #></summary>
<#
				}
#>
	[GraphQLField("<#= field.Name #>")]
	public <#= ParseType(field.Type, false, true, false) #> <#= ToPascalCase(field.Name) #> { get; set; }

<#
			}
#>
}

<#
		}
		else if(type.Kind == Kind.InputObject)
		{
#>
public struct GraphQL<#= type.Name #>
{
<# 
			foreach(var field in type.InputFields)
			{
				if(!string.IsNullOrEmpty(field.Description))
				{
#>
	/// <summary><#= field.Description #></summary>
<#
				}
#>
	[JsonProperty(PropertyName = "<#= field.Name #>")<#= field.Type.Kind == Kind.Enum ? ", JsonConverter(typeof(GraphQLEnumConverter))" : string.Empty #>]
	public <#= ParseType(field.Type, false, false, false) #> <#= ToPascalCase(field.Name) #> { get; set; }

<#
			}
#>
}

<#
		}
		else if(type.Kind == Kind.Enum)
		{
#>
[JsonConverter(typeof(GraphQLEnumConverter))]
public enum GraphQL<#= type.Name #>
{
<# 
			for(int enumValueIndex = 0; enumValueIndex < type.EnumValues.Length; enumValueIndex++)
			{
#>
	/// <summary><#= type.EnumValues[enumValueIndex].Description ?? string.Empty #></summary>
	<#= type.EnumValues[enumValueIndex].Name #><#= enumValueIndex < type.EnumValues.Length - 1 ? "," : string.Empty #>
<#
			}
#>
}

<#
		}
	}
 #>
[Dependency(DependencyScope.Transient)]
public sealed class GraphQLApiConsumer : IGraphQLApiConsumer
{
	private readonly IGraphQLClient _graphQlHttpClient;

	public GraphQLApiConsumer()
    {
        _graphQlHttpClient = new GraphQLHttpClient();
    }

    public GraphQLApiConsumer(IGraphQLClient graphQlHttpClient)
    {
        _graphQlHttpClient = graphQlHttpClient;
    }

    /// <summary>Gets or sets URL of the GraphQL API, when it is assign a new HttpClient id created./</summary>
    /// <value>The API URL.</value>
    public string ApiUrl {
        get => this._graphQlHttpClient.EndPoint.ToString();
        set => this._graphQlHttpClient.EndPoint = new Uri(value) ;
    }

    private void EnsureGraphQLUrl()
    {
        if(_graphQlHttpClient?.EndPoint == null)
            throw new Exception("The Api URL was not set, please add GraphQL source");
    }
<#
	var queries = introspection.Data.Schema.Types.FirstOrDefault(t => t.Name == introspection.Data.Schema.QueryType.Name);
	var includeAsycVersion = true; 
	var maxNumberOfIterations = includeAsycVersion ? 2 : 1;

	if (queries != null)
	{
		foreach(var field in queries.Fields)
		{
			for(var i = 0; i < maxNumberOfIterations; i ++ ){

			if(!string.IsNullOrEmpty(field.Description))
			{
#>
	/// <summary><#= field.Description ?? string.Empty #></summary>
<#
			}
			foreach(var arg in field.Args)
			{
				if(!string.IsNullOrEmpty(arg.Description))
				{
#>
	/// <param name="<#= arg.Name #>"><#= arg.Description #></param>
<#
				}
			}
#>
	/// <param name="sessionToken">Token Used on production</param>
	[GraphQLField("<#= field.Name #>")]
	public <#= i != 0 ? "Task<List": "GraphQLQuery"#><<#= ParseType(field.Type, false, false, false) #>><#= i != 0 ? ">": ""#><#= ToPascalCase(field.Name) #><#= i != 0 ? "Async": ""#> (string sessionToken, <#
			field.Args = field.Args.OrderBy(f => f.Type.Kind == Kind.NonNull ? 0 : 1).ToArray();
			for(int argIndex = 0; argIndex < field.Args.Length; argIndex++)
			{
				#><#= ParseType(field.Args[argIndex].Type, true, false, field.Args[argIndex].Type.Kind != Kind.NonNull) #> <#= field.Args[argIndex].Name #><#= field.Args[argIndex].Type.Kind != Kind.NonNull ? " = " + (field.Args[argIndex].DefaultValue.Enum != null ? field.Args[argIndex].DefaultValue.Enum.ToString().ToLower() : field.Args[argIndex].DefaultValue.ToString()) : string.Empty  #><#= argIndex < field.Args.Length - 1 ?  ", " : string.Empty #><#
			}
#>, Func<<#= ParseType(field.Type, false, false, false) #>, <#= ParseType(field.Type, false, false, false) #>> fields = null ) 
	{
		EnsureGraphQLUrl();
		var currentContext = MethodBase.GetCurrentMethod();
		return <#= i != 0 ? "Task.Run(() => ": ""#>new GraphQLQuery<<#= ParseType(field.Type, false, false, false) #>>(this._graphQlHttpClient, <#= (field.Type.Kind == Kind.List).ToString().ToLower() #>, currentContext, fields, sessionToken, <#
			for(int argIndex = 0; argIndex < field.Args.Length; argIndex++)
			{
				#><#= field.Args[argIndex].Name #><#= argIndex < field.Args.Length - 1 ?  ", " : string.Empty #><#
			}
#>)<#= i != 0 ? ".ToList())": ""#>;
	}

<#
		}
      }
	}
#>
}
<#+
    public string ParseType(OfTypeClass type, bool isArgument, bool isField, bool allowsNulls)
    {
		string result = string.Empty;

		switch(type.Kind)
		{
			case Kind.Object:
			case Kind.Enum:
			case Kind.InputObject:
				result = "GraphQL"+type.Name;
			break;
			case Kind.List:
				result = isArgument ? ParseType(type.OfType, isArgument, isField, false) + "[]" : (isField ? "List<" + ParseType(type.OfType, isArgument, false, allowsNulls) + ">" : ParseType(type.OfType, isArgument, false, allowsNulls));
			break;
			case Kind.Scalar:
				switch(type.Name)
				{
					case "Boolean":
						result = "bool?";
						break;
					case "Date":
					case "DateTime":
					case "DateTimeOffset":
						result = "DateTime?";
						break;
					case "String":
						result = "string";	
						break;
					default:
						result = type.Name.ToLower()+"?";
						break;
				}
			break;
			case Kind.NonNull:
				result = ParseType(type.OfType, isArgument, isField, false);
			break;
		}

		if (allowsNulls && (new [] {"int", "float", "bool", "DateTime"}.Contains(result) || type.Kind == Kind.Enum))
			result += "?";

		return result; 
    }

	public string ToPascalCase (string inputText)
    {
		if(!string.IsNullOrEmpty(inputText))
			inputText = inputText.Substring(0, 1).ToUpper() + inputText.Substring(1);

		return inputText;
    }

    public static string HttpPostGraphQLQuery(string weburl, string graphQLQuery)
    {
        try
        {
            using (var client = new WebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = "text/plain";
                byte[] data = Encoding.Default.GetBytes(graphQLQuery);
                byte[] result = client.UploadData(weburl, "POST", data);
                return Encoding.Default.GetString(result);
            }
        }
        catch (Exception e)
        {
            throw new Exception("Error accessing url:" + weburl + " :json:" + graphQLQuery, e);
        }
    }
#>