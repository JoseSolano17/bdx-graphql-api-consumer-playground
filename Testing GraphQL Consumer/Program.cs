﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Testing_GraphQL_Consumer
{
    public class Program
    {
        public static async Task Main(string[] args)
        {

            var container = Services.Concrete.StructureMapBootStrapper.Configure();

            var _grapghQlConsumer = container.GetInstance<IGraphQLApiConsumer>();

            _grapghQlConsumer.ApiUrl = "http://sprint-api.newhomesource.com/api/v3/graphQL";

            var homesAsync = await _grapghQlConsumer.SearchAllHomesAsync(partnerId: 1, marketId: 269, sessionToken: "",
                fields: h => new GraphQLSearchAllHomes
                {
                    Addr = h.Addr,
                    SpecId = h.SpecId,
                    Brand = new GraphQLApiBrand
                    {
                        Name = h.Brand.Name
                    }
                }).ConfigureAwait(false);

            var homesSync = _grapghQlConsumer.SearchAllHomes(partnerId: 1, marketId: 269, sessionToken: "",
                fields: h => new GraphQLSearchAllHomes
                {
                    Addr = h.Addr,
                    SpecId = h.SpecId,
                    Brand = new GraphQLApiBrand
                    {
                        Name = h.Brand.Name
                    }
                });

            var localTask = _grapghQlConsumer.SearchAllCommsCounts(
               partnerId: 1,
               marketId: 269,
               pageSize: 10,
               brandIds: new int?[] { 4, 5 },
               sessionToken: "token",
               fields: c => new GraphQLResultCommCounts
               {
                   AveragePrice = c.AveragePrice,
                   BlCount = c.BlCount,
                   BoylHomeCount = c.BoylHomeCount,
                   BuilderCount = c.BuilderCount,
                   CommAllHomeCount = c.CommAllHomeCount,
                   CommAllLuxuryCount = c.CommAllLuxuryCount,
                   HomeCount = c.HomeCount,
                   CommCount = c.CommCount,
                   HotDealsCount = c.HotDealsCount,
                   MfrCommCount = c.MfrCommCount,
                   ModelHomeCount = c.ModelHomeCount,
                   NbyCommCount = c.NbyCommCount,
                   PaidCommCount = c.PaidCommCount,
                   PaidNbyCommCount = c.PaidNbyCommCount,
                   QmiCount = c.QmiCount,
                   Radius = c.Radius,
                   SftRange = c.SftRange,
                   TotalBrands = c.TotalBrands,
                   TotalPlans = c.TotalPlans,
                   Facets = new GraphQLApiFacets
                   {
                       Brands = new List<GraphQLApiFacetCountOption>
                       {
                           new GraphQLApiFacetCountOption()
                           {
                               Count = c.Facets.Brands.FirstOrDefault()?.Count,
                               Key = c.Facets.Brands.FirstOrDefault()?.Key
                           }
                       }
                   }
               }
           );

            Console.WriteLine("Sync");
            foreach (var r in homesSync)
            {
                Console.WriteLine(r.Addr);
            }

            Console.WriteLine("Async");
            foreach (var r in homesAsync)
            {
                Console.WriteLine(r.Addr);
            }
            Console.WriteLine("Sync");
            foreach (var test in localTask)
            {
                Console.WriteLine(test.QmiCount);
            }

            Console.ReadLine();
        }
    }
}
